#mvn -q dependency:copy-dependencies
#mvn -q compile
CP="./config/:./target/classes/:./target/dependency/*"

MEMORY="-Xmx20g"
# OPTIONS="$MEMORY -Xss40m -ea -cp $CP"
# OPTIONS="$MEMORY -Xss40m -ea -cp $CP"
OPTIONS="$MEMORY -Xss40m -cp $CP"
PACKAGE_PREFIX="edu.illinois.cs.cogcomp"

MAIN="$PACKAGE_PREFIX.nytlabs.corpus.ACEEvents"
MAIN="$PACKAGE_PREFIX.nytlabs.corpus.core.NYTAnnotations"
MAIN="$PACKAGE_PREFIX.salience.learning.Main"
MAIN="$PACKAGE_PREFIX.readers.LazyNYTReader"
MAIN="$PACKAGE_PREFIX.features.MyTrainer"
MAIN="$PACKAGE_PREFIX.nytlabs.corpus.core.Evaluator"
#MAIN="$PACKAGE_PREFIX.features.AllFeatures"

#MAIN="$PACKAGE_PREFIX.salience.learning.Main"
#MAIN="$PACKAGE_PREFIX.nytlabs.corpus.MainClass"

time nice java $OPTIONS $MAIN $CONFIG_STR $*
