#!/usr/bin/env bash
ME=`basename $0` # for usage message

if [ "$#" -eq 0 ]; then 	# number of args
    echo "USAGE: "
    echo "$ME"
    exit
fi

tmp1="/tmp/tmp1$RANDOM"
tmp2="/tmp/tmp2$RANDOM"

cp "$1" "$tmp1"
for file in "$@"
do
    comm -1 -2 "$tmp1" "$file" > "$tmp2"
    cp "$tmp2" "$tmp1"
done
cat "$tmp1"
rm "$tmp1" "$tmp2"

if [[ $? == 0 ]]        # success
then
    :                   # do nothing
else                    # something went wrong
    echo "PROBLEM";            # echo file with problems
fi
