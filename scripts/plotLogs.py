import os
import sys
from optparse import OptionParser
import matplotlib.pyplot as plt
import math
if __name__=="__main__":
    f=[math.log(1+int(x.strip().split("\t")[1])) for x in open(sys.argv[1]).readlines()]
    plt.plot(f)
    plt.show()
