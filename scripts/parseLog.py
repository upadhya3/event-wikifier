import os
import sys
import math
import argparse
paragraph_prob={}
best_para={}
paraMatrix=[]
articleId=[]

def processEachPara(buf,title):
    global paraMatrix,articleId
    articleId+=[title]
    row=[]
    for b in buf:
        if "ABSTRACT TO ANNOTATE" in b or len(b.split(" "))!=3:
            continue
        sim,pos=b.split(" ")[0],int(b.split(" ")[2])
        # paragraph_prob[b.split(" ")[2]]=b.split(" ")[0]
        if pos not in paragraph_prob:
            paragraph_prob[pos]=[sim]
        else:
            paragraph_prob[pos]+=[sim]
        row+=[float(sim)]
    # paraMatrix+=[row+[0]*(60-len(row))]
    paraMatrix+=[row]

def processBuf(buf):
    assert "Best paragraph" in buf[-1]
    # print buf[1]
    tmp=buf[-1].split(" ")
    # print tmp[0:3],
    # print tmp[3].split("-->")[0]
    best=[int(tmp[2]),float(tmp[3].split("-->")[0])]
    # print best
    if best[0] not in best_para:
        best_para[best[0]]=[best[1]]
    else:
        best_para[best[0]]+=[best[1]]
    processEachPara(buf[3:-1],buf[1])

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Short sample app')

    parser.add_argument('-f', action="store", dest="logfile")
    parser.add_argument('-t', action="store", dest="thres", type=float)
    parser.add_argument('-s', action="store_true", dest="showFirst", default=False)

    options=parser.parse_args(sys.argv[1:])
    
    f=open(options.logfile)
    buf=[]
    for line in f:
        line=line.strip()
        if "===========" in line:
            processBuf(buf)
            buf=[]
        else:
            buf+=[line]
            # print buf

    for i in paraMatrix:
        # print len(i)
        if not options.showFirst and max(i)==i[0]: continue   # other paragraphs than 0
        for j in i:
            if j > options.thres:
                print j,
            else:
                print 0,
        print

# for i,j in enumerate(articleId):
#     print i,j

# for k in (paragraph_prob.keys()):
#     # print k,(paragraph_prob[k]+[0]*(200-len(paragraph_prob[k])))
#     print k,len(paragraph_prob[k]+[0]*(200-len(paragraph_prob[k])))
# print paragraph_prob.keys()

# FOR HEATMAP
# for k in range(0,11):
#     # sorted(paragraph_prob[k])
#     tmp=sorted(paragraph_prob[k]+[0]*(200-len(paragraph_prob[k])))
#     tmp=map(lambda x: float(x),tmp)
#     tmp=map(lambda x: 0 if x < 0.3 else x,tmp)
#     # print tmp
#     # print k," ".join(map(lambda x: str(x),tmp))
#     print " ".join(map(lambda x: str(x),tmp))
#     # print k,len(paragraph_prob[k]+[0]*(200-len(paragraph_prob[k])))


# for k in best_para.keys():
#     print k,len(best_para[k])
    # print k,sorted(best_para[k])

# print paraMatrix

