# coding: utf-8
import csv
import sys
from collections import Counter
from urlparse import urlparse
from itertools import islice

csv.field_size_limit(sys.maxsize)

f=open(sys.argv[1])

news_urls=csv.reader(open(sys.argv[2]),delimiter='\t')

# urls=[]
# for n in news_urls:
#     urls.append(n[0])
# print len(urls)

# print urls[0:5]
# sys.exit(0)

# target_urls=["http://news.bbc.co.uk/",'http://www.nytimes.com/']

target_urls=[]
for n in news_urls:
    target_urls.append(n[0].strip())
# print len(urls)
print target_urls[0:5]


data=csv.reader(f,delimiter='\t')
url_cnt=Counter()
cats_cnt=Counter()
url_cats_cnt=Counter()
page_cnt=Counter()
bad=0                           # bad due to race print conditions, should not be too much

def processURL(url):
    try:
        parsed_uri = urlparse(url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        if "///" in domain:
            # print url
            return None
        return domain
    except ValueError:
        print "INVALID URL",url
        return None



if __name__=="__main__":
    ffout=open("citations.cite_web.log","w")
    c=0
    # for row in islice(data,5000):
    for row in data:
        if len(row)!=4:
            # print "botched due to parallel"
            bad+=1
            continue
        assert len(row)==4
        page=row[1]
        cats=row[-1].split('#')
        domainurl = processURL(row[0])
        # print domainurl,domainurl in target_urls
        if domainurl in target_urls:
            print >>ffout, "\t".join(row)
            # print page
            # print domainurl
            # print cats
            c+=1
    ffout.close()
    print "BAD LINES",bad
    print "total",c
