import os
import sys
from optparse import OptionParser
import glob
import shutil
import random
from random import shuffle

def checkDuplicate(folder):
    print folder
    files=glob.glob(folder+"/"+"*.txt")
    if len(files)==0:
        print "EMPTY!"
    files=map(lambda x: os.path.basename(x).strip("0_"),files)
    print files
    if len(set(files))!=len(files):
        print "ALERT!"

def touch(path):
    with open(path, 'a'):
        os.utime(path, None)
        
def makeAnn(folder):
    files=glob.glob(folder+"/"+"*.txt")
    for f in files:
        # print f,os.path.basename(f)
        print folder+"/"+os.path.basename(f).split(".")[0]+".ann"
        touch(folder+"/"+os.path.basename(f).split(".")[0]+".ann")

def makeDuplicates():
    files=glob.glob("*.txt")
    print len(files)

    shuffle(files)

    for f in files[0:40]:
        print f
        base=os.path.splitext(f)[0]
        shutil.copy(f,"0_"+base+".txt")

def moveDuplicates():
    files=glob.glob("*.txt")
    print len(files)

    shuffle(files)
    # THEN MOVE
    folders=[f[0] for f in os.walk(".") if f[0] is not "." ]
    for i,folder in enumerate(folders):
        for f in files[20*i:20*(i+1)]:
            print f,folder+"/"+f
            shutil.move(f,folder+"/"+f)

if __name__=="__main__":
    step=int(sys.argv[1])
    if step==0:
        makeDuplicates()
    elif step==1:
        moveDuplicates()
    elif step==2:
        folders=[f[0] for f in os.walk(".") if f[0] is not "." ]
        for fold in folders:
            checkDuplicate(fold)
    elif step==3:
        folders=[f[0] for f in os.walk(".") if f[0] is not "." ]
        for fold in folders:
            makeAnn(fold)


    # shuffled = sorted(folders, key=lambda k: random.random())
    # overlap=zip(folders,shuffled)
    
    # for o in overlap:
    #     print o
