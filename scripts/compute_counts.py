# coding: utf-8
import csv
import sys
from collections import Counter
from urlparse import urlparse
from itertools import islice

csv.field_size_limit(sys.maxsize)

f=open(sys.argv[1])             # citations.log file for example
data=csv.reader(f,delimiter='\t')
url_cnt=Counter()
cats_cnt=Counter()
url_cats_cnt=Counter()
page_cnt=Counter()
bad=0                           # bad due to race print conditions, should not be too much

def processURL(url):
    try:
        parsed_uri = urlparse(url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        if "///" in domain:
            # print url
            return None
        return domain
    except ValueError:
        print "INVALID URL",url
        return None

def isValid(raw_url,page,outlinks,cats):
    if raw_url=="null":
        return False
    if outlinks < 100:
        return False
    return True

def updateCounters(page,cats,url):
    page_cnt.update((page,))
    cats_cnt.update(cats)
    url_cnt.update((url,))
    for cat in cats:
        url_cats_cnt.update((cat+"#"+url,))

def printCounts():
    print "PAGES with citations", len(page_cnt)

    f=open("pages.log","w")
    for i in page_cnt.most_common():
        print>>f, i[0],"\t",i[1]
    f.close()

    f=open("urls.log","w")
    print "unique URL", len(url_cnt)
    for i in url_cnt.most_common():
        print>>f, i[0],"\t",i[1]
    f.close()

    f=open("cats.log","w")
    print "unique cats", len(cats_cnt)
    for i in cats_cnt.most_common():
        print>>f, i[0],"\t",i[1]
    f.close()

    print "unique cats X urls", len(url_cats_cnt)
    f=open("urls_x_cats.log","w")
    for i in url_cats_cnt.most_common():
        print>>f, i[0],"\t",i[1]
    f.close()

if __name__=="__main__":
    # for row in islice(data,100000):
    for row in data:
        if len(row)!=4:
            # print "botched due to parallel"
            bad+=1
            continue
        assert len(row)==4
        page=row[1]
        cats=row[-1].split('#')
        domainurl = processURL(row[0])
        if domainurl is not None:
            updateCounters(page,cats,domainurl)
        # print page
        # print domainurl
        # print cats
    printCounts()
    print "BAD LINES",bad
