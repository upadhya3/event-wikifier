import os
import sys
import shutil
import random

def turn_off_output():
    sys.stdout = open(os.devnull,'w')

def turn_on_output(temp):
    sys.stdout = temp

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

if __name__=="__main__":
    # do something
    if len(sys.argv)!=4:
        print "USAGE <script> num_samples dir_to_sample dest"
        sys.exit(-1)
    num_samples=int(sys.argv[1])
    src=sys.argv[2]
    mylist = os.listdir(src)
    dest=sys.argv[3]
    sample =[mylist[i] for i in sorted(random.sample(xrange(len(mylist)), num_samples))]
    for f in sample:
        length=file_len(src+"/"+f)
        if length < 10:
            continue
        else:
            print f,length
            shutil.copyfile(src+"/"+f,dest+"/"+f)
    print "Size of dataset",len(os.listdir(dest))
