package edu.illinois.cs.cogcomp.framenet;

import edu.cmu.cs.lti.ark.fn.Semafor;
import edu.cmu.cs.lti.ark.fn.data.prep.formats.Sentence;
import edu.cmu.cs.lti.ark.fn.parsing.SemaforParseResult;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by upadhya3 on 12/4/15.
 */
public class SemaforUtils {
    public static void main(String[] args) throws Exception {
        Semafor semafor = Semafor.getSemaforInstance("/home/upadhya3/code/java_code/models/semafor_malt_model_20121129");
//        final File inputFile = new File("/home/upadhya3/code/java_code/semafor/input.conll");
//        InputSupplier<InputStreamReader> inputSupplier = Files.newReaderSupplier(inputFile, Charsets.UTF_8);
//        final SentenceCodec.SentenceIterator sentences = ConllCodec.readInput(inputSupplier.getInput());
//        while (sentences.hasNext()) {
//            final Sentence sentence = sentences.next();
        MyCuratorClient cc = new MyCuratorClient(false);
        TextAnnotation ta = cc.client.createAnnotatedTextAnnotation("", "", "The policeman charged him with a baton.");
        List<Sentence> cmusents = getCMUSentences(ta);
        Sentence sentence = cmusents.get(0);
            SemaforParseResult result = semafor.parseSentence(sentence);
            System.out.println(result.toJson());
            System.out.println();
            for(SemaforParseResult.Frame frame:result.frames)
            {
                System.out.println(frame.target.name);
                for(SemaforParseResult.Frame.Span span:frame.target.spans)
                    System.out.println(span.start+" "+span.end+" "+span.text);

            }
        }

    public static List<Sentence> getCMUSentences(TextAnnotation ta) {
        TokenLabelView tokview = (TokenLabelView) ta.getView(ViewNames.TOKENS);
        List<Sentence> ans = new ArrayList<>();
        for(int i=0;i<ta.getNumberOfSentences();i++)
        {
            edu.illinois.cs.cogcomp.core.datastructures.textannotation.Sentence ss = ta.getSentence(i);
            List<Constituent> tokens = tokview.getConstituentsCovering(ss.getSentenceConstituent());
            List<edu.cmu.cs.lti.ark.fn.data.prep.formats.Token> tmp= new ArrayList<>();
            for(Constituent tok:tokens)
            {
                edu.cmu.cs.lti.ark.fn.data.prep.formats.Token tokn=new edu.cmu.cs.lti.ark.fn.data.prep.formats.Token(null,tok.getSurfaceForm(), TAUtils.getLemma(tok),null,TAUtils.getPos(tok),null,null,null,null,null);
                tmp.add(tokn);
            }
            ans.add(new Sentence(tmp));
        }
        return ans;
    }
}
