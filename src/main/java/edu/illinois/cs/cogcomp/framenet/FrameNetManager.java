
package edu.illinois.cs.cogcomp.framenet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.thrift.base.Span;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class FrameNetManager {
	public static Map<String, Frame> frames;				// frame name -> Frame
	public static Map< Pair<String,String>, Set<Frame>> luToFrame;		// <posType, lexical unit string> -> Frames
	private static String FNHOME;
	private static DocumentBuilder dBuilder;
	public FrameNetManager(String fnhome){
		FNHOME=fnhome;
		frames = new HashMap<String, Frame>();
		luToFrame = new HashMap< Pair<String,String>, Set<Frame>>();

		try {
			readFrames();
			readLexicalUnits();
			mapLUToFrame();
			readFrameRelations();
			DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}


	}


	/**
	 * Go down the descendant tree and find all children frames
	 *
	 * @param f		The parent frame to start from
	 * @return		The set of all children/descendant frames
	 */
	public Set<Frame> getAllChildrenFrames(Frame f, Set<Frame> seenFrames) {
		Set<Frame> resultFrames = new HashSet<Frame>();

//		System.out.println("Parent frame: "+f.getName());
		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SubFrame);
//		System.out.println("Children frames:");
//		if(relatedFrames!=null) {
//			for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
//				System.out.print(" ["+it.next()+"]");
//			}
//			System.out.println("");
//		}

		if(relatedFrames!=null) {
			for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
				String frameName = it.next();
				Frame frame = frames.get(frameName);
				if(frame!=null && !seenFrames.contains(frame)) {
					resultFrames.add(frame);
					seenFrames.add(frame);
					//System.out.println("Going in");
					resultFrames.addAll( getAllChildrenFrames(frame, seenFrames) );
				}
			}
		}

		return resultFrames;
	}

	public Set<Frame> getDirectChildrenFrames(Frame f) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SubFrame);
		for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);
			if(frame!=null) {
				resultFrames.add(frame);
			}
		}

		return resultFrames;
	}

	public Set<Frame> getDirectParentFrames(Frame f) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		Set<String> relatedFrames = f.getRelatedFrames(FrameNetConstants.SuperFrame);
		for(Iterator<String> it=relatedFrames.iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);
			if(frame!=null) {
				resultFrames.add(frame);
			}
		}

		return resultFrames;
	}

	/**
	 * Gather a set of relevant frames based on an input list of lexical units. However, if I cannot find any relevant frames based on 'n', I will default to n=1.
	 *
	 * @param lus	List of <posType,lemma> lexical units
	 * @param n		A frame is only relevant if it contains at least 'n' elements in lus 
	 * @return		A set of relevant frames
	 */
	public Set<Frame> getRelevantFrames(List<Pair<String,String>> lus, int n) {
		Set<Frame> resultFrames = new HashSet<Frame>();

		// first, gather the entire set of candidate frames
		Set<Frame> candidateFrames = new HashSet<Frame>();
		for(int i=0; i<lus.size(); i++) {
			Pair<String,String> lu = lus.get(i);
			if(luToFrame.containsKey(lu)) {
				candidateFrames.addAll(luToFrame.get(lu));
			}
		}

		for(Iterator<Frame> frameIt=candidateFrames.iterator(); frameIt.hasNext();) {
			// count how many input lexical units this frame contains
			Frame frame = frameIt.next();
			int c = 0;
			for(int i=0; i<lus.size(); i++) {
				String posType = lus.get(i).getFirst();
				String lemma = lus.get(i).getSecond();
				if(frame.hasLU(posType, lemma)) {
					c += 1;
				}
			}
			if(c >= n) {
				resultFrames.add(frame);
			}
		}

		if(resultFrames.size() >= 1)
			return resultFrames;
		else
			return candidateFrames;
	}


	private static void mapLUToFrame() {
		for(Iterator<String> it=frames.keySet().iterator(); it.hasNext();) {
			String frameName = it.next();
			Frame frame = frames.get(frameName);

			Set<LU> lus = frame.getAllLu();
			for(Iterator<LU> luIt=lus.iterator(); luIt.hasNext();) {
				LU lu = luIt.next();
				Pair<String,String> p = new Pair<String,String>(lu.getPosType(), lu.getLemma());
				Set<Frame> myframes = null;
				if(!luToFrame.containsKey(p)) {
					myframes = new HashSet<Frame>();
				}
				else {
					myframes = luToFrame.get(p);
				}
				myframes.add(frame);
				luToFrame.put(p, myframes);
			}
		}
	}

	private static void readFrames() throws FileNotFoundException {
		List<String> lines = LineIO.read(FNHOME+"/"+"frameIndex.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<frame ")) {
				String frameName = Util.getXMLAttributeValue(line, "name");
				Frame frame = new Frame(frameName);
				frames.put(frameName, frame);
			}
		}
	}

	private static void readLexicalUnits() throws FileNotFoundException{
		List<String> lines = LineIO.read(FNHOME+"/"+"luIndex.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<lu ")) {
				String frameName = Util.getXMLAttributeValue(line, "frameName");
				assert frames.containsKey(frameName);

				String luName = Util.getXMLAttributeValue(line, "name");
				String lemma = luName.substring(0, luName.lastIndexOf("."));
				lemma = lemma.replaceAll("\\(", "");
				lemma = lemma.replaceAll("\\)", "");
				lemma = lemma.replaceAll(" ", "_");

				String posType = null;
				if(luName.endsWith(".n"))
					posType = Constants.NOUN;
				else if(luName.endsWith(".v"))
					posType = Constants.VERB;

				// TODO: handling punishment[act]
				if(lemma.contains("_"))
				{
//					System.out.println(lemma+" "+posType);
					if(lemma.endsWith("_act") || lemma.endsWith("_entity") || lemma.endsWith("_event"))
					{
						lemma=lemma.split("_")[0];
					}
				}

				if( posType!=null && (posType.compareTo(Constants.NOUN)==0 || posType.compareTo(Constants.VERB)==0) ) {
					LU lu = new LU(lemma, posType);
					frames.get(frameName).addLu(lu);
				}
			}
		}
	}

	private static List<TextAnnotation> readExamplesForLexicalUnits() throws FileNotFoundException{
		List<String> lines = LineIO.read(FNHOME+"/"+"luIndex.xml");
		List<TextAnnotation> examples = new ArrayList<>();
		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<lu ")) {
				String frameName = Util.getXMLAttributeValue(line, "frameName");
				String luName = Util.getXMLAttributeValue(line, "name");
				String luid= Util.getXMLAttributeValue(line, "ID");
				assert frames.containsKey(frameName);
				boolean hasAnnotation= Boolean.parseBoolean(Util.getXMLAttributeValue(line, "hasAnnotation"));
//				System.out.println(frameName+" "+hasAnnotation);
				if(hasAnnotation && !luName.contains(" "))
				{
					String lupath=FNHOME+"/lu/"+"lu"+luid+".xml";
					List<String> lufile= LineIO.read(lupath);
//					processExample(frameName,luName, lufile,examples);
					processXML(frameName,luName, lupath,examples);
				}
			}
		}
		return examples;
	}

	private static void processXML(String frameName, String luName, String lufile, List<TextAnnotation> examples) {
		Document doc = null;
//		System.out.println(lufile);
		try {
			doc = dBuilder.parse(lufile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("sentence");
//			System.out.println("----------------------------");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
//				System.out.println("\nCurrent Element :"
//						+ nNode.getNodeName());
//				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String text=eElement.getElementsByTagName("text").item(0).getTextContent();
				TextAnnotation ta = MyCuratorClient.taBuilder.createTextAnnotation(text);
//				System.out.println(text);
				Element annotations = (Element) eElement.getElementsByTagName("annotationSet").item(1);
				NodeList annNodes = annotations.getElementsByTagName("layer");
				PredicateArgumentView pav = new PredicateArgumentView("frameView", ta);
				// find the evoker
				Constituent evoker = null;
				for(int i=0;i<annNodes.getLength();i++) {
					Element annNode = (Element) annNodes.item(i);
					if (annNode.getAttribute("name").equals("Target")) {

						NodeList labels = annNode.getElementsByTagName("label");
						for (int j = 0; j < labels.getLength(); j++) {
							Element label = (Element) labels.item(j);
							if (label.getAttribute("start").equals("") || label.getAttribute("end").equals(""))
								continue;

							int start = Integer.parseInt(label.getAttribute("start"));
							int end = Integer.parseInt(label.getAttribute("end"));
							int spanStart=ta.getTokenIdFromCharacterOffset(start);
							int spanEnd=ta.getTokenIdFromCharacterOffset(end)+1;
							String relation = label.getAttribute("name");
							evoker= new Constituent("predicate", "frameView", ta, spanStart, spanEnd);
							evoker.addAttribute("frameName",frameName);
//							System.out.println("SURFACE "+ta.getText().substring(start,end+1));
//							System.out.println("CONS "+evoker.getSurfaceForm());
							if(!evoker.getSurfaceForm().equals(ta.getText().substring(start,end+1)))
							{
//								System.out.println("BBBBBBBBBBBBBBBBB!");
								evoker=null;
//									System.exit(-1);
								break;
							}
//							System.out.println(start + " " + end + " " + relation);
						}
					}
				}
				if(evoker==null)
					continue;

				List<Constituent> args=new ArrayList<>();
				List<String> relations = new ArrayList<>();
				for(int i=0;i<annNodes.getLength();i++)
				{
					Element annNode = (Element) annNodes.item(i);
					if(annNode.getAttribute("name").equals("FE"))
					{
						NodeList labels = annNode.getElementsByTagName("label");
						for(int j=0;j<labels.getLength();j++)
						{
							Element label = (Element) labels.item(j);
							if(label.getAttribute("start").equals("") || label.getAttribute("end").equals(""))
								continue;

							int start = Integer.parseInt(label.getAttribute("start"));
							int end = Integer.parseInt(label.getAttribute("end"));
							int spanStart=ta.getTokenIdFromCharacterOffset(start);
							int spanEnd=ta.getTokenIdFromCharacterOffset(end)+1;
							Constituent cons = new Constituent("label", "frameView", ta, spanStart, spanEnd);
//							System.out.println("SURFACE "+ta.getText().substring(start,end+1));
//							System.out.println("CONS "+cons.getSurfaceForm());
							if(!cons.getSurfaceForm().equals(ta.getText().substring(start,end+1)))
							{
//								System.out.println("AAAAAAAAAAAA!");
//									System.exit(-1);
								continue;
							}
							String relation = label.getAttribute("name");
//							Relation rel = new Relation(relation,evoker,cons,1.0);
//							System.out.println(start + " " + end + " "+ relation);
							args.add(cons);
							relations.add(relation);
						}
					}
				}
				double[] scores = new double[args.size()];
				for(int i=0;i<scores.length;i++) scores[i]=1.0;
				pav.addPredicateArguments(evoker,args,relations.toArray(new String[relations.size()]),scores);
				ta.addView("frameView",pav);
				examples.add(ta);
//				}
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		System.exit(-1);
	}

//	private static void processExample(String frameName, String luName, List<String> lufile, List<TextAnnotation> examples) {
//		String text="";
//		String rawtext="";
//		TextAnnotation ta = MyCuratorClient.taBuilder.createTextAnnotation(text);
//		PredicateArgumentView pav = new PredicateArgumentView(text,ta);
//		for(String luline:lufile) {
//			luline=luline.trim();
//			if(luline.startsWith("<text>"))
//			{
//				luline=luline.replace("<text>", "");
//				luline=luline.replace("</text>", "");
//				rawtext=luline;
//				text=Util.cleanLine(luline);
////							System.out.println(text);
//			}
//			getTarget(lufile);
//			try{
//				System.out.println(frameName+" "+luName+" "+start+" "+end+" "+text.substring(start, end+1));
//			}
//			catch(StringIndexOutOfBoundsException e)
//			{
//				e.printStackTrace();
//				System.out.println(rawtext);
//				System.out.println(text);
//				System.out.println(luName+" "+start+" "+end+" ");
//				System.out.println("---------");
//			}
//			if
//		}
//	}
//
//	private static void getTarget(List<String> lufile) {
//		int start,end;
//		for(String luline:lufile) {
//			luline = luline.trim();
//			if (luline.startsWith("<label") && luline.endsWith("name=\"Target\"/>")) {
//				start = Integer.parseInt(Util.getXMLAttributeValue(luline, "start"));
//				end = Integer.parseInt(Util.getXMLAttributeValue(luline, "end"));
//			}
//		}
//		new Span(start,end+1);
//	}

	private static void readFrameRelations() throws FileNotFoundException{
		List<String> lines = LineIO.read(FNHOME+"/"+"frRelation.xml");

		for(String line:lines) {
			line=line.trim();
			if(line.startsWith("<frameRelation ")) {
				String subFrame = Util.getXMLAttributeValue(line, "subFrameName");
				String superFrame = Util.getXMLAttributeValue(line, "superFrameName");
				assert frames.containsKey(subFrame);
				assert frames.containsKey(superFrame);

				frames.get(subFrame).addRelation(FrameNetConstants.SuperFrame, superFrame);
				frames.get(superFrame).addRelation(FrameNetConstants.SubFrame, subFrame);
			}
		}
	}



	public static void printFrames() {
		for(Iterator<Frame> it=frames.values().iterator(); it.hasNext();) {
			System.out.println(it.next());
		}
	}

	public static void main(String[] args) throws FileNotFoundException, ParserConfigurationException {
		FrameNetManager fm = new FrameNetManager("/shared/bronte/upadhya3/framenet_1.6");
//		FrameNetManager fm = new FrameNetManager("/Users/Shyam/code/java_code/nyt-stats/framenet_1.5");
		Frame ff = fm.frames.get("Event");
//		for(Frame fff:fm.getDirectChildrenFrames(ff))
//			System.out.println(fff.getName());;
		Set<Frame> seen = new HashSet<Frame>();
		Set<Frame> leaves = fm.getAllChildrenFrames(ff, seen);
		for(Frame fff:leaves)
		{
			System.out.println(fff.getName() + fff.getAllLu());
		}

		System.out.println(leaves.size());
//		List<TextAnnotation> examples = fm.readExamplesForLexicalUnits();
//		for(TextAnnotation ta:examples)
//		{
//			PredicateArgumentView pav= (PredicateArgumentView) ta.getView("frameView");
//			List<Constituent> preds = pav.filterPredicates();
//			System.out.println(preds.size());
//			Constituent pred = preds.get(0);
//			System.out.println(pred.getLabel());
//			System.out.println(pred.getAttribute("frameName"));
//
//			for(Relation rel:pred.getOutgoingRelations())
//			{
//				System.out.println(rel.getRelationName());
//				System.out.println(rel.getTarget());
//			}
//		}
//		System.out.println(examples.size());
//		Frame ff = fm.frames.get("Rewards_and_punishments");


//		int cnt=0;
//		for(String frame_name:fm.frames.keySet()) {
//			Frame ff = fm.frames.get(frame_name);
//			for (LU ll : ff.getAllLu()) {
//				if(ll.getLemma().contains("_"))
//				{
//					cnt++;
//					break;
//				}
//			}
//		}
//		System.out.println(cnt+"/"+fm.frames.size());
//		cnt=0;
//		for(Pair<String,String>tmp:fm.luToFrame.keySet())
//		{
//			if(tmp.getSecond().contains("_"))
//				cnt++;
//		}
//		System.out.println(cnt+"/"+fm.luToFrame.size());


//		System.out.println(fm.frames.size());
//		fm.printFrames();
//		int c=0;
//		for(Frame f: frames.values())
//		{
//			System.out.println(f.getAllLu().size());
//			if(f.getAllLu().size()>=10)
//			{
//				c++;
//			}
//		}
//		ArrayList<Pair<String, String>> tmp = new ArrayList<Pair<String, String>>();
//		tmp.add(new Pair<String, String>("VERB","set"));
//		tmp.add(new Pair<String, String>("VERB","free"));
//		Set<Frame> ff = fm.getRelevantFrames(tmp, 1);
//		System.out.println(ff);
//		for(Pair<String,String> k:luToFrame.keySet())
//		{
//			System.out.println(k);
//		}
//		System.out.println(c);
	}


}
