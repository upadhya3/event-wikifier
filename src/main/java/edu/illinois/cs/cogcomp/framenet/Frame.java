
package edu.illinois.cs.cogcomp.framenet;

// import coreUtil.common.Constants;

import java.util.*;


public class Frame {
	private final String name;		// name of this frame
	private Set<LU> lus;
	private Map<String, Set<String>> frameRelations;
	
	public Frame(String name) {
		this.name = name;
		lus = new HashSet<LU>();
		
		frameRelations = new HashMap<String, Set<String>>();
	}
	
	public String getName() {
		return name;
	}
	
	public void addLu(LU lu) {
		lus.add(lu);
	}
	public Set<LU> getAllLu() {
		return lus;
	}
	
	public List<LU> getLuByPos(String pos) {
		assert (pos.compareTo(Constants.NOUN)==0 || pos.compareTo(Constants.VERB)==0 || 
				pos.compareTo(Constants.ADJ)==0 || pos.compareTo(Constants.ADV)==0);
		
		List<LU> mylus = new ArrayList<LU>();
		for(Iterator<LU> it=lus.iterator(); it.hasNext();) {
			LU lu = it.next();
			if(lu.getPosType().compareTo(pos)==0) {
				mylus.add(lu);
			}
		}
			
		return mylus;
	}
	
	public void printLuByPos(String pos) {
		List<LU> mylus = getLuByPos(pos);
		
		StringBuffer s = new StringBuffer("");
		for(int i=0; i<mylus.size(); i++) {
			s.append(mylus.get(i).getLemma()+" ");
		}
		System.out.print(s.toString().trim());
	}
	
	public boolean hasLU(String posType, String lemma) {
		for(Iterator<LU> it=lus.iterator(); it.hasNext();) {
			LU lu = it.next();
			if(posType.compareTo(lu.getPosType())==0 && lemma.compareTo(lu.getLemma())==0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param r		Input frame relation type
	 * @param f		Name of a frame
	 */
	public void addRelation(String r, String f) {
		assert FrameNetConstants.frameRelations.contains(r);
		
		Set<String> myset = null;
		if(!frameRelations.containsKey(r)) {
			myset = new HashSet<String>();
		}
		else {
			myset = frameRelations.get(r);
		}
		myset.add(f);
		frameRelations.put(r, myset);
	}
	
	/**
	 * Get related frames of a specific relation type
	 * 
	 * @param r		Input frame relation type ; possible types are defined in FrameNetConstants
	 * @return		The set of related frame (names)
	 */
	public Set<String> getRelatedFrames(String r) {
		assert FrameNetConstants.frameRelations.contains(r);
		return frameRelations.get(r);
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer(name+":");
		for(Iterator<LU> it=lus.iterator(); it.hasNext();) {
			LU lu = it.next();
			s.append(" "+lu);
		}
		return s.toString();
	}
	
	public boolean equals(Object other) {
		if(this == other)
			return true;
		if(!(other instanceof Frame))
			return false;
		if(this.name.compareTo( ((Frame)other).getName() )==0) 
			return true;
		else
			return false;
	}
	
	public int hashcode() {
		return this.name.hashCode();
	}
}

