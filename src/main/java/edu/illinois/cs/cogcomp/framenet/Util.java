
package edu.illinois.cs.cogcomp.framenet;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;

import java.util.*;

public class Util {
	 
	
	/**
	 * E.g., if myset = {"A","B",Z"} and sep = "-", then output String is: A-B-Z
	 * 
	 * @param myset		Input set of strings
	 * @param sep		User provided separator
	 * @return
	 */
	public static String toSymbolSeparatedString(Set<String> myset, String sep) {
		StringBuffer s = new StringBuffer("");
		for(Iterator<String> it=myset.iterator(); it.hasNext();) {
			s.append(sep+it.next());
		}
		return s.toString().substring(1);
	}
	
	/**
	 * E.g.: tag : <xmltag attribute1="abc" attribute2="xyz"> ; attribute is "attribute2" ; this method returns "xyz"
	 * 
	 * @param tag			Input XML line; this should contain the attribute
	 * @param attribute		Attribute whose value to extract
	 * @return				NULL if " "+attribute+"=" is not present in the input XML line, else return value of attribute
	 */
	public static String getXMLAttributeValue(String tag, String attribute) {
		String key = " "+attribute+"=";
		String value = null;
		
		if(tag.contains(key)) {
			int i1 = tag.indexOf("\"", tag.indexOf(key))+1;
			int i2 = tag.indexOf("\"", i1);
			value = tag.substring(i1, i2);
		}
		return value;
	}
	
	public static String getXMLValue(String inputString, String key) {

		int pos = inputString.indexOf("<" + key);

		if (pos == -1) {
			System.out.println("ERROR: Unable to find " + key + " in the input string.");
			System.out.println("Input string = " + inputString);
			System.exit(1);
		}

		int pos1 = inputString.indexOf(">", pos + 1);
		int pos2 = inputString.indexOf("</" + key + ">");
		String value = inputString.substring(pos1 + 1, pos2);

		return value.trim();
	}
	
	// *NOTE: it uses the first integer of the pair
	public static int binarySearch(List<Pair<Integer,Integer>> values, int key, int low, int high) {
		if(high < low) {
			System.out.println("****ERROR:binary search failed; key="+key);
			return -1;
		}
		
		int mid = low + (high-low)/2;
		if(key < values.get(mid).getFirst().intValue()) 
			return binarySearch(values, key, low, mid-1);
		else if(key > values.get(mid).getFirst().intValue()) 
			return binarySearch(values, key, mid+1, high);
		else
			return mid;
	}

	public static String posType(String p) {
		
		if(p.compareTo(Constants.NOUN)==0 || p.startsWith("NN"))
			return Constants.NOUN;
		else if(p.compareTo(Constants.VERB)==0 || p.startsWith("VB"))
			return Constants.VERB;
		else if(p.compareTo(Constants.ADJ)==0 || p.startsWith("JJ"))
			return Constants.ADJ;
		else if(p.compareTo(Constants.ADV)==0 || p.startsWith("RB") || p.compareTo("WRB")==0) 
			return Constants.ADV;
		else
			return Constants.MISC;
	}
	
	public static String cleanLine(String line) {
		String s = line;
	
		s = s.replaceAll("\\t+", " ");
		s = s.replaceAll("__", "_");
		s = s.replaceAll(" _ ", " ");
		s = s.replaceAll("_ ", " ");
		s = s.replaceAll(" _", " ");
		s = s.replaceAll("//", " ");
		s = s.replaceAll(" \\.\\.\\. ", " ");
		s = s.replaceAll("\\.\\.\\.", "\\.");
		s = s.replaceAll("\\s\\s+", " ");
	
		s = s.replaceAll("&lt;", "<");
		s = s.replaceAll("&gt;", ">");
		s = s.replaceAll("&apos;", "'");
		s = s.replaceAll("&quot;", "\"");
		s = s.replaceAll("&amp;", "&");
		
		s = s.replaceAll("&LT;", "<");
		s = s.replaceAll("&GT;", ">");
		s = s.replaceAll("&APOS;", "'");
		s = s.replaceAll("&QUOT;", "\"");
		s = s.replaceAll("&AMP;", "&");
		
//		s = s.replaceAll("''' ", "'\"");
//		s = s.replaceAll("''", "\"");
//		s = s.replaceAll("``", "\"");
		
		return s;
	}
	
	
	public static void sortConstituents(List<Constituent> constituents) {

		Collections.sort(constituents, new Comparator<Constituent>() {

			public int compare(Constituent o1, Constituent o2) {
				if (o1.getStartSpan() > o2.getStartSpan()) {
					return 1;			// o1 is greater
				} else {
					if (o1.getStartSpan() == o2.getStartSpan()) {
						if (o1.getEndSpan() > o2.getEndSpan())
							return 1;	// o1 is greater
						else
							return 0;	// 
					} else
						return -1;
				}
			}
		});
	}

	public static List<Pair<String, Double>> mapToListPair(Map<String, Double> mymap) {
		List<Pair<String, Double>> mylist = new ArrayList<Pair<String, Double>>();
		for(Iterator<String> it=mymap.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			Double value = mymap.get(key);
			mylist.add(new Pair<String, Double>(key, value));
		}
		return mylist;
	}
	
	public static List sortByValueDescending(List<Pair<String, Double>> mylist) {
		List list = new LinkedList(mylist);
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable)((Pair)(o2)).getSecond()).compareTo(((Pair)o1).getSecond());
			}
		});
		return list;
	}
	
	public static Map sortByValue(Map map) {
		List list = new LinkedList(map.entrySet());
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
			}
		});
		// logger.info(list);
		Map result = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry)it.next();
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
	public static Constituent duplicateConstituent(Constituent c, String viewName) {
		Constituent newCon = new Constituent(c.getLabel(), viewName, c.getTextAnnotation(), c.getStartSpan(), c.getEndSpan());
		Set<String> attributeKeys = c.getAttributeKeys();
		for(Iterator<String> it=attributeKeys.iterator(); it.hasNext();) {
			String attributeKey = it.next();
			String attributeValue = c.getAttribute(attributeKey);
			newCon.addAttribute(attributeKey, attributeValue);
		}
		return newCon;
	}
	
//	public static double calculateF1(int c, int r, int p) {
//		double recall = c;
//		recall = recall/r;
//		double precision = c;
//		precision = precision/p;
//		double f1 = (2*precision*recall)/(precision+recall);
//		return f1;
//	}
//
//	public static double calculateF1(double c, double r, double p) {
//		double recall = c/r;
//		double precision = c/p;
//		double f1 = (2*precision*recall)/(precision+recall);
//		return f1;
//	}
//
//	public static double calculateWeightedF1(int c, int r, int p, int beta) {
//		double recall = c;
//		recall = recall/r;
//		double precision = c;
//		precision = precision/p;
//		double f1 = (1+(beta*beta))*(precision*recall)/( ((beta*beta)*precision) + recall );
//		return f1;
//	}
//
//	public static double logNatural(double s) {
//        if(s==0) {
//        	return Math.log(Double.MIN_VALUE);
//        }
//        else {
//        	return Math.log(s);
//        }
//	}
//
//	public static String findMaxValue(Map<String, Double> scores) {
//		String maxLabel = "";
//		double maxScore = 0;
//		for(Iterator<String> it=scores.keySet().iterator(); it.hasNext();) {
//			String key = it.next();
//			double score = scores.get(key).doubleValue();
//			if(score > maxScore) {
//				maxLabel = key;
//				maxScore = score;
//			}
//		}
//		return maxLabel;
//	}
//
//	public static Map<String, Double> normalize(Map<String, Double> scores) {
//		Map<String, Double> newScores = new HashMap<String, Double>();
//		double total = 0;
//		for(Iterator<Double> it=scores.values().iterator(); it.hasNext();) {
//			total += it.next().doubleValue();
//		}
//		for(Iterator<String> it=scores.keySet().iterator(); it.hasNext();) {
//			String key = it.next();
//			double oldScore = scores.get(key).doubleValue();
//			double newScore = oldScore/total;
//			newScores.put(key, new Double(newScore));
//		}
//
//		return newScores;
//	}
	
}



