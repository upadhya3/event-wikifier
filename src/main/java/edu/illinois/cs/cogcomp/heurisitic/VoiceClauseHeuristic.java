package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by upadhya3 on 2/12/16.
 */
public class VoiceClauseHeuristic extends Heuristic {
    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {
        String voice = TAUtils.voiceFeat(ee.pred_srl).getName();
        String clause =TAUtils.getClauseNature(ee.pred_srl);
        String lemma = TAUtils.getLemma(ee.pred_srl);
        String srl_type= ee.pred_srl.getViewName();

        boolean isFirst=TAUtils.isFirstNonAppVerbPredicateInList(ee, others);

        if(voice.equals("A") || clause.equals("main"))
        {
            ee.salient=1;
        }
        if(clause.equals("relative")) // maybe relative too?
        {
            ee.salient=0;
        }
        if(lemma.equals("say"))
        {
            ee.salient=0;
        }
        if(!isFirst)
        {
            ee.salient=0;
        }
        if(srl_type.equals("SRL_NOM"))
        {
            ee.salient=0;
        }
    }


}
