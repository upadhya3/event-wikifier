package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.features.AllFeatures;
import edu.illinois.cs.cogcomp.features.LBJavaFeatureExtractor;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjsrc.MyClassifier;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.util.List;

/**
 * Created by Shyam on 2/16/16.
 */
public class ClassifierHeuristic extends Heuristic{
    private final MyClassifier classifier;
    private final AllFeatures feat;

    public static int posPred=0;
    public static int negPred=0;
    public ClassifierHeuristic(MyClassifier c, AllFeatures feat)
    {
        this.classifier=c;
        this.feat=feat;
    }

    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {
        FeatureVector f = feat.classify(ee);
//            System.out.println(f.featuresSize());
//        System.out.println(e==null);
        String ans = classifier.discreteValue(ee);
        if(ans.equals("1"))
        {
            posPred++;
            ee.salient=1;
        }
        else
            negPred++;
//            ScoreSet ss = learner.scores(inst);
    }
}
