package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by upadhya3 on 2/12/16.
 */
public class TenseHeuristic extends Heuristic {
    private final String type;

    public TenseHeuristic(String type) {
        this.type=type;
    }

    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {
        if(TAUtils.getTense(ee.pred_srl).equals(type))
        {
            ee.salient=1;
        }

    }
}
