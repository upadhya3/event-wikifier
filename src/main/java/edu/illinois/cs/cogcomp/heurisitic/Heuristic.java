package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.util.List;

/**
 * Created by upadhya3 on 2/10/16.
 */
public abstract class Heuristic {
     public abstract void apply(EventInstance ee, List<EventInstance> others) throws EdisonException;

    public static void ApplyHeuristic(AnnotatedDocument doc, Heuristic h, String type) throws EdisonException {
        List<EventInstance> target;
        if(type.equals("abs"))
            target=doc.abs_events;
        else
        {
            target=doc.body_events;
        }
        for(EventInstance ee:target)
        {
            h.apply(ee,target);
        }
    }
}
