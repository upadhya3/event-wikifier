package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by upadhya3 on 2/10/16.
 */
public class VoiceHeuristic extends Heuristic{
    private final String type;

    public VoiceHeuristic(String type){
        this.type=type;
    }
    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {

        if(TAUtils.voiceFeat(ee.pred_srl).getName().equals(type))
        {
            ee.salient=1;
        }
//        if(TAUtils.getClauseNature(ee.pred_srl).equals("subord")) // maybe relative too?
//        {
//            ee.salient=0;
//        }
//        if(TAUtils.isInAppositive(ee.pred_srl)) // maybe relative too?
//        {
//            ee.salient=0;
//        }
    }
}
