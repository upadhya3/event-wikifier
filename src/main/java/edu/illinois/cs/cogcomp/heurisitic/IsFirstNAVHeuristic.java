package edu.illinois.cs.cogcomp.heurisitic;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by Shyam on 2/21/16.
 */
public class IsFirstNAVHeuristic extends Heuristic {
    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {
        boolean isFirst= TAUtils.isFirstNonAppVerbPredicateInList(ee, others);
        if(isFirst)
        {
            ee.salient=1;
        }
    }
}
