package edu.illinois.cs.cogcomp.timeml;


import edu.illinois.cs.cogcomp.framenet.Constants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class TimeMLConstants {
    // public static final String PROPERTY_FILENAME =
    // "edu/illinois/cs/cogcomp/illinoisEE/common/illinoisEE.properties";
    public static final String PROPERTY_FILENAME = "illinois-machine-reading.properties";

    public static final String NOUN = "NOUN";
    public static final String VERB = "VERB";
    public static final String ADJ = "ADJ";
    public static final String ADV = "ADV";
    public static final String MISC = "MISC";

    public static final String BINARY_TEST = "BINARY_TEST";
    public static final String MULTI_TEST = "MULTI_TEST";

    public static final String TIMEML = "TIMEML";

    public static final String ALL_EVENT_MENTION = "ALL_EVENT_MENTION"; // includes
    // STATE
    public static final String EVENT_MENTION = "EVENT_MENTION"; // excludes
    // STATE
    public static final String EVENT_MENTION_CANDIDATE = "EVENT_MENTION_CANDIDATE";
    public static final String NOT_EVENT_MENTION = "NULL";

    public static final String POS_MENTION = "POS_MENTION";
    public static final String PARSE_PHRASE = "PARSE_PHRASE";
    public static final String CHUNK_PHRASE = "CHUNK_PHRASE";

    public static final String NE_MENTION = "NE_MENTION";
    public static final String VALUE_MENTION = "VALUE_MENTION";
    public static final String TIME_MENTION = "TIME_MENTION";
    public static final String PREDICATE = "PREDICATE";
    public static final String NOT_MENTION = "NULL";

    public static final String GOLD_NE_MENTION_VIEW = "GOLD_NE_MENTION_VIEW";
    public static final String GOLD_VALUE_MENTION_VIEW = "GOLD_VALUE_MENTION_VIEW";
    public static final String GOLD_TIME_MENTION_VIEW = "GOLD_TIME_MENTION_VIEW";
    public static final String GOLD_PREDICATE_VIEW = "GOLD_PREDICATE_VIEW";
    public static final String CANDIDATE_NE_MENTION_VIEW = "CANDIDATE_NE_MENTION_VIEW";
    public static final String CANDIDATE_PREDICATE_VIEW = "CANDIDATE_PREDICATE_VIEW";
    public static final String TYPED_CANDIDATE_NE_MENTION_VIEW = "TYPED_CANDIDATE_NE_MENTION_VIEW";
    public static final String PRED_NE_MENTION_VIEW = "PRED_NE_MENTION_VIEW";
    public static final String PRED_PREDICATE_VIEW = "PRED_PREDICATE_VIEW";

    public static final String GOLD_EVENT_PREDICATE_ARGUMENT_VIEW = "GOLD_EVENT_PREDICATE_ARGUMENT_VIEW";
    public static final String PRED_EVENT_PREDICATE_ARGUMENT_VIEW = "PRED_EVENT_PREDICATE_ARGUMENT_VIEW";

    public static final String GOLD_SENSE_VIEW = "GOLD_SENSE_VIEW";
    public static final String CANDIDATE_SENSE_VIEW = "CANDIDATE_SENSE_VIEW";
    public static final String PRED_SENSE_VIEW = "PRED_SENSE_VIEW";

    public static final String PRED_CAND_TRIGGER_VIEW = "PRED_CAND_TRIGGER_VIEW";
    public static final String TRIGGER_CANDIDATE = "TRIGGER_CANDIDATE";
    public static final String PRED_TYPED_TRIGGER_VIEW = "PRED_TYPED_TRIGGER_VIEW";

    public static Set<String> setXMLTags = new HashSet<String>();
    static {
        setXMLTags.add("ANNOTATION");
        setXMLTags.add("BODY");
        setXMLTags.add("DATE");
        setXMLTags.add("DATETIME");
        setXMLTags.add("DATE_TIME");
        setXMLTags.add("DOC");
        setXMLTags.add("DOCID");
        setXMLTags.add("DOCNO");
        setXMLTags.add("DOCTYPE");
        setXMLTags.add("ENDTIME");
        setXMLTags.add("END_TIME");
        setXMLTags.add("FOOTER"); // not found in either ace04, ace05, nor IC
        setXMLTags.add("HEADER");
        setXMLTags.add("HEADLINE");
        setXMLTags.add("KEYWORD"); // not found in either ace04, ace05, nor IC
        setXMLTags.add("P");
        setXMLTags.add("POST");
        setXMLTags.add("POSTDATE");
        setXMLTags.add("POSTER");
        setXMLTags.add("QUOTE");
        setXMLTags.add("SLUG");
        setXMLTags.add("SPEAKER");
        setXMLTags.add("SUBJECT");
        setXMLTags.add("TEXT");
        setXMLTags.add("TRAILER");
        setXMLTags.add("TURN");
    }

    public static Set<String> validStartPOS = new HashSet<String>();
    static {
        validStartPOS.add("D");
        validStartPOS.add("J");
        validStartPOS.add("V");
        validStartPOS.add("N");
        validStartPOS.add("P");
        validStartPOS.add("W");
        // ---------------------
        validStartPOS.add("C");
        validStartPOS.add("D");
        validStartPOS.add("E");
        validStartPOS.add("F");
        validStartPOS.add("I");
        validStartPOS.add("J");
        validStartPOS.add("L");
        validStartPOS.add("M");
        validStartPOS.add("N");
        validStartPOS.add("P");
        validStartPOS.add("R");
        validStartPOS.add("S");
        validStartPOS.add("T");
        validStartPOS.add("U");
        validStartPOS.add("V");
        validStartPOS.add("W");
    }
    public static Set<String> validEndPOS = new HashSet<String>();
    static {
        validEndPOS.add("N");
        validEndPOS.add("R");
        validEndPOS.add("P");
        validEndPOS.add("W");
        // ---------------------
        validEndPOS.add("C");
        validEndPOS.add("D");
        validEndPOS.add("E");
        validEndPOS.add("F");
        validEndPOS.add("I");
        validEndPOS.add("J");
        validEndPOS.add("L");
        validEndPOS.add("M");
        validEndPOS.add("N");
        validEndPOS.add("P");
        validEndPOS.add("R");
        validEndPOS.add("S");
        validEndPOS.add("T");
        validEndPOS.add("U");
        validEndPOS.add("V");
        validEndPOS.add("W");
    }
    public static Set<String> invalidStartPOS = new HashSet<String>();
    static {
        invalidStartPOS.add(".");
        invalidStartPOS.add(",");
        invalidStartPOS.add(":");
        invalidStartPOS.add(";");
        invalidStartPOS.add("!");
        invalidStartPOS.add("$");
        invalidStartPOS.add("*");
        // -----------------------
        // invalidStartPOS.add("POS");
    }

    public static Set<String> invalidEndPOS = new HashSet<String>();
    static {
        invalidEndPOS.add(".");
        invalidEndPOS.add(",");
        invalidEndPOS.add(":");
        invalidEndPOS.add(";");
        invalidEndPOS.add("!");
        invalidEndPOS.add("$");
        invalidEndPOS.add("*");
        // -----------------------
        // invalidEndPOS.add("D");
        // invalidEndPOS.add("J");
        // invalidEndPOS.add("V");
        // invalidEndPOS.add("POS");
    }

    public static Set<String> countNouns = new HashSet<String>();
    static {
        countNouns.add("several");
        countNouns.add("few");
        countNouns.add("tens");
        countNouns.add("hundreds");
        countNouns.add("thousands");
        countNouns.add("millions");
        countNouns.add("dozens");
    }

    // ========= START of Relations stuffs =========
    public static final String NO_RELATION = "NO_RELATION";
    public static final String PRED_RELATION_VIEW = "PRED_RELATION_VIEW";
    public static final String GOLD_RELATION_VIEW = "GOLD_RELATION_VIEW";

    public static Set<String> binaryRelationLabels = new HashSet<String>();
    static {
        binaryRelationLabels.add(new String("HAS_RELATION"));
        binaryRelationLabels.add(Constants.NO_RELATION);
    }

    public static Set<String> coarseRelationLabels = new HashSet<String>();
    static {
        coarseRelationLabels.add("m1-ART-m2");
        coarseRelationLabels.add("m1-EMP-ORG-m2");
        coarseRelationLabels.add("m1-GPE-AFF-m2");
        coarseRelationLabels.add("m1-OTHER-AFF-m2");
        coarseRelationLabels.add("m1-PER-SOC-m2");
        coarseRelationLabels.add("m1-PHYS-m2");
        coarseRelationLabels.add("m2-ART-m1");
        coarseRelationLabels.add("m2-EMP-ORG-m1");
        coarseRelationLabels.add("m2-GPE-AFF-m1");
        coarseRelationLabels.add("m2-OTHER-AFF-m1");
        coarseRelationLabels.add("m2-PER-SOC-m1");
        coarseRelationLabels.add("m2-PHYS-m1");
        coarseRelationLabels.add(Constants.NO_RELATION);
    }

    public static Set<String> fineRelationLabels = new HashSet<String>();
    static {
        fineRelationLabels.add("m1-ART:Inventor-or-Manufacturer-m2");
        fineRelationLabels.add("m1-ART:Other-m2");
        fineRelationLabels.add("m1-ART:User-or-Owner-m2");
        fineRelationLabels.add("m1-EMP-ORG:Employ-Executive-m2");
        fineRelationLabels.add("m1-EMP-ORG:Employ-Staff-m2");
        fineRelationLabels.add("m1-EMP-ORG:Employ-Undetermined-m2");
        fineRelationLabels.add("m1-EMP-ORG:Member-of-Group-m2");
        fineRelationLabels.add("m1-EMP-ORG:Other-m2");
        fineRelationLabels.add("m1-EMP-ORG:Partner-m2");
        fineRelationLabels.add("m1-EMP-ORG:Subsidiary-m2");
        fineRelationLabels.add("m1-GPE-AFF:Based-In-m2");
        fineRelationLabels.add("m1-GPE-AFF:Citizen-or-Resident-m2");
        fineRelationLabels.add("m1-GPE-AFF:Other-m2");
        fineRelationLabels.add("m1-OTHER-AFF:Ethnic-m2");
        fineRelationLabels.add("m1-OTHER-AFF:Ideology-m2");
        fineRelationLabels.add("m1-OTHER-AFF:Other-m2");
        fineRelationLabels.add("m1-PER-SOC:Business-m2");
        fineRelationLabels.add("m1-PER-SOC:Family-m2");
        fineRelationLabels.add("m1-PER-SOC:Other-m2");
        fineRelationLabels.add("m1-PHYS:Located-m2");
        fineRelationLabels.add("m1-PHYS:Near-m2");
        fineRelationLabels.add("m1-PHYS:Part-Whole-m2");
        fineRelationLabels.add("m2-ART:Inventor-or-Manufacturer-m1");
        fineRelationLabels.add("m2-ART:Other-m1");
        fineRelationLabels.add("m2-ART:User-or-Owner-m1");
        fineRelationLabels.add("m2-EMP-ORG:Employ-Executive-m1");
        fineRelationLabels.add("m2-EMP-ORG:Employ-Staff-m1");
        fineRelationLabels.add("m2-EMP-ORG:Employ-Undetermined-m1");
        fineRelationLabels.add("m2-EMP-ORG:Member-of-Group-m1");
        fineRelationLabels.add("m2-EMP-ORG:Other-m1");
        fineRelationLabels.add("m2-EMP-ORG:Partner-m1");
        fineRelationLabels.add("m2-EMP-ORG:Subsidiary-m1");
        fineRelationLabels.add("m2-GPE-AFF:Based-In-m1");
        fineRelationLabels.add("m2-GPE-AFF:Citizen-or-Resident-m1");
        fineRelationLabels.add("m2-GPE-AFF:Other-m1");
        fineRelationLabels.add("m2-OTHER-AFF:Ethnic-m1");
        fineRelationLabels.add("m2-OTHER-AFF:Ideology-m1");
        fineRelationLabels.add("m2-OTHER-AFF:Other-m1");
        fineRelationLabels.add("m2-PER-SOC:Business-m1");
        fineRelationLabels.add("m2-PER-SOC:Family-m1");
        fineRelationLabels.add("m2-PER-SOC:Other-m1");
        fineRelationLabels.add("m2-PHYS:Located-m1");
        fineRelationLabels.add("m2-PHYS:Near-m1");
        fineRelationLabels.add("m2-PHYS:Part-Whole-m1");
        fineRelationLabels.add(Constants.NO_RELATION);
    }

    public static Set<String> relationsToIgnore = new HashSet<String>();
    static {
        // I might want to ignore: DISC PHYS:Located PHYS:Near EMP-ORG:Partner
        // relationsToIgnore.add("PHYS:Located");
        relationsToIgnore.add("DISC");
        // relationsToIgnore.add("PHYS:Near");
    }
    // ========= END of Relations stuffs =========

    // ======== IC stuffs =========
    public static final String IC_PREFIX = "IC_";
    public static final String IC_MENTION_VIEW = "IC_MENTION_VIEW";

    public static HashMap<String, String> aceToIcMentionsRemap = new HashMap<String, String>();
    static {


        // FAC:Building One or more buildings that can be referred to as a unit,
        // e.g.: the [national archives]
        // IC: Building

        aceToIcMentionsRemap.put("FAC:Other", "Property");
        aceToIcMentionsRemap.put("FAC:Path", "Property");
        aceToIcMentionsRemap.put("FAC:Plant", "Property");
        aceToIcMentionsRemap.put("FAC:Subarea-Building", "Building" );

        aceToIcMentionsRemap.put("FAC:Building", "Building");//
        // IC: PhysicalThing -> NonHumanPhysicalThing -> Building

        // GPE:County-or-District Any county, district, or prefecture, e.g.:
        // [Palm Beach] and [Volusia] [counties]
        // IC: Municipality
        // //aceToIcMentionsRemap.put("GPE:County-or-District", "Municipality");
        // IC: PersonGroup -> HumanOrganization -> GeopoliticalEntity ->
        // Municipality



        // GPE:Nation Any nation, e.g.: Japan, Spain
        // IC: NationState
        aceToIcMentionsRemap.put("GPE:Nation", "StateOrProvince");//
        // IC: PersonGroup -> HumanOrganization -> GeopoliticalEntity ->
        // NationState

        // GPE:Population-Center E.g.: Washington, New Delhi, Salzburg
        // IC: Municipality
        // //aceToIcMentionsRemap.put("GPE:Population-Center", "Municipality");

        aceToIcMentionsRemap.put("GPE:Municipality", "CityTownOrVillage");//


        // GPE:State-or-Province State or province, e.g.: Texas
        // IC: StateOrProvince
        aceToIcMentionsRemap.put("GPE:State-or-Province", "StateOrProvince");//
        // IC: PersonGroup -> HumanOrganization -> GeopoliticalEntity ->
        // StateOrProvince

        aceToIcMentionsRemap.put("GPE:Other", "GeopoliticalOrganization");
        aceToIcMentionsRemap.put("GPE:Continent", "GeopoliticalOrganization");


        // ORG:Commercial Organizations focusing primarily on profits. Also
        // includes industries and industrial sectors. E.g. [Major League
        // Baseball], [Techsource Marine Industries], the [Press Agency]
        // IC: CommericalOrganization
        aceToIcMentionsRemap.put("ORG:Commercial", "CommercialCompany");//
        // IC: HumanAgentGroup -> PersonGroup -> HumanOrganization ->
        // CommericalOrganization

        // ORG:Educational E.g.: the [Applied Research Laboratory],
        // [Pennsylvania State University]
        // IC: EducationalInstitution
        aceToIcMentionsRemap.put("ORG:Educational", "EducationalInstitution");//
        // IC: HumanAgentGroup -> PersonGroup -> HumanOrganization ->
        // EducationalInstitution

        // ORG:Government Organizations dealing with the affairs of government
        // or the state. Political parties are not included here. E.g.: a former
        // [KGB] agent, the [court], the security [services], [Congress]
        // IC: GovernmentOrganization
        aceToIcMentionsRemap.put("ORG:Government", "GovernmentOrganization");//
        // IC: HumanAgentGroup -> PersonGroup -> HumanOrganization ->
        // GovernmentOrganization

        aceToIcMentionsRemap.put("ORG:Terrorist", "TerroristOrganization");//
        // IC: HumanAgentGroup -> PersonGroup -> HumanOrganization ->
        // TerroristOrganization

        aceToIcMentionsRemap.put("ORG:Political", "PoliticalParty");//
        // IC: PersonGroup -> HumanOrganization ->
        // DemocraticPoliticalOrganization -> PoliticalParty

        aceToIcMentionsRemap.put("ORG:Other", "HumanOrganization");

        // PER Includes a single person, groups of people (e.g. family,
        // painters, linguists), as well as ethnic and religious groups (the
        // Arabs, the Christians)
        // IC: Person
        aceToIcMentionsRemap.put("PER", "Person");//

        // WEA:Exploding Weapons that are designed to accomplish damage through
        // explosion, e.g.: explosives
        // IC: Bomb
        aceToIcMentionsRemap.put("WEA:Exploding", "Weapon");//

        aceToIcMentionsRemap.put("WEA:Blunt", "Weapon");
        aceToIcMentionsRemap.put("WEA:Chemical", "Weapon");
        aceToIcMentionsRemap.put("WEA:Exploding", "Weapon");
        aceToIcMentionsRemap.put("WEA:Nuclear", "Weapon");
        aceToIcMentionsRemap.put("WEA:Other", "Weapon");
        aceToIcMentionsRemap.put("WEA:Projectile", "Weapon");
        aceToIcMentionsRemap.put("WEA:Sharp", "Weapon");
        aceToIcMentionsRemap.put("WEA:Shooting", "Weapon");

        aceToIcMentionsRemap.put("LOC:Address", "Location");
        aceToIcMentionsRemap.put("LOC:Boundary", "Location");
        aceToIcMentionsRemap.put("LOC:Celestial", "Location");
        aceToIcMentionsRemap.put("LOC:Land-Region-Natural", "Location");
        aceToIcMentionsRemap.put("LOC:Region-International", "Location");
        aceToIcMentionsRemap.put("LOC:Region-National", "Location");
        aceToIcMentionsRemap.put("LOC:Water-Body", "Location");

        //TODO: could also be locations; support multiple type labels
        aceToIcMentionsRemap.put("VEH:Air", "Property");
        aceToIcMentionsRemap.put("VEH:Land", "Property");
        aceToIcMentionsRemap.put("VEH:Other", "Property");
        aceToIcMentionsRemap.put("VEH:Subarea-Vehicle", "Property");
        aceToIcMentionsRemap.put("VEH:Water", "Property");

        aceToIcMentionsRemap.put( "NULL", "NULL" );



		/*
		 * WEA:Blunt Weapons used as a bludgeoning instrument, e.g.: a [baseball
		 * bat] IC: Weapon
		 *
		 * WEA:Chemical E.g.: Sarin gas IC: Weapon
		 *
		 * WEA:Nuclear E.g.: nuclear [missiles] IC: Weapon
		 *
		 * WEA:Other IC: Weapon
		 *
		 * WEA:Projectile E.g.: bullets IC: Weapon
		 *
		 * WEA:Sharp E.g.: knife IC: Weapon
		 *
		 * WEA:Shooting Weapons used to send projectile objects at great speed,
		 * e.g.: pistol IC: Weapon
		 */
        // aceToIcMentionsRemap.put("WEA:Blunt", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Chemical", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Nuclear", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Other", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Projectile", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Sharp", "Weapon");
        // aceToIcMentionsRemap.put("WEA:Shooting", "Weapon");
    }

    public static HashMap<String, String> aceToIcRelationsRemap = new HashMap<String, String>();
    static {
        // GEN-AFF:Citizen arg1 is a citizen of arg2
        // Person hasCitizenship NationState arg1 is a citizen of arg2
        aceToIcRelationsRemap.put("m1-GEN-AFF:Citizen-m2",
                "m1-hasCitizenship-m2");
        aceToIcRelationsRemap.put("m2-GEN-AFF:Citizen-m1",
                "m2-hasCitizenship-m1");
        // IC: hasCitizenship: Person -- NationState

        // ORG-AFF:Employ-Executive arg1 is an executive of arg2 *change
        // direction
        // HumanAgent employs Person // HumanAgent can be ORG
        // HumanOrganization isLedBy Person arg1 has leader arg2
        aceToIcRelationsRemap.put("m1-ORG-AFF:Employ-Executive-m2",
                "m2-isLedBy-m1");
        aceToIcRelationsRemap.put("m2-ORG-AFF:Employ-Executive-m1",
                "m1-isLedBy-m2");
        // IC: isLedBy: HumanOrganization -- Person

        // ORG-AFF:Employ-Staff arg1 is a staff of arg2 *change direction
        // HumanAgent employs Person arg1 employs arg2
        aceToIcRelationsRemap
                .put("m1-ORG-AFF:Employ-Staff-m2", "m2-employs-m1");
        aceToIcRelationsRemap
                .put("m2-ORG-AFF:Employ-Staff-m1", "m1-employs-m2");
        // IC: employs: HumanAgent -- Person

        // ORG-AFF:Member arg1 is a member of arg2 *change direction
        // Group hasMember Thing arg1 has member arg2
        aceToIcRelationsRemap.put("m1-ORG-AFF:Member-m2", "m2-hasMember-m1");
        aceToIcRelationsRemap.put("m2-ORG-AFF:Member-m1", "m1-hasMember-m2");
        // **** Specialization: I can do: PER,ORG,GPE x PER,ORG,GPE
        // If either m1/m2 is PER, then I use hasMemberPerson; else I use
        // hasMemberHumanAgent
        // IC:hasMember: Group -- Thing
        // hasMemberBuilding: GroupOfBuildings -- Building
        // hasMemberHumanAgent: HumanAgentGroup -- HumanAgent
        // hasMemberPerson: PersonGroup -- Person

        // ORG-AFF:Student-Alum arg1 is an alum of arg2
        // Person attendedSchool EducationalInstitution arg1 attended arg2
        aceToIcRelationsRemap.put("m1-ORG-AFF:Student-Alum-m2",
                "m1-attendedSchool-m2");
        aceToIcRelationsRemap.put("m2-ORG-AFF:Student-Alum-m1",
                "m2-attendedSchool-m1");
        // IC:attendedSchool: Person -- EducationalInstitution

        // ORG-AFF:Subsidiary arg1 is a subsidiary of arg2 *change direction
        // HumanOrganization hasSubOrganization HumanOrganization arg1 has arg2
        // as sub
        aceToIcRelationsRemap.put("m1-ORG-AFF:Subsidiary-m2",
                "m2-hasSubOrganization-m1");
        aceToIcRelationsRemap.put("m2-ORG-AFF:Subsidiary-m1",
                "m1-hasSubOrganization-m2");
        // **** Specialization:
        // m1-ORG-AFF:Subsidiary-m2: if m2=GPE and m1=ORG:Government, then =>
        // hasGovernment
        // m2-ORG-AFF:Subsidiary-m1: if m1=GPE and m2=ORG:Government, then =>
        // hasGovernment
        // m1-ORG-AFF:Subsidiary-m2: if m1=GPE and m2=GPE, then => hasSubGPE
        // m2-ORG-AFF:Subsidiary-m1: if m1=GPE and m2=GPE, then => hasSubGPE
        // IC:hasSubOrganization: HumanOrganization -- HumanOrganization
        // hasGovernment: GeopoliticalEntity -- Government
        // hasSubGPE: GeopoliticalEntity -- GeopoliticalEntity

        // PER-SOC:hasChild arg1 hasChild arg2
        // Person hasChild Person arg1 has arg2 as child
        aceToIcRelationsRemap.put("m1-PER-SOC:hasChild-m2", "m1-hasChild-m2");
        aceToIcRelationsRemap.put("m2-PER-SOC:hasChild-m1", "m2-hasChild-m1");
        // IC:hasChild: Person -- Person
        // hasDaughter: Person -- Person
        // hasSon: Person -- Person

        // PER-SOC:hasSibling
        // Person hasSibling Person should be undirected
        aceToIcRelationsRemap.put("PER-SOC:hasSibling", "hasSibling");
        // IC:hasSibling: Person -- Person
        // hasBrother
        // hasSister

        // PER-SOC:hasSpouse
        // Person hasSpouse Person should be undirected
        aceToIcRelationsRemap.put("PER-SOC:hasSpouse", "hasSpouse");
        // IC:hasSpouse: Person -- Person

        // PHYS:Gpe-Part-Whole arg1 is a part of arg2 *change direction
        // GeopoliticalEntity hasSubGPE GeopoliticalEntity arg1 is larger than
        // arg2
        aceToIcRelationsRemap.put("m1-PHYS:Gpe-Part-Whole-m2",
                "m2-hasSubGPE-m1");
        aceToIcRelationsRemap.put("m2-PHYS:Gpe-Part-Whole-m1",
                "m1-hasSubGPE-m2");
        // IC:hasSubGPE: GeopoliticalEntity -- GeopoliticalEntity

        // specialized IC relations
        aceToIcRelationsRemap.put("m1-hasMemberPerson-m2",
                "m1-hasMemberPerson-m2");
        aceToIcRelationsRemap.put("m2-hasMemberPerson-m1",
                "m2-hasMemberPerson-m1");
        aceToIcRelationsRemap.put("m1-hasMemberHumanAgent-m2",
                "m1-hasMemberHumanAgent-m2");
        aceToIcRelationsRemap.put("m2-hasMemberHumanAgent-m1",
                "m2-hasMemberHumanAgent-m1");
        aceToIcRelationsRemap.put("m1-hasGovernment-m2", "m1-hasGovernment-m2");
        aceToIcRelationsRemap.put("m2-hasGovernment-m1", "m2-hasGovernment-m1");
        aceToIcRelationsRemap.put("m1-hasSubGPE-m2", "m1-hasSubGPE-m2");
        aceToIcRelationsRemap.put("m2-hasSubGPE-m1", "m2-hasSubGPE-m1");

        aceToIcRelationsRemap.put("m1-hasSon-m2", "m1-hasSon-m2");
        aceToIcRelationsRemap.put("m2-hasSon-m1", "m2-hasSon-m1");
        aceToIcRelationsRemap.put("m1-hasDaughter-m2", "m1-hasDaughter-m2");
        aceToIcRelationsRemap.put("m2-hasDaughter-m1", "m2-hasDaughter-m1");
        aceToIcRelationsRemap.put("m1-hasBrother-m2", "m1-hasBrother-m2");
        aceToIcRelationsRemap.put("m2-hasBrother-m1", "m2-hasBrother-m1");
        aceToIcRelationsRemap.put("m1-hasSister-m2", "m1-hasSister-m2");
        aceToIcRelationsRemap.put("m2-hasSister-m1", "m2-hasSister-m1");
    }

    // =========TimeMLEvent Types========
    public static Set<String> targetEventTypes = new HashSet<String>();
    static {
        targetEventTypes.add("Conflict:Attack");
        targetEventTypes.add("Conflict:Demonstrate");
        targetEventTypes.add("Bombing");
        targetEventTypes.add("Life:Die");
        targetEventTypes.add("Life:Injure");
    }
}
