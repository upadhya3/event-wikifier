package edu.illinois.cs.cogcomp.timeml;


        import java.util.ArrayList;
        import java.util.List;

        import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
        import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
        import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
        import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
        import edu.illinois.cs.cogcomp.framenet.Util;

        import edu.illinois.cs.cogcomp.core.datastructures.Pair;

public class DataLoader {

    public DataLoader(MRResourceManager rm_) {
    }

    public static TextAnnotation createViewFromCharSpans(String viewName,
                                                         TextAnnotation ta, List<Mention> mentions) {
        List<Pair<Integer, Integer>> charSpans = new ArrayList<Pair<Integer, Integer>>();
        List<String> labels = new ArrayList<String>();
        for (Mention mention : mentions) {
            charSpans
                    .add(new Pair<Integer, Integer>(new Integer(mention
                            .getCharStartSpan()), new Integer(mention
                            .getCharEndSpan())));
            labels.add(mention.getLabel());
        }

        ta = DataLoader
                .createViewFromCharSpans(viewName, ta, charSpans, labels);
        return ta;
    }

    public static TextAnnotation createViewFromCharSpans(String viewName,
                                                         TextAnnotation ta, List<Pair<Integer, Integer>> charSpans,
                                                         List<String> labels) {
        // System.out.println(ta.getTokenizedText());
        // List<Constituent> posCons =
        // ta.getView(ViewNames.POS).getConstituents();
        // for(int i=0; i<posCons.size(); i++) {
        // System.out.print(i+":"+posCons.get(i).getStartCharOffset()+","+posCons.get(i).getEndCharOffset()+" ");
        // if((i%5)==0)
        // System.out.println("");
        // }
        //
        // for(Pair<Integer,Integer> charSpan:charSpans) {
        // int i1 = charSpan.getFirst().intValue();
        // int i2 = charSpan.getSecond().intValue();
        // System.out.println(i1+","+i2+" ["+ta.getDetokenizedText().substring(i1,
        // i2)+"]");
        // }
        // System.out.println("==============");

        List<Pair<Integer, Integer>> tokenSpans = alignCharToTokenOffset(
                charSpans, ta); // tokenSpans must be created in the same order
        // as charSpans
        // assert tokenSpans.size() == charSpans.size();
        SpanLabelView myView = new SpanLabelView(viewName, "Default", ta, 1.0,
                true);

        for (int i = 0; i < tokenSpans.size(); i++) {
            Pair<Integer, Integer> tokenSpan = tokenSpans.get(i);
            // System.out.println("label="+labels.get(i)+" viewName="+viewName+" startSpan="+tokenSpan.getFirst().intValue()+" endSpan="+tokenSpan.getSecond().intValue());
            Constituent con = new Constituent(labels.get(i), viewName, ta,
                    tokenSpan.getFirst().intValue(), tokenSpan.getSecond()
                    .intValue());
            myView.addConstituent(con);
        }
        ta.addView(viewName, myView);

        return ta;
    }

    // Convert the input argument charOffsets to tokenOffsets
    public static List<Pair<Integer, Integer>> alignCharToTokenOffset(
            List<Pair<Integer, Integer>> charOffsets, TextAnnotation ta) {
        List<Pair<Integer, Integer>> charOffsetsOfEachToken = new ArrayList<Pair<Integer, Integer>>();
        List<Pair<Integer, Integer>> tokenOffsets = new ArrayList<Pair<Integer, Integer>>();

        SpanLabelView posView = (SpanLabelView) ta.getView(ViewNames.POS);
        List<Constituent> posCons = posView.getConstituents();
        for (Constituent posCon : posCons) {
            charOffsetsOfEachToken.add(new Pair<Integer, Integer>(new Integer(
                    posCon.getStartCharOffset()), new Integer(posCon
                    .getEndCharOffset())));
        }

        // System.out.println(ta.getDetokenizedText());
        // System.out.println("--------");
        // System.out.println(ta.getTokenizedText());
        // for(Pair<Integer,Integer> p:charOffsetsOfEachToken) {
        // System.out.println(p.getFirst()+","+p.getSecond());
        // }

        for (Pair<Integer, Integer> charSpan : charOffsets) {
            int i1 = charSpan.getFirst().intValue();
            int i2 = charSpan.getSecond().intValue();
            int startTokenOffset = Util.binarySearch(charOffsetsOfEachToken,
                    i1, 0, charOffsetsOfEachToken.size() - 1);
            int endTokenOffset = -1;
            Pair<Integer, Integer> tokenOffset = null;
            if (startTokenOffset != -1) {
                for (int i = startTokenOffset; i < charOffsetsOfEachToken
                        .size()
                        && (i2 >= charOffsetsOfEachToken.get(i).getSecond()
                        .intValue()); i++) {
                    if (i2 == charOffsetsOfEachToken.get(i).getSecond()
                            .intValue()) {
                        endTokenOffset = i;
                        break;
                    }
                }
                if (endTokenOffset != -1) {
                    tokenOffset = new Pair<Integer, Integer>(startTokenOffset,
                            endTokenOffset + 1);
                }
            }
            if (tokenOffset != null) {
                // System.out.println("DataLoader:Found the token offsets for char offset: "
                // + i1+","+i2 + " [" + ta.getDetokenizedText().substring(i1,
                // i2)+"]");
                tokenOffsets.add(tokenOffset);
            } else {
                System.out
                        .println("DataLoader:Cannot find the token offsets for char offset: "
                                + i1
                                + ","
                                + i2
                                + " ["
                                + ta.getText().substring(i1, i2) + "]");
            }
        }

        return tokenOffsets;
    }

}
