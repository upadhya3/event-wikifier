package edu.illinois.cs.cogcomp.timeml;

/**
 * Created by Shyam on 11/29/15.
 */

        import java.util.List;

public class TimeMLDoc {
    /**
     *
     */
    private static final long serialVersionUID = -8551724164585264623L;
    private String docId;
    private String docIdLine;
    private Mention docTime;
    private String text; // just plain text
    private String textWithTags; // plain text with embedded xml tags
    private List<Mention> events;
    private List<Mention> times;
    private List<Mention> signals;
    private List<String> tLinkLines;
    private List<String> sLinkLines;
    private List<String> aLinkLines;

    public TimeMLDoc(String docId) {
        this.docId=docId;
        docIdLine = null;
        docTime = null;
        text = null;
        textWithTags = null;
        events = null;
        times = null;
        signals = null;
        tLinkLines = null;
        sLinkLines = null;
        aLinkLines = null;
    }

    public void setDocIdLine(String docIdLine) {
        this.docIdLine = docIdLine;
    }

    public String getDocIdLine() {
        return docIdLine;
    }

    public void setDocTime(Mention docTime) {
        this.docTime = docTime;
    }

    public Mention getDocTime() {
        return docTime;
    }

    public void setText(String s) {
        this.text = s;
    }

    public String getText() {
        return text;
    }

    public void setTextWithTags(String s) {
        this.textWithTags = s;
    }

    public String getTextWithTags() {
        return textWithTags;
    }

    public void setEvents(List<Mention> events) {
        this.events = events;
    }

    public List<Mention> getEvents() {
        return events;
    }

    public void setTimes(List<Mention> times) {
        this.times = times;
    }

    public List<Mention> getTimes() {
        return times;
    }

    public void setSignals(List<Mention> signals) {
        this.signals = signals;
    }

    public List<Mention> getSignals() {
        return signals;
    }

    public void setTLinkLines(List<String> lines) {
        tLinkLines = lines;
    }

    public List<String> getTLinkLines() {
        return tLinkLines;
    }

    public void setSLinkLines(List<String> lines) {
        sLinkLines = lines;
    }

    public List<String> getSLinkLines() {
        return sLinkLines;
    }

    public void setALinkLines(List<String> lines) {
        aLinkLines = lines;
    }

    public List<String> getALinkLines() {
        return aLinkLines;
    }

    public String toString() {
        StringBuffer s = new StringBuffer("");
        s.append("<DOC id=\"" + docId + "\">");
        s.append("\n");
        s.append("<ID line>");
        s.append(docIdLine);
        s.append("</ID>");
        s.append("\n");
        s.append(text);
        s.append("\n");
        s.append("</DOC>");
        return s.toString();
    }

    public String getId() {
        return docId;
    }
}