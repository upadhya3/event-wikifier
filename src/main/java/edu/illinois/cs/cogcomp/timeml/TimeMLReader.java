package edu.illinois.cs.cogcomp.timeml;

/**
 * Created by Shyam on 11/29/15.
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.illinois.cs.cogcomp.framenet.Util;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;

public class TimeMLReader {
    public static int singleEvent = 0;
    public static int multipleEvent = 0;
    private MRResourceManager m_resourceManager;

    public TimeMLReader( MRResourceManager rm_ )
    {
        m_resourceManager = rm_;
    }

//    public TextAnnotation readXmlFile(String filename) throws TException, ServiceUnavailableException, AnnotationFailedException {
//
//        DataLoader dl = new DataLoader( m_resourceManager );
//
//        TimeMLDoc timemlDoc = readAnnotations(filename);
//        TextAnnotation ta = dl.annotateText(TimeMLConstants.TIMEML, timemlDoc.getId(), timemlDoc.getText());	// corpusId, textId, plain-text
//
//        // **** Create EVENT_MENTION view **** //
//        List<Mention> events = timemlDoc.getEvents();
//        ta = DataLoader.createViewFromCharSpans(Constants.ALL_EVENT_MENTION, ta, events);
//
//        List<Mention> eventsSubset = new ArrayList<Mention>();
//        for(Mention mention:events) {
//            if(mention.getLabel().compareTo("STATE")!=0) {
//                eventsSubset.add(mention);
//            }
//        }
//        ta = DataLoader.createViewFromCharSpans(Constants.EVENT_MENTION, ta, eventsSubset);
//
//        for(Mention event:events) {
//            if(((TimeMLEvent)event).isSingleInstance())
//                singleEvent += 1;
//            else
//                multipleEvent += 1;
//        }
//        // **** Create TIME_MENTION view **** //
//        List<Mention> times = timemlDoc.getTimes();
//        ta = DataLoader.createViewFromCharSpans(Constants.TIME_MENTION, ta, times);
//
//        return ta;
//    }
//
    // Input arg 'text' is a piece of text consisting of all lines in the xml document, annotated with xml tags
    private static Pair<String, List< Pair<String, Pair<Integer,Integer>> >> extractXMLTagsWithPosition(String text) {
        List< Pair<String, Pair<Integer,Integer>> > tags = new ArrayList< Pair<String, Pair<Integer,Integer>> >();

        char[] chars = text.toCharArray();

        char[] textChars = new char[chars.length];
        boolean hasTag=false;
        int i=0, j=0, i1=-1, i2=-1;
        for(i=0; i<chars.length; i++) {
            if(chars[i]=='<' && ((i+5)<chars.length) && chars[i+1]=='E' && chars[i+2]=='V' && chars[i+3]=='E' && chars[i+4]=='N' && chars[i+5]=='T') {
                hasTag = true;
            }
            else if(chars[i]=='<' && ((i+6)<chars.length) && chars[i+1]=='/' && chars[i+2]=='E' && chars[i+3]=='V' && chars[i+4]=='E' && chars[i+5]=='N' && chars[i+6]=='T') {
                hasTag = true;
            }
            else if(chars[i]=='<' && ((i+5)<chars.length) && chars[i+1]=='T' && chars[i+2]=='I' && chars[i+3]=='M' && chars[i+4]=='E' && chars[i+5]=='X') {
                hasTag = true;
            }
            else if(chars[i]=='<' && ((i+6)<chars.length) && chars[i+1]=='/' && chars[i+2]=='T' && chars[i+3]=='I' && chars[i+4]=='M' && chars[i+5]=='E' && chars[i+6]=='X') {
                hasTag = true;
            }
            else if(chars[i]=='<' && ((i+6)<chars.length) && chars[i+1]=='S' && chars[i+2]=='I' && chars[i+3]=='G' && chars[i+4]=='N' && chars[i+5]=='A' && chars[i+6]=='L') {
                hasTag = true;
            }
            else if(chars[i]=='<' && ((i+7)<chars.length) && chars[i+1]=='/' && chars[i+2]=='S' && chars[i+3]=='I' && chars[i+4]=='G' && chars[i+5]=='N' && chars[i+6]=='A' && chars[i+7]=='L') {
                hasTag = true;
            }
            if(hasTag) {
                i1 = i;
                for(i2=(i1+1); chars[i2]!='>'; i2++) {}
                String xmlTag = text.substring(i1, i2+1);
                if(xmlTag.startsWith("</")) {		// ending tag
                    int lastIndex = tags.size()-1;
                    Pair<String, Pair<Integer,Integer>> p = tags.get(lastIndex);
                    String s = p.getFirst();
                    Pair<Integer,Integer> positionP = new Pair<Integer,Integer>(p.getSecond().getFirst(), new Integer(j));
                    tags.remove(lastIndex);
                    tags.add(new Pair<String, Pair<Integer,Integer>>(s, positionP));
                }
                else {								// starting tag
                    Pair<Integer,Integer> positionP = new Pair<Integer,Integer>(new Integer(j), new Integer(-1));
                    tags.add(new Pair<String, Pair<Integer,Integer>>(xmlTag, positionP));
                }
                i = i2;
                hasTag = false;
            }
            else {
                textChars[j++] = chars[i];
            }
        }

        textChars[j] = '\0';
        String justText = new String(textChars);
        justText = justText.trim();
        justText = justText.replaceAll("_", " ");
        //for(Pair<String, Pair<Integer,Integer>> tag:tags) {
        //	System.out.println(tag.getFirst()+" "+tag.getSecond().getFirst()+","+tag.getSecond().getSecond()+" ["+justText.substring(tag.getSecond().getFirst(), tag.getSecond().getSecond())+"]");
        //}

        return new Pair<String, List< Pair<String, Pair<Integer,Integer>> >>(justText, tags);
    }

    private static List<Mention> createEventMentions(List< Pair<String, Pair<Integer,Integer>> > xmlTags) {
        List<Mention> events = new ArrayList<Mention>();

        for(Pair<String, Pair<Integer,Integer>> xmlTag:xmlTags) {
            String tag = xmlTag.getFirst();
            if(tag.startsWith("<EVENT")) {
                String eId = Util.getXMLAttributeValue(tag, "eid");
                String sc = Util.getXMLAttributeValue(tag, "class");
                TimeMLEvent e = new TimeMLEvent(eId);
                e.setLabel(sc);
                e.setCharSpan(xmlTag.getSecond().getFirst().intValue(), xmlTag.getSecond().getSecond().intValue());
                events.add(e);
            }
        }

        return events;
    }

    private static List<Mention> createTimeMentions(List< Pair<String, Pair<Integer,Integer>> > xmlTags) {
        List<Mention> times = new ArrayList<Mention>();

        for(Pair<String, Pair<Integer,Integer>> xmlTag:xmlTags) {
            String tag = xmlTag.getFirst();
            if(tag.startsWith("<TIMEX")) {
                String tId = Util.getXMLAttributeValue(tag, "tid");
                String type = Util.getXMLAttributeValue(tag, "type");
                String value = Util.getXMLAttributeValue(tag, "value");
                String anchorTimeId = Util.getXMLAttributeValue(tag, "anchorTimeID");
                String beginPoint = Util.getXMLAttributeValue(tag, "beginPoint");
                String endPoint = Util.getXMLAttributeValue(tag, "endPoint");
                String freq = Util.getXMLAttributeValue(tag, "freq");
                String functionInDocument = Util.getXMLAttributeValue(tag, "functionInDocument");
                String mod = Util.getXMLAttributeValue(tag, "mod");
                String quant = Util.getXMLAttributeValue(tag, "quant");
                String temporalFunction = Util.getXMLAttributeValue(tag, "temporalFunction");
                String valueFromFunction = Util.getXMLAttributeValue(tag, "valueFromFunction");

                assert tId!=null;
                assert type!=null;
                assert value!=null;
                Time t = new Time(tId);
                t.setLabel(type);
                t.setValue(value);
                if(anchorTimeId!=null)
                    t.setAnchorTimeId(anchorTimeId);
                if(beginPoint!=null)
                    t.setBeginPoint(beginPoint);
                if(endPoint!=null)
                    t.setEndPoint(endPoint);
                if(freq!=null)
                    t.setFreq(freq);
                if(functionInDocument!=null)
                    t.setFunctionInDocument(functionInDocument);
                if(mod!=null)
                    t.setMod(mod);
                if(quant!=null)
                    t.setQuant(quant);
                if(temporalFunction!=null)
                    t.setTemporalFunction(temporalFunction);
                if(valueFromFunction!=null)
                    t.setValueFromFunction(valueFromFunction);
                t.setCharSpan(xmlTag.getSecond().getFirst().intValue(), xmlTag.getSecond().getSecond().intValue());
                times.add(t);
            }
        }

        return times;
    }

    private static List<Mention> createSignalMentions(List< Pair<String, Pair<Integer,Integer>> > xmlTags) {
        List<Mention> signals = new ArrayList<Mention>();

        for(Pair<String, Pair<Integer,Integer>> xmlTag:xmlTags) {
            String tag = xmlTag.getFirst();
            if(tag.startsWith("<SIGNAL")) {
                String sId = Util.getXMLAttributeValue(tag, "sid");
                Signal s = new Signal(sId);
                s.setCharSpan(xmlTag.getSecond().getFirst().intValue(), xmlTag.getSecond().getSecond().intValue());
                signals.add(s);
            }
        }

        return signals;
    }

    private static List<Mention> associateEventInstanceWithEvent(List<Mention> events, List<String> instanceLines) {
        Map<String, TimeMLEvent> eventMap = new HashMap<String, TimeMLEvent>();

        for(Mention event:events) {
            eventMap.put(event.getId(), (TimeMLEvent)event);
        }

        for(String line:instanceLines) {
            String id = Util.getXMLAttributeValue(line, "eiid");
            String eId = Util.getXMLAttributeValue(line, "eventID");
            String signalId = Util.getXMLAttributeValue(line, "signalID");
            String tense = Util.getXMLAttributeValue(line, "tense");
            String aspect = Util.getXMLAttributeValue(line, "aspect");
            String polarity = Util.getXMLAttributeValue(line, "polarity");
            String pos = Util.getXMLAttributeValue(line, "pos");
            String modality = Util.getXMLAttributeValue(line, "modality");			// might be null
            String cardinality = Util.getXMLAttributeValue(line, "cardinality");	// might be null
            assert id!=null;
            assert eId!=null;
            assert pos!=null;
            assert tense!=null;
            assert aspect!=null;
            assert polarity!=null;

            //assert eventMap.containsKey(eId);
            if(!eventMap.containsKey(eId)) {
                System.out.println("ERROR:TimeMLReader.associateEventInstanceWithEvent: There is an event instance for eId="+eId+", but no such event is found.");
            }
            else {
                TimeMLEventInstance ei = new TimeMLEventInstance(id);
                ei.setTense(tense);
                ei.setAspect(aspect);
                ei.setPolarity(polarity);
                ei.setPos(pos);
                if(modality!=null)
                    ei.setModality(modality);
                if(cardinality!=null)
                    ei.setCardinality(cardinality);
                if(signalId!=null)
                    ei.setSignalId(signalId);

                TimeMLEvent e = eventMap.get(eId);
                e.addEventInstance(ei);
            }
        }

        List<Mention> newEvents = new ArrayList<Mention>();
        for(Iterator<TimeMLEvent> it=eventMap.values().iterator(); it.hasNext();) {
            newEvents.add(it.next());
        }

        return newEvents;
    }



    public static TimeMLDoc readAnnotations(String filename) {
        List<String> lines = IOManager.readLines(filename);
        String docId = filename.substring(filename.lastIndexOf("/")+1, filename.indexOf(".tml"));

        String docIdLine = null;
        List<String> makeInstanceLines = new ArrayList<String>();
        List<String> tLinkLines = new ArrayList<String>();
        List<String> sLinkLines = new ArrayList<String>();
        List<String> aLinkLines = new ArrayList<String>();
        StringBuffer text = new StringBuffer("");
        for(int i=0; i<lines.size(); i++) {
            if(lines.get(i).compareTo("</TimeML>")==0) {
                break;
            }
            else if(lines.get(i).compareTo("")==0) {
                text.append(" ");
            }
            else if(lines.get(i).startsWith("<?xml ")) {}
            else if(lines.get(i).startsWith("<TimeML")) {
                while(!lines.get(i).endsWith(">"))
                    i += 1;
            }
            else if(lines.get(i).startsWith("<MAKEINSTANCE")) {
                makeInstanceLines.add(lines.get(i));
            }
            else if(lines.get(i).startsWith("<TLINK")) {
                tLinkLines.add(lines.get(i));
            }
            else if(lines.get(i).startsWith("<SLINK")) {
                sLinkLines.add(lines.get(i));
            }
            else if(lines.get(i).startsWith("<ALINK")) {
                aLinkLines.add(lines.get(i));
            }
            else {
                if(lines.get(i).contains("CREATION_TIME") || lines.get(i).contains("PUBLICATION_TIME")) {
                    docIdLine = lines.get(i);
                }
                else {
                    text.append(lines.get(i));
                }
            }
            text.append(" ");
        }

        String textWithTags = text.toString().replaceAll("\\s+", " ");
        textWithTags = textWithTags.replaceAll("\\.;", "\\ . ;");
        textWithTags = textWithTags.replaceAll("\\.\"", "\\ . \"");
        textWithTags = textWithTags.trim();

        // ---- Now, start creating a TimeMLDoc ---- //
        TimeMLDoc doc = new TimeMLDoc(docId);
        if(docIdLine!=null) {
            doc.setDocIdLine(docIdLine);
            List< Pair<String, Pair<Integer,Integer>> > docIdTimeXmlLine = new ArrayList<Pair<String, Pair<Integer,Integer>>>();
            docIdTimeXmlLine.add(new Pair<String, Pair<Integer,Integer>>(docIdLine.substring(docIdLine.indexOf("<"), docIdLine.indexOf(">")+1),
                    new Pair<Integer,Integer>(new Integer(-1), new Integer(-1))));
            List<Mention> docTime = createTimeMentions(docIdTimeXmlLine);
            assert docTime.size()==1;
            doc.setDocTime(docTime.get(0));
        }
        doc.setTextWithTags(textWithTags);					// this is with the xml tags

        Pair<String, List< Pair<String, Pair<Integer,Integer>> >> processedText = extractXMLTagsWithPosition(textWithTags);
        // I will get two things:
        // 1. plain text with no xml tags
        // 2. list of xml tags for; events, times, signals
        //System.out.println("==== After calling extractXMLTagsWithPosition ====");
        //System.out.println(textWithTags);
        //System.out.println("==========");
        //for(Pair<String, Pair<Integer,Integer>> tag:processedText.getSecond()) {
        //	System.out.println(tag.getFirst()+" "+tag.getSecond().getFirst()+","+tag.getSecond().getSecond());
        //}

        String justText = processedText.getFirst();			// get the plain text with no xml tags, and store it inside the doc
        doc.setText(justText);
        //System.out.println("JUSTTEXT:");
        //System.out.println(justText);

        // from the list of xml tags; create objs of events, times, signals
        List<Mention> events = createEventMentions(processedText.getSecond());
        List<Mention> times = createTimeMentions(processedText.getSecond());
        List<Mention> signals = createSignalMentions(processedText.getSecond());

        events = associateEventInstanceWithEvent(events, makeInstanceLines);	// associate event instances with events; I can ignore makeInstanceLines after this

        doc.setEvents(events);
        doc.setTimes(times);
        doc.setSignals(signals);
        doc.setTLinkLines(tLinkLines);
        doc.setSLinkLines(sLinkLines);
        doc.setALinkLines(aLinkLines);

        return doc;
    }

}

