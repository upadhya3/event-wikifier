package edu.illinois.cs.cogcomp.timeml;


public class Time extends Mention {
    private String value;
    private String anchorTimeId;
    private String beginPoint;
    private String endPoint;
    private String freq;
    private String functionInDocument;
    private String mod;
    private String quant;
    private String temporalFunction;
    private String valueFromFunction;

    public Time(String id) {
        super(id);
        value = null;
        anchorTimeId = null;
        beginPoint = null;
        endPoint = null;
        freq = null;
        functionInDocument = null;
        mod = null;
        quant = null;
        temporalFunction = null;
        valueFromFunction = null;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public void setAnchorTimeId(String anchorTimeId) {
        this.anchorTimeId = anchorTimeId;
    }
    public String getAnchorTimeId() {
        return anchorTimeId;
    }

    public void setBeginPoint(String beginPoint) {
        this.beginPoint = beginPoint;
    }
    public String getBeginPoint() {
        return beginPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }
    public String getEndPoint() {
        return endPoint;
    }

    public void setFreq(String freq) {
        this.freq = freq;
    }
    public String getFreq() {
        return freq;
    }

    public void setFunctionInDocument(String functionInDocument) {
        this.functionInDocument = functionInDocument;
    }
    public String getFunctionInDocument() {
        return functionInDocument;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }
    public String getMod() {
        return mod;
    }

    public void setQuant(String quant) {
        this.quant = quant;
    }
    public String getQuant() {
        return quant;
    }

    public void setTemporalFunction(String temporalFunction) {
        this.temporalFunction = temporalFunction;
    }
    public String getTemporalFunction() {
        return temporalFunction;
    }

    public void setValueFromFunction(String valueFromFunction) {
        this.valueFromFunction = valueFromFunction;
    }
    public String getValueFromFunction() {
        return valueFromFunction;
    }
}