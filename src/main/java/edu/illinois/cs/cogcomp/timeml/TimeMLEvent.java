package edu.illinois.cs.cogcomp.timeml;


        import java.util.ArrayList;
        import java.util.HashSet;
        import java.util.List;
        import java.util.Set;

public class TimeMLEvent extends Mention {
    /**
     *
     */
    private static final long serialVersionUID = -1140786156110275577L;
    private List<TimeMLEventInstance> eventInstances;

    public TimeMLEvent(String id) {
        super(id);
        this.eventInstances = new ArrayList<TimeMLEventInstance>();
    }

    public void addEventInstance(TimeMLEventInstance ei) {
        eventInstances.add(ei);
    }

    public List<TimeMLEventInstance> getEventInstances() {
        return eventInstances;
    }

    public Set<String> getEventInstanceIds() {
        Set<String> ids = new HashSet<String>();
        for (TimeMLEventInstance ei : eventInstances) {
            ids.add(ei.getId());
        }
        return ids;
    }

    public int getNumberOfEventInstances() {
        return eventInstances.size();
    }

    public boolean containEventInstance(String id) {
        Set<String> ids = getEventInstanceIds();
        if (ids.contains(id))
            return true;
        else
            return false;
    }

    public boolean isSingleInstance() {
        if (getNumberOfEventInstances() == 1
                && eventInstances.get(0).getCardinality().compareTo("1") == 0)
            return true;
        else
            return false;
    }

    public String toString() {
        StringBuffer s = new StringBuffer("");
        s.append("id=");
        s.append(super.id);
        s.append(" chars=");
        s.append(super.charStartSpan + "," + super.charEndSpan);
        s.append(" label=");
        s.append(label);
        s.append(" #");
        s.append(isSingleInstance());
        s.append(" numEventInstances=");
        s.append(getNumberOfEventInstances());
        s.append(getEventInstanceIds());
        return s.toString();
    }
}
