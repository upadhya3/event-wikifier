package edu.illinois.cs.cogcomp.timeml;

import java.util.ArrayList;
import java.util.List;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.lbjava.nlp.Sentence;
import edu.illinois.cs.cogcomp.lbjava.nlp.SentenceSplitter;


public class MySentenceSplitter {

    public MySentenceSplitter() {
    }

    // public void splitText(String inputFile, String outputFile) {
    // SentParDetector sentSplitter = new SentParDetector();
    // String content = IOManager.readContentAddingPeriod(inputFile);
    // String splittedContent = sentSplitter.markupRawText(0, content);
    // IOManager.writeContent(splittedContent, outputFile);
    // }

    public void splitTextLBJ(String inputFile, String outputFile) {
        SentenceSplitter senSplit = new SentenceSplitter(inputFile);
        Sentence allSentences[] = senSplit.splitAll();
        StringBuffer sBuf = new StringBuffer("");
        for (Sentence sentence : allSentences) {
            sBuf.append(sentence.text + "\n");
        }
        IOManager.writeContent(sBuf.toString(), outputFile);
    }

    public ArrayList<String> splitTextLBJ(String content) throws Exception {
        ArrayList<String> arrSentences = new ArrayList<String>();
        String text[] = content.split("\n");

        // if (text.length < 5)
        // return arrSentences;
        // System.out.println("text.length=" + text.length);

        // System.out.println("Lenght: " + text.length);

        SentenceSplitter senSplit = new SentenceSplitter(text);

        // System.out.println(senSplit);

        try {

            // System.out.println("Before.");
            Sentence allSentences[] = senSplit.splitAll();
            // System.out.println("After.");

            if (allSentences != null)
                for (Sentence sentence : allSentences) {
                    arrSentences.add(sentence.text);
                }

        } catch (Exception e) {
            throw e;
        }

        return arrSentences;
    }

    public static List<String> sentenceSplit(String text) {
        List<String> arrSentences = new ArrayList<String>();
        String texts[] = text.split("\n");

        SentenceSplitter senSplit = new SentenceSplitter(texts);

        Sentence allSentences[] = senSplit.splitAll();

        if (allSentences != null)
            for (Sentence sentence : allSentences) {
                arrSentences.add(sentence.text);
            }

        return arrSentences;
    }

    /**
     * Note: This function works under the assumption that there is no repeated
     * sentences in the text.
     *
     * @param text
     * @param sents
     * @return
     */
    public static List<Pair<String, Pair<Integer, Integer>>> recoverSentCharOffset(
            String text, List<String> sents) {
        List<Pair<String, Pair<Integer, Integer>>> sentWithCharOffset = new ArrayList<Pair<String, Pair<Integer, Integer>>>();
        for (String sent : sents) {
            String sStr, eStr;
            if (sent.length() > 30) {
                sStr = sent.substring(0, 30);
                eStr = sent.substring(sent.length() - 30);
            } else {
                sStr = eStr = sent;
            }
            int pos = 0;
            int start = -1;
            int end = -1;
            while (pos < sent.length()) {
                int sPos = text.indexOf(sStr, pos);
                assert (sPos != -1) : "ERROR: Cannot find the starting of the input sentence in the text.\n"
                        + sStr + "...";
                int ePos = text.indexOf(eStr, sPos);
                assert (ePos != -1) : "ERROR: Cannot find the ending of the input sentence in the text.\n"
                        + eStr + "...";
                start = sPos;
                end = ePos + eStr.length();
                String copySent = text.substring(start, end);
                if (!sent.equals(copySent)) {
                    pos = ePos + 1;
                    continue;
                } else {
                    break;
                }
            }
            assert (start != -1 && end != -1) : "ERROR: Cannot find the offset of the input sentence in the text.\n"
                    + sent;
            sentWithCharOffset.add(new Pair<String, Pair<Integer, Integer>>(
                    sent, new Pair<Integer, Integer>(start, end)));
        }
        return sentWithCharOffset;
    }
}
