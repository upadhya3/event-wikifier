package edu.illinois.cs.cogcomp.timeml;



import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
//import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;




public class MRResourceManager extends ResourceManager
{
    private static Properties properties;
    private final static String RESOURCE_ROOT = "/edu/illinois/cs/cogcomp/illinoisEE";

    public MRResourceManager( String configFile_ ) throws IOException
    {
        super( configFile_ );
    }

    public MRResourceManager( Properties props_ )
    {
        super( props_ );
    }

    public boolean getUtilizeVerbSRL()
    {
        return super.getBoolean( "verb-srl");
    }

    public boolean getUtilizeNomSRL()
    {
        return super.getBoolean("nom-srl");
    }

    public String getStrudelIndexDir() {
        return super.getString("strudel-index-dir");
    }

    public boolean getLoadStrudelClassifier()
    {
        return super.getBoolean("load-strudel-classifier");
    }

    public URL getNEMentionModelResource()
    {
        URL url = MRResourceManager.class.getResource( RESOURCE_ROOT + "/IC_models/mention.model");
        return url;
    }

    public URL getNEMentionLexResource() {
        URL url = MRResourceManager.class.getResource( RESOURCE_ROOT + "/IC_models/mention.lex");
        return url;
    }

    public URL getACEEventPredicateModelResource() {
        URL url = MRResourceManager.class.getResource( RESOURCE_ROOT + "/predicate/ace_predicate.model");
        return url;
    }

    public URL getACEEventPredicateLexResource() {
        URL url = MRResourceManager.class.getResource("/edu/illinois/cs/cogcomp/illinoisEE/predicate/ace_predicate.lex");
        return url;
    }

    public URL getREModelResource() {
        URL url = MRResourceManager.class.getResource( RESOURCE_ROOT + "/IC_models/re.model");
        return url;
    }

    public URL getRELexResource() {
        URL url = MRResourceManager.class.getResource( RESOURCE_ROOT + "/IC_models/re.lex");
        return url;
    }


    public String getProjectRoot() {
        String projectRoot = null;
        try {
            projectRoot = IOUtils.pwd();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int len = projectRoot.length();
        if (projectRoot.substring(len - 4, len).equals("dist"))
            projectRoot += "/../";
        return projectRoot;
    }

    public String getProjectResourceRoot() {
        return getProjectRoot()+RESOURCE_ROOT;
    }

    public String getIlpDir() {
        return getProjectRoot()+"/ilp";
    }

}


