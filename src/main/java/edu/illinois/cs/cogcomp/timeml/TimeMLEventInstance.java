package edu.illinois.cs.cogcomp.timeml;


public class TimeMLEventInstance {
    private String id;
    private String eventId;
    private String signalId;
    private String tense;
    private String aspect;
    private String polarity;
    private String pos;
    private String modality;
    private String cardinality;

    public TimeMLEventInstance(String id) {
        this.id = id;
        eventId = null;
        signalId = null;
        tense = null;
        aspect = null;
        polarity = null;
        pos = null;
        modality = null;
        cardinality = "1";
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
    public String getEventId() {
        return eventId;
    }

    public void setSignalId(String signalId) {
        this.signalId = signalId;
    }
    public String getSignalId() {
        return signalId;
    }

    public void setTense(String tense) {
        this.tense = tense;
    }
    public String getTense() {
        return tense;
    }

    public void setAspect(String aspect) {
        this.aspect = aspect;
    }
    public String getAspect() {
        return aspect;
    }

    public void setPolarity(String polarity) {
        this.polarity = polarity;
    }
    public String getPolarity() {
        return polarity;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }
    public String getPos() {
        return pos;
    }

    public void setModality(String modality) {
        this.modality = modality;
    }
    public String getModality() {
        return modality;
    }

    public void setCardinality(String cardinality) {
        this.cardinality = cardinality;
    }
    public String getCardinality() {
        return cardinality;
    }


}


