package edu.illinois.cs.cogcomp.timeml;

import java.io.IOException;
import java.util.List;

import org.apache.thrift.TException;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;

/**
 * @author dxquang Oct 31, 2011
 */
public class TimeML {
    public static String configFile = "temporalreasoning.config";

    public static void main(String[] args) throws IOException, TException,
            ServiceUnavailableException, AnnotationFailedException {
        String filename = "/Users/Shyam/timebank_1_2/data/timeml/ABC19980108.1830.0711.tml";
        TimeMLDoc doc = TimeMLReader.readAnnotations(filename);
        String text = doc.getText();
        System.out.println("Text: " + text);

        List<String> sentences = MySentenceSplitter.sentenceSplit(text);
        for (String sent : sentences) {
            System.out.println(sent);
        }

        // Recovering sentence char offsets
        List<Pair<String, Pair<Integer, Integer>>> sentsWithCharOffset = MySentenceSplitter
                .recoverSentCharOffset(text, sentences);
        for (int i = 0; i < sentsWithCharOffset.size(); i++) {
            Pair<String, Pair<Integer, Integer>> s = sentsWithCharOffset.get(i);
            System.out.println(i + ". " + s.getFirst());
            System.out.println(s.getSecond());
        }

        // Events and Times
        List<Mention> events = doc.getEvents();
        for (Mention event : events) {
            TimeMLEvent e = (TimeMLEvent) event;
            System.out.println("TimeMLEvent: " + e);
            int start = e.getCharStartSpan();
            int end = e.getCharEndSpan();
            System.out.println("\tText: " + text.substring(start, end));
        }
        List<Mention> times = doc.getTimes();
        for (Mention time : times) {
            Time t = (Time) time;
            System.out.println("Time: " + t);
            System.out.println("\tValue: " + t.getValue());
        }
        Mention docTime = doc.getDocTime();
        System.out.println("DCT = " + docTime.toString());
        for (int i = 0; i < sentsWithCharOffset.size(); i++) {
            Pair<String, Pair<Integer, Integer>> s = sentsWithCharOffset.get(i);
            int startOff = s.getSecond().getFirst();
            int endOff = s.getSecond().getSecond();
            System.out.println((i + 1) + ". " + s.getFirst());
            System.out.println(s.getSecond());
            System.out.print("Events: ");
            for (int j = 0; j < events.size(); j++) {
                TimeMLEvent e = (TimeMLEvent) events.get(j);
                int start = e.getCharStartSpan();
                int end = e.getCharEndSpan();
                if (start >= startOff && end <= endOff) {
                    System.out.print(" " + e.getId() + "="
                            + text.substring(start, end) + " (" + start + ","
                            + end + "),");
                }
//                for(TimeMLEventInstance inst:e.getEventInstances())
//                {
//                    System.out.println("instance "+inst.toString()+" "+inst.getTense());
//                }
            }
            System.out.println();
            System.out.print("Times: ");
            for (int j = 0; j < times.size(); j++) {
                Time t = (Time) times.get(j);
                int start = t.getCharStartSpan();
                int end = t.getCharEndSpan();
                if (start >= startOff && end <= endOff) {
                    System.out.print(" " + t.getId() + "="
                            + text.substring(start, end) + " (" + t.getValue()
                            + "," + start + "," + end + "),");
                }
            }
            System.out.println();
        }

        System.out.println("TLINKS:");
        List<String> tLinks = doc.getTLinkLines();
        for (String t : tLinks) {
            System.out.println(t);
        }
    }
}
