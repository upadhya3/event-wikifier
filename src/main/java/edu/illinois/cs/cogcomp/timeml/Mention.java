package edu.illinois.cs.cogcomp.timeml;

import java.io.Serializable;
import java.util.List;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;

public abstract class Mention implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8250799650797625105L;
    protected String id;
    protected String label;
    protected List<Pair<String, Double>> labelProbs;

    protected int charStartSpan;
    protected int charEndSpan;
    protected int charHeadStartSpan;
    protected int charHeadEndSpan;

    protected int tokenStartSpan;
    protected int tokenEndSpan;
    protected int tokenHeadStartSpan;
    protected int tokenHeadEndSpan;

    protected int headTokenOffset;
    protected String headPos; // part of speech of the head word

    protected String surfaceString;

    protected Constituent c;

    protected boolean gold; // whether this is a gold, or predicted mention

    public Mention(String id) {
        this.id = id;
        label = null;

        charStartSpan = -1;
        charEndSpan = -1;
        charHeadStartSpan = -1;
        charHeadEndSpan = -1;

        tokenStartSpan = -1;
        tokenEndSpan = -1;
        tokenHeadStartSpan = -1;
        tokenHeadEndSpan = -1;

        headTokenOffset = -1;
        headPos = null;

        surfaceString = null;

        c = null;

        gold = false;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabelProbs(List<Pair<String, Double>> probs) {
        labelProbs = probs;
    }

    public List<Pair<String, Double>> getLabelProbs() {
        return labelProbs;
    }

    public void setCharSpan(int start, int end) {
        charStartSpan = start;
        charEndSpan = end;
    }

    public void setCharStartSpan(int i) {
        charStartSpan = i;
    }

    public void setCharEndSpan(int i) {
        charEndSpan = i;
    }

    public int getCharStartSpan() {
        return charStartSpan;
    }

    public int getCharEndSpan() {
        return charEndSpan;
    }

    public void setCharHeadSpan(int start, int end) {
        charHeadStartSpan = start;
        charHeadEndSpan = end;
    }

    public void setCharHeadStartSpan(int i) {
        charHeadStartSpan = i;
    }

    public void setCharHeadEndSpan(int i) {
        charHeadEndSpan = i;
    }

    public int getCharHeadStartSpan() {
        return charHeadStartSpan;
    }

    public int getCharHeadEndSpan() {
        return charHeadEndSpan;
    }

    public void setTokenStartSpan(int i) {
        tokenStartSpan = i;
    }

    public int getTokenStartSpan() {
        return tokenStartSpan;
    }

    public void setTokenEndSpan(int i) {
        tokenEndSpan = i;
    }

    public int getTokenEndSpan() {
        return tokenEndSpan;
    }

    public void setTokenHeadStartSpan(int i) {
        tokenHeadStartSpan = i;
    }

    public int getTokenHeadStartSpan() {
        return tokenHeadStartSpan;
    }

    public void setTokenHeadEndSpan(int i) {
        tokenHeadEndSpan = i;
    }

    public int getTokenHeadEndSpan() {
        return tokenHeadEndSpan;
    }

    public int getHeadTokenOffset() {
        return headTokenOffset;
    }

    public void setHeadPos(String pos) {
        headPos = pos;
    }

    public String getHeadPos() {
        return headPos;
    }

    public void setSurfaceString(String surfaceString) {
        this.surfaceString = surfaceString;
    }

    public String getSurfaceString() {
        return surfaceString;
    }

    public void setConstituent(Constituent c) {
        this.c = c;
        // this.headTokenOffset = MentionUtil.findHeadTokenOffset(c);
        TokenLabelView posView = (TokenLabelView) c.getTextAnnotation()
                .getView(ViewNames.POS);
        this.headPos = posView.getLabel(headTokenOffset);

        charStartSpan = c.getStartCharOffset();
        charEndSpan = c.getEndCharOffset();
        tokenStartSpan = c.getStartSpan();
        tokenEndSpan = c.getEndSpan();

    }

    public Constituent getConstituent() {
        return c;
    }

    public void setGold(boolean gold) {
        this.gold = gold;
    }

    public boolean isGold() {
        return gold;
    }

    public int getSentenceId() {
        return c.getSentenceId();
    }

    public String toString() {
        StringBuffer s = new StringBuffer("");
        s.append("id=" + id + " label=" + label);
        s.append(" charExtend=" + charStartSpan + "," + charEndSpan
                + " charHead=" + charHeadStartSpan + "," + charHeadEndSpan);
        s.append(" tokenExtend=" + tokenStartSpan + "," + tokenEndSpan
                + " tokenHead=" + tokenHeadStartSpan + "," + tokenHeadEndSpan);
        s.append(" [" + surfaceString + "]");
        return s.toString();
    }
}