package edu.illinois.cs.cogcomp.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.factory.VerbVoiceIndicator;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.esrl.specificity.lbjava.SpecificityClassifier;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.readers.LazyNYTReader;

/**
 * Created by upadhya3 on 11/23/15.
 */
public class TAUtils {


	public static String getTense(Constituent cons) {
		TextAnnotation ta = cons.getTextAnnotation();
		int start=cons.getSpan().getFirst();
		int end=cons.getSpan().getSecond();

		TokenLabelView posview = (TokenLabelView) ta.getView(ViewNames.POS);

		Constituent poscon = posview.getConstituentsCovering(cons).get(0);
		Constituent prevcon = null;
		if(start>0)
		{
			prevcon = posview.getConstituentsCoveringSpan(start-1, end-1).get(0);
		}
		String postag = poscon.getLabel();
		String tense ="";
//		System.out.println("---------TENSE---------- "+ cons);
		if(postag.equals("VBP") || postag.equals("VBZ")) // singular present
		{
			if(prevcon!=null && prevcon.getSurfaceForm().equals("be")) // will be ...
			{
//				System.out.println("FUTURE:"+cons);
				tense="future";
			}
			else
			{
//				System.out.println("PRESENT:"+cons);
				tense="present";
			}
		}
		if(postag.equals("VBN")) // past participle, "agreed"
		{

		}
		if(postag.equals("VBD")) // verb past tense
		{
//			System.out.println("PAST:"+cons);
			tense="past";
			if(prevcon!=null) {
//				System.out.println(prevcon);
			}
		}
		if(postag.equals("VBG"))
		{
			if(prevcon!=null && prevcon.getLabel().equals("MD"))
			{
//				System.out.println(prevcon+" modal");
//				System.out.println("FUTURE:"+prevcon);
			}
		}
		if(postag.equals("VB"))
		{

		}
		return tense;
	}

	public static List<Constituent> getSubOrdClauses(TextAnnotation ta) {
		List<Constituent> ans = new ArrayList<>();
		TreeView tree = (TreeView) ta.getView(ViewNames.PARSE_STANFORD);
		for (Constituent cons : tree.getConstituents()) {
			if (cons.getLabel().equals("SBAR")) {
				// System.out.println(cons+" "+cons.getLabel());
				ans.add(cons);
			}
		}
		return ans;
	}

	static String[] relatives = {"what", "who", "whom", "that", "which", "where", "whose", "whether"};

	public static List<Constituent> getRelativeClauses(TextAnnotation ta, List<Constituent> subOrdClauses) {
		List<Constituent> ans = new ArrayList<>();
		TokenLabelView tokens = (TokenLabelView) ta.getView(ViewNames.TOKENS);
		for (Constituent cons : subOrdClauses) {
			List<Constituent> toks = tokens.getConstituentsCoveringSpan(cons.getStartSpan(), cons.getEndSpan());
//		System.out.println("ZZZZ "+toks.get(0)+" "+cons);
			String surface = toks.get(0).getSurfaceForm();
			if (Arrays.asList(relatives).contains(surface)) {
//			System.out.println("RELATIVE!"+cons);
				ans.add(cons);
			}
		}
		return ans;
	}

	public static String getPos(Constituent c) {
		TextAnnotation ta = c.getTextAnnotation();
		TokenLabelView pos= (TokenLabelView) ta.getView(ViewNames.POS);
		List<Constituent> cons = pos.getConstituentsCovering(c);
		assert cons.size()==1;
		return cons.get(0).getLabel();
	}
	public static String getLemma(Constituent c){
		TextAnnotation ta = c.getTextAnnotation();
		TokenLabelView lemma = (TokenLabelView) ta.getView(ViewNames.LEMMA);
		List<Constituent> cons = lemma.getConstituentsCovering(c);
		assert cons.size()==1;
		return cons.get(0).getLabel();

	}
	public static String getFrame(Constituent c) {
		TextAnnotation ta = c.getTextAnnotation();
		SpanLabelView lemma = (SpanLabelView) ta.getView("FRAMENET_FRAMES");
		List<Constituent> cons = lemma.getConstituentsCovering(c);
		if(cons.size()==1)
			return cons.get(0).getLabel();
		else
			return "NO_FRAME";

	}

	public static List<Constituent> getNounPhrase(Constituent arg1) {
		TextAnnotation ta = arg1.getTextAnnotation();
		SpanLabelView chunk= (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);
		List<Constituent> chunkcons = chunk.getConstituentsCovering(arg1);
		return chunkcons;
	}

	public static List<Constituent> getCorefWord(Constituent arg1) {
		TextAnnotation ta = arg1.getTextAnnotation();
		CoreferenceView coref = (CoreferenceView) ta.getView(ViewNames.COREF);
		SpanLabelView ner = (SpanLabelView) ta.getView(ViewNames.NER_CONLL);
		List<Constituent> corefcons = coref.getConstituentsCovering(arg1);
//		if (corefcons != null) {
//			for (Constituent cons : corefcons) {
//				Constituent canonical = coref.getCanonicalEntity(cons);
//				ner.getConstituentsCovering(canonical);
//			}
//		}
		return corefcons;
	}

	public static Sentence getSentence(Constituent cons) {
		TextAnnotation ta = cons.getTextAnnotation();
		Sentence sent = ta.getSentence(cons.getSentenceId());
		return sent;
	}


	public static String[] getTokensInWindow(Constituent cons, int i, String direction) {
        TextAnnotation ta = cons.getTextAnnotation();
        int start=cons.getStartSpan();
        int end=cons.getEndSpan();
        String [] window = null;
        if(direction.equals("L")) // left window
        {
        	window= ta.getTokensInSpan(Math.max(start-i, 0), start);
        }
        if(direction.equals("R")) // right window
        {
        	window= ta.getTokensInSpan(end,Math.min(end+i, ta.getTokens().length));
        }
        return window;	
	}

	public static boolean isInAppositive(Constituent cons)
	{
		boolean ans= false;
		TextAnnotation ta = cons.getTextAnnotation();
		int start=cons.getStartSpan();
		int end=cons.getEndSpan();

		PredicateArgumentView pav = (PredicateArgumentView) ta.getView("SRL_COMMA");
		List<Constituent> commas=getAppositiveCommas(pav.getPredicates());
		for(Constituent comma:commas)
		{
			List<Relation> relations = comma.getOutgoingRelations();
			for(Relation rel:relations)
			{
				if(rel.getTarget().doesConstituentCover(cons))
				{
					ans=true;
				}
			}
		}
		return ans;
	}

	private static List<Constituent> getAppositiveCommas(List<Constituent> predicates) {
		List<Constituent> ans = new ArrayList<>();
		for(Constituent predicate:predicates) {
			if (predicate.getSurfaceForm().equals(","))
			{
				if(predicate.getLabel().equals("Substitute")
						|| predicate.getLabel().equals("Attribute")
						|| predicate.getLabel().equals("Introductory"))
					ans.add(predicate);
			}
		}
//		System.out.println(ans.size());
		return ans;
	}

	public static String[] getPosInWindow(Constituent cons, int i, String direction) {
		TextAnnotation ta = cons.getTextAnnotation();
		int start=cons.getStartSpan();
		int end=cons.getEndSpan();
		String [] window = null;
		TokenLabelView posview = (TokenLabelView) ta.getView(ViewNames.POS);
		List<Constituent> win = null;
		if(direction.equals("L")) // left window
		{
//        	window= ta.getTokensInSpan(Math.max(start-i, 0), start-1);
        	win= posview.getConstituentsCoveringSpan(Math.max(start-i, 0), start);
        }
        if(direction.equals("R")) // right window
        {
//        	window= ta.getTokensInSpan(end+1,Math.min(end+i, ta.getTokens().length));
        	win= posview.getConstituentsCoveringSpan(end,Math.min(end+i, ta.getTokens().length));
        }
    	List<String>tmp= new ArrayList<>();
    	for(Constituent ll:win)
    	{
    		tmp.add(ll.getLabel());
    	}
    	window = tmp.toArray(new String[tmp.size()]);

        return window;	
	}

	// TODO: make this efficient
	public static String getClauseNature(Constituent cons) {
		TextAnnotation ta = cons.getTextAnnotation();
		List<Constituent> subOrdClauses = TAUtils.getSubOrdClauses(ta);
		List<Constituent> relativeClauses = TAUtils.getRelativeClauses(ta,
				subOrdClauses);
		boolean insideSubord = false;
		boolean insideRelative = false;
		for (Constituent clause : subOrdClauses) {
			if (clause.doesConstituentCover(cons)) {
				insideSubord = true;
				break;
			}
		}
		for (Constituent clause : relativeClauses) {
			if (clause.doesConstituentCover(cons)) {
				insideRelative = true;
				break;
			}
		}
		if (insideRelative) return "relative";
		if(insideSubord) return "subord";
		return "main";
	}

	/**
	 * returns [A],[P] or [X]
	 * @param verb
	 * @return
	 * @throws EdisonException
	 */
	public static Feature voiceFeat(Constituent verb) throws EdisonException {
		return voice.getFeatures(verb).toArray(new Feature[1])[0];
	}

	public static VerbVoiceIndicator voice = new VerbVoiceIndicator(ViewNames.PARSE_STANFORD);


	public static List<Constituent> filterPredicates(List<Constituent> cons) {
		List<Constituent> ans = new ArrayList<>();
		for(Constituent con:cons) {
			if (!con.getLabel().equals("Predicate"))
				continue;
			ans.add(con);
		}
		return ans;
	}

	public static void main(String[] args) throws Exception {
	    SpecificityClassifier classifier = new SpecificityClassifier("/home/upadhya3/spec.model","/home/upadhya3/spec.lex");

		LazyNYTReader reader = new LazyNYTReader();
		AnnotatedDocument doc = reader.next();
		while(doc!=null)
		{
			TextAnnotation ta = doc.getBodyTA();
			for (int i = 0; i < ta.getNumberOfSentences(); i++) {
				Constituent cons = ta.getSentence(i).getSentenceConstituent();
				System.out.println(classifier.discreteValue(cons));
			}
			doc=reader.next();
		}
//		PredicateArgumentView verb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
//		PredicateArgumentView nom = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
//		PredicateArgumentView com = (PredicateArgumentView) ta.getView("SRL_COMMA");
//
//		System.out.println(verb.getPredicates());
//		System.out.println(nom.getPredicates());
//
//		for(Constituent cons:com.getPredicates())
//		{
//			System.out.println(cons+" "+cons.getLabel());
//			for(Relation rel: cons.getOutgoingRelations())
//				System.out.println(rel.getTarget()+" "+rel.getTarget().getLabel());
//			classifier.discreteValue(cons);
//			System.out.println("--------");
//		}
//		for(Constituent cons:verb.getPredicates())
//		{
//			System.out.println(cons+" "+TAUtils.isInAppositive(cons));
//		}
//		System.exit(-1);

	}

	static SpecificityClassifier classifier;
	static {
		classifier = new SpecificityClassifier("/home/upadhya3/spec.model","/home/upadhya3/spec.lex");
	}
	public static String getSentenceSpecificity(Constituent pred_srl) {

		int sid = pred_srl.getSentenceId();
		TextAnnotation ta = pred_srl.getTextAnnotation();
		Constituent sent = ta.getSentence(sid).getSentenceConstituent();
		return classifier.discreteValue(sent);
	}

	public static boolean isFirst(EventInstance ee, List<EventInstance> others){
		for(EventInstance ev:others)
		{
				if (ev.start < ee.start && ev.end < ee.end)
					return false;
		}
		return true;
	}
	public static boolean isFirstNonAppVerbPredicateInList(EventInstance ee, List<EventInstance> others) {
		if(!ee.pred_srl.getViewName().equals("SRL_VERB"))
		{
			return false;
		}
		if(TAUtils.isInAppositive(ee.pred_srl))
		{
			return false;
		}
		for(EventInstance ev:others)
		{
			if(!TAUtils.isInAppositive(ev.pred_srl)) {
				if (ev.start < ee.start && ev.end < ee.end && ev.pred_srl.getViewName().equals("SRL_VERB"))
					return false;
			}
		}
		return true;
	}
}
