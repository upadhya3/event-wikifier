// Modifying this comment will cause the next execution of LBJava to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294550FDA4F94C4A4DC1D07D2B4DCB21FCCB2E294CCB4E455828C82A4C2E45D450B1D558A6500AAB2D2AC38A89E51726E462059A5B24D20002DA48B1644000000

package edu.illinois.cs.cogcomp.lbjsrc;

import edu.illinois.cs.cogcomp.features.*;
import edu.illinois.cs.cogcomp.lbjava.classify.*;
import edu.illinois.cs.cogcomp.lbjava.infer.*;
import edu.illinois.cs.cogcomp.lbjava.io.IOUtilities;
import edu.illinois.cs.cogcomp.lbjava.learn.*;
import edu.illinois.cs.cogcomp.lbjava.parse.*;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;


public class MyLabel extends Classifier
{
  public MyLabel()
  {
    containingPackage = "edu.illinois.cs.cogcomp.lbjsrc";
    name = "MyLabel";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance"; }
  public String getOutputType() { return "discrete"; }


  public FeatureVector classify(Object __example)
  {
    return new FeatureVector(featureValue(__example));
  }

  public Feature featureValue(Object __example)
  {
    String result = discreteValue(__example);
    return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result), (short) allowableValues().length);
  }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof EventInstance))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'MyLabel(EventInstance)' defined on line 6 of MyClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    EventInstance phrase = (EventInstance) __example;

    return "" + (phrase.salient);
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof EventInstance[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'MyLabel(EventInstance)' defined on line 6 of MyClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "MyLabel".hashCode(); }
  public boolean equals(Object o) { return o instanceof MyLabel; }
}

