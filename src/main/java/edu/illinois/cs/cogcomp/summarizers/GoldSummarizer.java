package edu.illinois.cs.cogcomp.summarizers;

import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;

/**
 * Created by upadhya3 on 2/12/16.
 */
public class GoldSummarizer extends Summarizer{
    @Override
    public String getSummary(NYTCorpusDocument nydoc) {
        return nydoc.articleAbstract;
    }
}
