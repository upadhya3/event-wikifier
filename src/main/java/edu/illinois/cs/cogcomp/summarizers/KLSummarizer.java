package edu.illinois.cs.cogcomp.summarizers;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;

import java.io.FileNotFoundException;

/**
 * Created by upadhya3 on 2/12/16.
 */
public class KLSummarizer extends Summarizer {
    private int len;
    private String summdir = "/shared/bronte/upadhya3/NYTsummaries/kl/";
    private String suffix;

    public KLSummarizer(int len)
    {
        this.len=len;
        this.suffix="_body.txt.kldiv."+len+".txt";
    }

    public KLSummarizer(String summdir, String suffix)
    {
        this.summdir=summdir;
        this.suffix=suffix;
    }
    @Override
    public String getSummary(NYTCorpusDocument nydoc) throws FileNotFoundException {
        String summ = LineIO.slurp(summdir + "/" + nydoc.getGuid() + suffix);
        return summ;
    }
}
