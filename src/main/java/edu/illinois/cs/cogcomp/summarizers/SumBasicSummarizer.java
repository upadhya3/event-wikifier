package edu.illinois.cs.cogcomp.summarizers;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;

import java.io.FileNotFoundException;

/**
 * Created by Shyam on 2/16/16.
 */
public class SumBasicSummarizer extends Summarizer {
    private int len;
    private String summdir = "/shared/bronte/upadhya3/NYTsummaries/sb/";
    private String suffix;

    public SumBasicSummarizer(int len) {
        this.len=len;
        this.suffix="_body.txt.sumbasic."+len+".txt";
    }

    public SumBasicSummarizer(String summdir, String suffix) {
        this.summdir = summdir;
        this.suffix = suffix;
    }

    @Override
    public String getSummary(NYTCorpusDocument nydoc) throws FileNotFoundException {
        String summ = LineIO.slurp(summdir + "/" + nydoc.getGuid() + suffix);
        return summ;
    }
}