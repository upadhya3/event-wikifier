package edu.illinois.cs.cogcomp.summarizers;

import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;

import java.io.FileNotFoundException;

/**
 * Created by upadhya3 on 2/12/16.
 */
public abstract class Summarizer {
    public abstract String getSummary(NYTCorpusDocument nydoc) throws FileNotFoundException;
}
