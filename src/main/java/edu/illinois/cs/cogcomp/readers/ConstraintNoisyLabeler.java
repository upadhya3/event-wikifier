package edu.illinois.cs.cogcomp.readers;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.heurisitic.Heuristic;
import edu.illinois.cs.cogcomp.heurisitic.VoiceClauseHeuristic;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.Evaluator;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

/**
 * Created by Shyam on 2/22/16.
 */
public class ConstraintNoisyLabeler extends AbstractLabeler{
    public final LazyNYTReader reader;
    private Heuristic h;

    public ConstraintNoisyLabeler(LazyNYTReader reader, Heuristic h) {
        this.reader=reader;
        this.size=reader.docids.size();
        this.h=h;
    }

    @Override
    public AnnotatedDocument next() {
        AnnotatedDocument doc = reader.next();
        if(doc!=null) {
            for (EventInstance e : doc.abs_events) {
                try {
                    h.apply(e,doc.abs_events);
                } catch (EdisonException e1) {
                    e1.printStackTrace();
                    System.err.println("Sometimes ... some crimes ...");
                }
            }
        }
        return doc;
    }

    @Override
    public void reset() {
        reader.reset();
    }

    @Override
    public void close() {

    }

    public static void main(String[] args) throws Exception {
        LazyNYTReader docReader = new LazyNYTReader();
        AbstractLabeler labeler = new ConstraintNoisyLabeler(docReader, new VoiceClauseHeuristic());
        AnnotatedDocument doc = (AnnotatedDocument) labeler.next();
        while(doc!=null)
        {
            System.out.println(doc.absTA.getText());
            System.out.println("abs:"+doc.abs_events);
//            System.out.println("body:"+doc.body_events);
            doc= (AnnotatedDocument) labeler.next();
        }
    }
}
