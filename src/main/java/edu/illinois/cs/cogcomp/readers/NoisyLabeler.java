package edu.illinois.cs.cogcomp.readers;

import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.Evaluator;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.util.List;

/**
 * Created by upadhya3 on 2/12/16.
 */
public class NoisyLabeler extends AbstractLabeler{

    public final LazyNYTReader reader;

    public NoisyLabeler(LazyNYTReader reader) {
        this.reader=reader;
        this.size=reader.docids.size();
    }

    @Override
    public AnnotatedDocument next() {
        AnnotatedDocument doc = reader.next();
        if(doc!=null) {
            for (EventInstance e : doc.abs_events) {
                e.salient = 1;
            }
//            int proj = Evaluator.projectLabelToDoc(doc, doc.abs_events);
//            System.out.println("proj:"+proj);
        }
        return doc;
    }

    @Override
    public void reset() {
        reader.reset();
    }

    @Override
    public void close() {

    }

    public static void main(String[] args) throws Exception {
        LazyNYTReader docReader = new LazyNYTReader();
        AbstractLabeler labeler = new NoisyLabeler(docReader);
        AnnotatedDocument doc = (AnnotatedDocument) labeler.next();
        while(doc!=null)
        {
            System.out.println("abs:"+doc.abs_events);
            System.out.println("body:"+doc.body_events);
            doc= (AnnotatedDocument) labeler.next();
        }
    }
}
