package edu.illinois.cs.cogcomp.readers;

import edu.illinois.cs.cogcomp.features.MyTrainer;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shyam on 2/17/16.
 */
public class FlatReader implements Parser{

    private final AbstractLabeler labeler;
    private final boolean isTrain;
    public int curr=0;
    public int curr_event=0;
    public AnnotatedDocument currdoc;
    public FlatReader(AbstractLabeler l,boolean isTrain)
    {
        labeler = l;
        this.isTrain=isTrain;
        currdoc= (AnnotatedDocument) labeler.next();
        if(isTrain)
            BalanceDoc(currdoc);
//        System.out.println(currdoc.nydoc.getGuid());
    }
    @Override
    public Object next() {
        if(curr_event<currdoc.abs_events.size())
        {
            return currdoc.abs_events.get(curr_event++);
        }
        else if(curr<labeler.size)
        {
            while(true)
            {
                currdoc= (AnnotatedDocument) labeler.next();
                if(currdoc==null)
                    return null;
                if(isTrain)
                    BalanceDoc(currdoc);
                curr_event=0;
                curr++;
                if(curr_event<currdoc.abs_events.size())
                {
                    return currdoc.abs_events.get(curr_event++);
                }
            }
        }
        else
            return null;
    }

    @Override
    public void reset() {
        curr=0;
        curr_event=0;
        labeler.reset();
        currdoc= (AnnotatedDocument) labeler.next();
        if(isTrain)
            BalanceDoc(currdoc);
//        System.out.println(currdoc.nydoc.getGuid());

    }

    private void BalanceDoc(AnnotatedDocument curr) {
        List<EventInstance> ee = curr.abs_events;
        List<EventInstance> pos=new ArrayList<>(),neg=new ArrayList<>(), ans = new ArrayList<>();
        for(EventInstance e: ee)
        {
            if(e.salient==1)
                pos.add(e);
            else
                neg.add(e);
        }
//        System.out.println("pos:"+pos.size()+" neg:"+neg.size()+" in doc");
        int i=0;
        List<EventInstance> bigger,smaller;
        bigger = pos.size()> neg.size() ? pos : neg;
        smaller = pos.size() <= neg.size() ? pos : neg;
        for(EventInstance pp:smaller)
        {
            ans.add(pp);
            ans.add(bigger.get(i++));
        }
        if(ans.size()>0)
            curr.abs_events=ans;
        else
            return;
    }

    @Override
    public void close() {

    }

    public static void main(String[] args) throws Exception {
        boolean alreadyCached = true;
        List<String> docids = AnnotateAbstracts.getTrainIds("world.txt"); //Evaluator.getGoldDocIds();
        List<String> train_ids = docids.subList(0, 100);
        List<String> test_ids = docids.subList(100, 200);
        System.out.println("train size:"+train_ids.size());
        System.out.println("test size:"+test_ids.size());


        MyCuratorClient cc = new MyCuratorClient(alreadyCached);
        LazyNYTReader d1= new LazyNYTReader(cc, alreadyCached, train_ids);
        LazyNYTReader d2= new LazyNYTReader(cc, alreadyCached, test_ids);

        NoisyLabeler trainReader = new NoisyLabeler(d1);
        NoisyLabeler testReader = new NoisyLabeler(d2);

//        NoisySupervisionReader trainReader = new NoisySupervisionReader(new NoisyLabeler(d1), true);
//        NoisySupervisionReader testReader = new NoisySupervisionReader(new NoisyLabeler(d2), false);

//        MyTrainer.getDist(trainReader);
//        MyTrainer.getDist(testReader);
//        trainReader.reset();
//        testReader.reset();
//        MyTrainer.getDist(trainReader);
//        MyTrainer.getDist(testReader);
        FlatReader fr1 = new FlatReader(trainReader, false);
        FlatReader fr2 = new FlatReader(testReader, false);
        MyTrainer.getDist(fr1);
        MyTrainer.getDist(fr2);
        System.out.println("resetting ");
        trainReader.reset();
        testReader.reset();
        fr1.reset();
        fr2.reset();

//        FlatReader fr3 = new FlatReader(trainReader, false);
//        FlatReader fr4 = new FlatReader(testReader, false);
        MyTrainer.getDist(fr1);
        MyTrainer.getDist(fr2);

    }
}
