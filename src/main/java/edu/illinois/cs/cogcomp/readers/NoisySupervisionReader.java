//package edu.illinois.cs.cogcomp.readers;
//
//import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
//import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
//import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
//import edu.illinois.cs.cogcomp.nytlabs.corpus.core.Evaluator;
//import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by upadhya3 on 2/12/16.
// */
//public class NoisySupervisionReader implements Parser{
//    private final NoisyLabeler reader; // possibly decorated reader
//    private final boolean isTrain;
//    private AnnotatedDocument curr = null;
//    private int curr_in_doc = 0;
//    public NoisySupervisionReader(Parser reader, boolean isTrain)
//    {
//        this.reader = reader;
//        curr = (AnnotatedDocument) reader.next();
//        this.isTrain = isTrain;
//    }
//    @Override
//    public Object next() {
//        EventInstance i = getNextFromDoc(curr);
//        if(i!=null) // just return it
//        {
//            return i;
//        }
//        else // null means doc is done/empty
//        {
//            while(i==null) {
//                curr = (AnnotatedDocument) reader.next();
//                if (curr == null) // last doc seen
//                {
//                    return null;
//                } else // new doc, start afresh
//                {
//                    if(isTrain)
//                        BalanceDoc(curr);
//                    curr_in_doc = 0;
////                    System.out.println(curr.nydoc.getGuid());
//                    i=getNextFromDoc(curr);
//                }
//            }
//            return i;
//        }
//    }
//
//    private void BalanceDoc(AnnotatedDocument curr) {
//        List<EventInstance> ee = curr.body_events;
//        List<EventInstance> pos=new ArrayList<>(),neg=new ArrayList<>(), ans = new ArrayList<>();
//        for(EventInstance e: ee)
//        {
//            if(e.salient==1)
//                pos.add(e);
//            else
//                neg.add(e);
//        }
////        System.out.println("pos:"+pos.size()+" neg:"+neg.size()+" in doc");
//        int i=0;
//        List<EventInstance> bigger,smaller;
//        bigger = pos.size()> neg.size() ? pos : neg;
//        smaller = pos.size() <= neg.size() ? pos : neg;
//        for(EventInstance pp:smaller)
//        {
//            ans.add(pp);
//            ans.add(bigger.get(i++));
//        }
//        curr.body_events=ans;
//    }
//
//    private EventInstance getNextFromDoc(AnnotatedDocument curr) {
//        if(curr==null)
//        {
//            return null;
//        }
//        List<EventInstance> ee = curr.body_events;
//        if(curr_in_doc<ee.size())
//        {
//            return ee.get(curr_in_doc++);
//        }
//        else
//            return null;
//    }
//
//    @Override
//    public void reset() {
//        reader.reset();
//        curr = (AnnotatedDocument) reader.next();
//    }
//
//    @Override
//    public void close() {
//
//    }
//
//    public static void main(String[] args) throws Exception {
//        boolean alreadyCached = true;
//        List<String> docids = new ArrayList<>();
////        docids.add("1453362");
////        docids.add("1453356");
////        docids.add("1453363");
//        docids = Evaluator.getGoldDocIds();
//        MyCuratorClient cc = new MyCuratorClient(alreadyCached);
//        LazyNYTReader docReader = new LazyNYTReader(cc, alreadyCached, docids);
////        AnnotatedDocument dd = docReader.next();
////        System.out.println(dd.abs_events);
//        NoisySupervisionReader reader = new NoisySupervisionReader(docReader, false);
//        EventInstance inst = (EventInstance) reader.next();
//        int i=0;
//        while(inst!=null)
//        {
//            inst= (EventInstance) reader.next();
////            System.out.println(inst);
//            System.out.println(i++);
//        }
//    }
//}
