package edu.illinois.cs.cogcomp.readers;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.features.AllFeatures;
import edu.illinois.cs.cogcomp.heurisitic.ClassifierHeuristic;
import edu.illinois.cs.cogcomp.heurisitic.TenseHeuristic;
import edu.illinois.cs.cogcomp.heurisitic.VoiceHeuristic;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.lbjsrc.MyClassifier;
import edu.illinois.cs.cogcomp.nytlabs.corpus.FramenetExtractor;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.*;
import edu.illinois.cs.cogcomp.summarizers.*;
import edu.knowitall.common.ling.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shyam on 12/22/15.
 */
public class LazyNYTReader implements Parser{
    private final Summarizer summarizer;
    public List<String> docids;
    private int curr=0;
    NYTCorpusDocumentParser pp;
    FramenetExtractor extractor;
    MyCuratorClient cc;
    boolean alreadyCached;

    public LazyNYTReader() throws Exception {
        this(new MyCuratorClient(true),true,Evaluator.getGoldDocIds());
    }

    public LazyNYTReader(Summarizer summ) throws Exception {
        this(new MyCuratorClient(true),true,Evaluator.getGoldDocIds(),summ);
    }
    public LazyNYTReader(MyCuratorClient cc, boolean alreadyCached, List<String> docids) throws FileNotFoundException {
        pp= new NYTCorpusDocumentParser();
        extractor = new FramenetExtractor();
        this.cc=cc;
        this.alreadyCached=alreadyCached;
        this.docids=docids;
        this.summarizer = new GoldSummarizer();
    }

    /***
     * @param cc
     * @param alreadyCached
     * @param docids
     * @throws FileNotFoundException
     */
    public LazyNYTReader(MyCuratorClient cc, boolean alreadyCached, List<String> docids, Summarizer summ) throws FileNotFoundException {
        pp= new NYTCorpusDocumentParser();
        extractor = new FramenetExtractor();
        this.cc=cc;
        this.alreadyCached=alreadyCached;
        this.docids=docids;
        this.summarizer = summ;
    }

    public LazyNYTReader(List<String> docids) throws Exception {
        this(new MyCuratorClient(true),true,docids);
    }

    public LazyNYTReader(List<String> trainIds, Summarizer summ) throws Exception {
        this(new MyCuratorClient(true),true, trainIds, summ);
    }

    public AnnotatedDocument next() {
        if (curr < docids.size())
        {
            try {
                AnnotatedDocument ans = processDoc(docids.get(curr));
                curr++;
                return ans;
            }
            catch (AnnotatorException e) {
                e.printStackTrace();
                return null;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
        else
            return null;
    }

    @Override
    public void reset() {
        curr=0;
    }

    @Override
    public void close() {

    }

    /**
     * works with gold summary from NYT
     * @param docid
     * @return
     * @throws AnnotatorException
     */
    AnnotatedDocument processDoc(String docid) throws AnnotatorException, FileNotFoundException {
        String textFile = Params.dataPath + "/" + docid + ".xml";
//        System.out.println("annotating "+docid);
        NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(
                new File(textFile), false);
        String reduced_body = "";
        List<TextAnnotation> paraTas= new ArrayList<>();
        for (int i = 0; i < Math.min(10, nydoc.paragraphs.size()); i++) {
            String para = nydoc.paragraphs.get(i);
            reduced_body += para + "\n";
            TextAnnotation paraTa = AnnotateAbstracts.annotate(para, cc, false);
            paraTas.add(paraTa);
        }
        TextAnnotation bodyTa = AnnotateAbstracts.annotate(reduced_body, cc, false);
        String summ = summarizer.getSummary(nydoc);

        TextAnnotation absTa;
        if(summarizer instanceof GoldSummarizer)
        {
            absTa = AnnotateAbstracts.annotate(summ, cc, true);
        }
        else
        {
            absTa= AnnotateAbstracts.annotate(summ, cc, false);
        }
        if(absTa==null || bodyTa ==null)
        {
            System.err.println("could not annotate ... "+ textFile);
            // System.exit(-1);
            return new AnnotatedDocument();
        }
        AnnotatedDocument doc = null;
        List<EventInstance> abs_events = extractor.extract(absTa);
        List<EventInstance> body_events = extractor.extract(bodyTa);
        List<List<EventInstance>>para_events = new ArrayList<>();
        for(TextAnnotation parata:paraTas)
        {
            para_events.add(extractor.extract(parata));
        }
        doc = new AnnotatedDocument(para_events, body_events, abs_events,paraTas, bodyTa,absTa);
        doc.setParents();
        doc.nydoc = nydoc;
//        doc.bestpara = extractGoldParagraphs(nydoc, cc);
        return doc;
    }

    public static void main(String[] args) throws Exception {
        boolean isCached = false;
        int len=3;
        List<String> lst = Evaluator.getGoldDocIds();
        LazyNYTReader reader = new LazyNYTReader(new MyCuratorClient(isCached), isCached, lst, new LexRankSummarizer(len));
        AnnotatedDocument doc = reader.next();
        while(doc!=null)
        {
//            System.out.println(doc.absTA);
            doc=reader.next();
        }

        reader = new LazyNYTReader(new MyCuratorClient(isCached), isCached, lst, new KLSummarizer(len));
        doc = reader.next();
        while(doc!=null)
        {
//            System.out.println(doc.absTA);
            doc=reader.next();
        }

        reader = new LazyNYTReader(new MyCuratorClient(isCached), isCached, lst, new LSASummarizer(len));
        doc = reader.next();
        while(doc!=null)
        {
//            System.out.println(doc.absTA);
            doc=reader.next();
        }
        reader = new LazyNYTReader(new MyCuratorClient(isCached), isCached, lst, new SumBasicSummarizer(len));
        doc = reader.next();
        while(doc!=null)
        {
//            System.out.println(doc.absTA);
            doc=reader.next();
        }

//        int totsalient = 0;
//        int zeros=0;
//        int i=0;
//        while(doc!=null)
//        {
//            List<EventInstance> goldann = Evaluator.readGoldData(doc,Params.goldPath + "/" + doc.nydoc.getGuid() + "_abs.ann","abs");
//            Evaluator.projectLabelToDoc(doc, goldann);
//            int docsalient = 0;
//            for (EventInstance bodyE : doc.body_events) {
//                if (bodyE.salient == 1)
//                    docsalient++;
//            }
//            if(docsalient==0) {
//                zeros++;
////                System.out.println("docsalient:" + docsalient +" "+doc.nydoc.getGuid());
//            }
//            totsalient+=docsalient;
//            System.out.println(doc.nydoc.getGuid());
//            doc=reader.next();
//            System.out.println(i++);
//        }
//        System.out.println("total salient "+totsalient);
//        System.out.println("docs with no salient proj "+zeros+"/"+docids.size());
//        Evaluator.evaluate(reader, new VoiceHeuristic("P"), "abs");
//        ClassifierHeuristic ch = new ClassifierHeuristic(new MyClassifier(), new AllFeatures());
//        Evaluator.evaluate(reader, ch,"abs");
//        System.out.println("prediction profile pos:"+ch.posPred+" neg:"+ch.negPred);
//        Evaluator.evaluate(reader, new TenseHeuristic("present"),"abs");
//        Evaluator.evaluate(reader, new VoiceClauseHeuristic(),"abs");
//        Evaluator.evaluate(reader, new ClauseHeuristic("relative"),"abs");
//        Evaluator.evaluate(reader, new ClauseHeuristic("main"),"abs");
//        Evaluator.evaluate(reader, new ClauseHeuristic("subord"),"abs");
    }




}
