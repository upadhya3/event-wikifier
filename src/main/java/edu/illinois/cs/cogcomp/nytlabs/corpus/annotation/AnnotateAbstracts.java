package edu.illinois.cs.cogcomp.nytlabs.corpus.annotation;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandDescription;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocumentParser;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by upadhya3 on 10/23/15.
 */
public class AnnotateAbstracts {

	// some errors are due to some unclosed file streams. Check for .nfs files
	// under annotationcache
    static String[] srlBreaks = { "1460640", "1459172", "1456970", "1460631",
				  "1457408", "1453133", "1459962", "1460007", "1454146", "1455969",
				  "1454376", "1457383", "1455478", "1454145", "1474578", "1487343",
				  "1514474", "1534482", "1518103", "1534397", "1453369", "1460629",
				  "1456654", "1460571",

				  "1453703","1453715","1519575",
				  "1467699",
				  "1474147",
				  "1467992",
				  "1465458",
				  "1465092",
				  "1508461",
				  "1508018",
				  "1516673",
				  "1522498",
				  "1537329",
				  "1539408",
				  "1511245",
				  "1493813",
				  "1527173",
				  "1528937",
				  "1485570",
				  "1540339",
				  "1476239",
				  "1473830",
				  "1473386",
				  "1476952",
				  "1472201",
				  "1475428",
				  "1483709",
				  "1481589",
				  "1458442",
				  "1463931",
				  "1465764",
				  "1471323",
				  "1475664",
				  "1531231",
				  "1546753",
				  "1462919",
				  "1469355",
				  "1486636",
				  "1516038"
	};

	public static List<String> getTrainIds(String filename) throws FileNotFoundException {
		List<String> docids = new ArrayList<>();
		List<NYAbstracts> abstracts = AnnotateAbstracts.readAbstracts(filename, -1);
		System.out.println(abstracts.size());
//        List<String> all_frames = LineIO.read("accept_frames_ALL.txt");
//        AnnotateAbstracts.acceptableFrames = all_frames.subList(0, Params.frameK);
		for(NYAbstracts abs:abstracts)
		{
			docids.add(abs.docid);
		}
		Collections.sort(docids);
		return docids;
	}
	public static List<NYAbstracts> readAbstracts(String absFile, int limit)
			throws FileNotFoundException {
		List<String> abstracts = LineIO.read(absFile); // abstract.txt
		List<NYAbstracts> ans = new ArrayList<>();
		int c = 0;
		for (int i = 0; i < abstracts.size(); i += 2) {
//			 System.out.println(abstracts.get(i));
			String[] parts = abstracts.get(i).split("\\t");
			// System.out.println(parts[0]+" "+parts[1]);
			// System.out.println(abstracts.get(i + 1));
			if (IsBadAbstract(abstracts.get(i + 1))) {
				continue;
			}
			if (stringContainsItemFromList(parts[0], srlBreaks)) // breaks SRL
				continue;
			ans.add(new NYAbstracts(abstracts.get(i + 1), parts[0], parts[1]));
			c++;
			if (c == limit) {
				break;
			}
		}
		return ans;
	}

	static String[] function_pos = { "TO", "IN", "EX", "POS", "WDT", "PDT",
			"WRB", "MD", "CC", "DT", "RP", "UH" };

	static float simpleTextOverlap(TextAnnotation abs,
								   TextAnnotation paragraph, int sentId) {
		TokenLabelView absLemmas = (TokenLabelView) abs
				.getView(ViewNames.LEMMA);
		TokenLabelView paraLemmas = (TokenLabelView) paragraph
				.getView(ViewNames.LEMMA);
		TokenLabelView absPos = (TokenLabelView) abs.getView(ViewNames.POS);
		TokenLabelView paraPos = (TokenLabelView) paragraph
				.getView(ViewNames.POS);
		Sentence sent = paragraph.getSentence(sentId);
		List<Constituent> sentLemmas = paraLemmas.getConstituentsCovering(sent
				.getSentenceConstituent());
		List<Constituent> sentPos = paraPos.getConstituentsCovering(sent
				.getSentenceConstituent());

		List<String> lAbsLemma = new ArrayList<>();
		List<String> lSentLemma = new ArrayList<>();
		List<String> lAbsPos = new ArrayList<>();
		List<String> lSentPos = new ArrayList<>();
		for (Constituent cons : absLemmas.getConstituents())
			lAbsLemma.add(cons.getLabel());
		for (Constituent cons : absPos.getConstituents())
			lAbsPos.add(cons.getLabel());
		for (Constituent cons : sentLemmas)
			lSentLemma.add(cons.getLabel());
		for (Constituent cons : sentPos)
			lSentPos.add(cons.getLabel());

		List<String> lAbsContent = new ArrayList<>();
		List<String> lParaContent = new ArrayList<>();

		for (int i = 0; i < lAbsLemma.size(); i++) {
			if (!stringContainsItemFromList(lAbsPos.get(i), function_pos)) {
				lAbsContent.add(lAbsLemma.get(i));
			}
		}
		for (int i = 0; i < lSentLemma.size(); i++) {
			if (!stringContainsItemFromList(lSentPos.get(i), function_pos)) {
				lParaContent.add(lSentLemma.get(i));
			}
		}
		int o = 0;

		for (String tok : lAbsContent) {
			if (lParaContent.contains(tok))
				o++;
		}

		return o * 1.0f / lAbsLemma.size();

	}

	static float simpleTextOverlap(TextAnnotation abs, TextAnnotation paragraph) {
		TokenLabelView absLemmas = (TokenLabelView) abs
				.getView(ViewNames.LEMMA);
		TokenLabelView paraLemmas = (TokenLabelView) paragraph
				.getView(ViewNames.LEMMA);
		TokenLabelView absPos = (TokenLabelView) abs.getView(ViewNames.POS);
		TokenLabelView paraPos = (TokenLabelView) paragraph
				.getView(ViewNames.POS);

		List<String> lAbsLemma = new ArrayList<>();
		List<String> lParaLemma = new ArrayList<>();
		List<String> lAbsPos = new ArrayList<>();
		List<String> lParaPos = new ArrayList<>();

		for (Constituent cons : absLemmas.getConstituents())
			lAbsLemma.add(cons.getLabel());
		for (Constituent cons : absPos.getConstituents())
			lAbsPos.add(cons.getLabel());
		for (Constituent cons : paraLemmas.getConstituents())
			lParaLemma.add(cons.getLabel());
		for (Constituent cons : paraPos.getConstituents())
			lParaPos.add(cons.getLabel());

		List<String> lAbsContent = new ArrayList<>();
		List<String> lParaContent = new ArrayList<>();

		for (int i = 0; i < lAbsLemma.size(); i++) {
			if (!stringContainsItemFromList(lAbsPos.get(i), function_pos)) {
				lAbsContent.add(lAbsLemma.get(i));
			}
		}
		for (int i = 0; i < lParaLemma.size(); i++) {
			if (!stringContainsItemFromList(lParaPos.get(i), function_pos)) {
				lParaContent.add(lParaLemma.get(i));
			}
		}
		int o = 0;

		for (String tok : lAbsContent) {
			if (lParaContent.contains(tok))
				o++;
		}

		return o * 1.0f / lAbsLemma.size();
	}

	public static boolean stringContainsItemFromList(String inputString,
													 String[] items) {
		for (int i = 0; i < items.length; i++) {
			if (inputString.contains(items[i])) {
				return true;
			}
		}
		return false;
	}

	static String[] bad = { "Transcript of ", "Times column", "analysis;",
			"Article in", "Analysis:", "gives recipe", "Article desribes",
			"column notes", "article considers", "Excerpts from",
			"Interview with", "interview with", "interview of", "reviews",
			"column on", "Analysis of", "analysis of", "column says",
			"Op-Ed column", "Op-Ed article", "column discusses",
			"Answer to reader's query", "Profile of", "profile of",
			"Advice on", "Article on", "Article by", "article by", "Study by",
			"comments on", "Update on", "Editorial on", "Editorial",
			"Article describes" };

	public static boolean IsBadAbstract(String s) {
		return stringContainsItemFromList(s, bad);
	}

	// static TextAnnotation annotate(NYAbstracts abs, MyCuratorClient cc,
	// boolean headline) {
	// try {
	// if (headline) {
	// TextAnnotation ta = cc.client.createBasicTextAnnotation("nyt", abs.docid
	// + "_head", abs.headline);
	// // System.out.println(cleanText);
	// for (String viewName : cc.rm
	// .getCommaSeparatedValues("viewsToAdd")) {
	// cc.client.addView(ta, viewName);
	// }
	// return ta;
	// } else {
	// String text = abs.abs.split(";")[0] + ".";
	// System.out.println("ABSTRACT TO ANNOTATE:" + text);
	// TextAnnotation ta = cc.client.createBasicTextAnnotation("nyt", abs.docid
	// + "_abs", text);
	// // System.out.println(cleanText);
	// for (String viewName : cc.rm
	// .getCommaSeparatedValues("viewsToAdd")) {
	// cc.client.addView(ta, viewName);
	// }
	// return ta;
	// }
	// } catch (AnnotatorException e) {
	// e.printStackTrace();
	// System.err.println("WTF!");
	// }
	// return null;
	// }

	public static TextAnnotation annotate(String text, MyCuratorClient cc,
								   boolean isAbstract) throws AnnotatorException {

		if (isAbstract) {
			text = text.split(";")[0] + ".";
//			System.out.println("ABSTRACT TO ANNOTATE:" + text);
			try {
				TextAnnotation ta = cc.client.createAnnotatedTextAnnotation("nyt",
						"" + "_abs", text);
				// System.out.println(cleanText);
				for (String viewName : cc.rm.getCommaSeparatedValues("viewsToAdd")) {
					cc.client.addView(ta, viewName);
				}
				return ta;
			}
			catch (Exception e)
			{
				System.err.println("Unable to process abstract!");
//				e.printStackTrace();
			}
		}
		else {
			try {
				TextAnnotation ta = cc.client.createAnnotatedTextAnnotation("nyt", "",
						text);
				// System.out.println(cleanText);
				for (String viewName : cc.rm.getCommaSeparatedValues("viewsToAdd")) {
					cc.client.addView(ta, viewName);
				}
				return ta;
			}
			catch (Exception e)
			{
				System.err.println("Unable to process!");
//				e.printStackTrace();
			}
		}
		return null;
	}

	private static void UpdateCountPOS(TextAnnotation ta,
									   Counter<Pair<String, String>> c) {
		TokenLabelView pos = (TokenLabelView) ta.getView(ViewNames.POS);
		for (Constituent cons : pos.getConstituents()) {
			c.incrementCount(new Pair<String, String>(cons.getSurfaceForm(),
					cons.getLabel()));
		}
	}

	static Counter<Pair<String, String>> posCountsBody = new Counter<>();
	static Counter<Pair<String, String>> posCountsAbs = new Counter<>();
	static Counter<Integer> blockLocCounts = new Counter<>();


	public static void main(String args[]) throws Exception {
		InteractiveShell<AnnotateAbstracts> tester = new InteractiveShell<AnnotateAbstracts>(
				AnnotateAbstracts.class);
		if (args.length == 0)
			tester.showDocumentation();
		else {
			tester.runCommand(args);
		}
	}

//	static List<String> acceptableFrames;
	@CommandDescription(description="NUMDOCS, caches annotations")
	public static void extract(String NUMDOCS) throws Exception {
		boolean alreadyCached = false;
		int numdocs = Integer.parseInt(NUMDOCS);
		MyCuratorClient cc = new MyCuratorClient(alreadyCached);
		int c = 0;
		List<NYAbstracts> abstracts = readAbstracts("world.txt", numdocs);

//		List<String> all_frames = LineIO.read("accept_frames_ALL.txt");
//		acceptableFrames = all_frames.subList(0, Params.frameK);

		NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();

//		List<AnnotatedDocument> docs = new ArrayList<>();
		for (NYAbstracts abs : abstracts) {
			String textFile = Params.dataPath + "/" + abs.docid + ".xml";
			System.out.println(textFile);
			NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(
					new File(textFile), false);
			try {
				String reduced_body = "";
				for (int i = 0; i < Math.min(10, nydoc.paragraphs.size()); i++) {
					String para = nydoc.paragraphs.get(i);
					reduced_body+=para+"\n";
					System.out.println("here1");
					TextAnnotation paraTa = annotate(para, cc, false);
				}
				System.out.println("here2");
				TextAnnotation bodyTa = annotate(reduced_body, cc, false);
				System.out.println("here3");
				TextAnnotation absTa = annotate(nydoc.articleAbstract, cc, true);
//				if(alreadyCached)
//				{
//					List<EventInstance> abs_events = extractor.extract(absTa,
//							acceptableFrames);
////					System.out.println(abs_events.size());
//					List<EventInstance> body_events = extractor.extract(bodyTa,
//							acceptableFrames);
////					System.out.println(body_events.size());
//					AnnotatedDocument doc = new AnnotatedDocument(body_events, abs_events);
//					doc.absTa = absTa;
//					doc.bodyTa = bodyTa;
//					doc.nydoc = nydoc;
//					doc.bestpara=extractGoldParagraphs(nydoc,cc);
////					docs.add(doc);
//
//					// ForBrat.writeEvents(abs.docid, abs_events, absTa, "abs");
//					// ForBrat.writeEvents(abs.docid, body_events, bodyTa, "body");
//
//				}



//				extractSupervision(bodyTa);

				// UpdateCountPOS(bodyTa, posCountsBody);
				// System.out.println(event_triggers.size()+" acceptable frame triggers found!");
				// Evaluator.EvaluateOnTriggerSurface(absTa,event_triggers,tester,acceptableFrames);

				// Evaluator.EvaluateOnAbstractFrames(abs_events,body_events,tester,acceptableFrames);

				// int score = getBaselineScore(nydoc, cc);
				// if(score==0)
				// {
				// hard++;
				// }

//				System.out.println("==================================");
				c++;
				System.out.println("c=" + c);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
//		System.out.println(tester.getPerformanceTable().toOrgTable());
		cc.cleanUp();
//		return docs;
	}


	public static int extractGoldParagraphs(NYTCorpusDocument nydoc,
											MyCuratorClient cc) throws AnnotatorException {
		TextAnnotation absTA = annotate(nydoc.articleAbstract, cc, true);
		UpdateCountPOS(absTA, posCountsAbs);
		float overlap;
		int maxidx = -1;
		float max = -99999.0f;
		for (int i = 0; i < Math.min(5, nydoc.paragraphs.size()); i++) {
			String para = nydoc.paragraphs.get(i);
			TextAnnotation TA = annotate(para, cc, false);
			overlap = simpleTextOverlap(absTA, TA);
//			System.out.println(overlap + " with " + i);
			if (overlap > max) {
				maxidx = i;
				max = overlap;
			}
		}
//		System.out.println("Best paragraph: " + maxidx + " " + max + "-->"
//				+ nydoc.paragraphs.get(maxidx));
		return maxidx;
	}

	public static void extractSupervision(TextAnnotation ta) throws EdisonException {
		PredicateArgumentView verb = (PredicateArgumentView) ta
				.getView(ViewNames.SRL_VERB);
		PredicateArgumentView nom = (PredicateArgumentView) ta
				.getView(ViewNames.SRL_NOM);
		// TreeView tree = (TreeView) ta.getView(ViewNames.DEPENDENCY_STANFORD);
		// System.out.println(tree.toString());
		PredicateArgumentView comma = (PredicateArgumentView) ta
				.getView("SRL_COMMA");
		List<Constituent> subOrdClauses = TAUtils.getSubOrdClauses(ta);
		List<Constituent> relativeClauses = TAUtils.getRelativeClauses(ta,
				subOrdClauses);
		for (int i = 0; i < ta.getNumberOfSentences(); i++) {
			Sentence sent = ta.getSentence(i);
			System.out.println(sent);
//			System.out.println("---VERB---");
//			if (verb == null || nom == null) {
//				System.out.println("BAD!!!!");
//				continue;
//			}
			for (Constituent cons : verb.getConstituentsCoveringSpan(
					sent.getStartSpan(), sent.getEndSpan())) {
				if (cons.getLabel().equals("Predicate")) {

					TAUtils.getTense(cons);
//					System.out.println("VOICE: "
//							+ TAUtils.voiceFeat(cons) + " " + cons);
//					List<Relation> rels = cons.getOutgoingRelations();
//
//					for (Relation relIns : rels) {
//						Constituent arg = relIns.getTarget();
//						System.out.println("==>" + relIns.getRelationName()
//								+ " " + arg.getSurfaceForm());
//					}

//					TokenLabelView tok_lemmas = (TokenLabelView) headline
//							.getView(ViewNames.LEMMA);
//					List<String> ll = new ArrayList<>();
//					for (Constituent cc : tok_lemmas.getConstituents()) {
//						ll.add(cc.getLabel());
//					}
//					if (ll.contains(TAUtils.getLemma(cons))) {
//						// System.out.println("LEMMA MATCH IN HEADLINE!!" +
//						// cons);
//					}

//					boolean insideSubord = false;
//					boolean insideRelative = false;
//					for (Constituent clause : subOrdClauses) {
//						if (clause.doesConstituentCover(cons)) {
//							insideSubord = true;
//							break;
//						}
//					}
//					for (Constituent clause : relativeClauses) {
//						if (clause.doesConstituentCover(cons)) {
//							insideRelative = true;
//							break;
//						}
//					}
//
//					if (insideSubord) {
//						if (insideRelative) {
//							System.out.println("INSIDE RELATIVE");
//						} else
//							System.out.println("INSIDE SUBORD");
//					}
				}
			}
			System.out.println("---NOM---");
			for (Constituent cons : nom.getConstituentsCoveringSpan(
					sent.getStartSpan(), sent.getEndSpan())) {
				if (cons.getLabel().equals("Predicate")) {
					// System.out.println(cons);
//					TokenLabelView tok_lemmas = (TokenLabelView) headline
//							.getView(ViewNames.LEMMA);
//					List<String> ll = new ArrayList<>();
//					for (Constituent cc : tok_lemmas.getConstituents()) {
//						ll.add(cc.getLabel());
//					}
//					if (ll.contains(TAUtils.getLemma(cons))) {
//						// System.out.println("LEMMA MATCH IN HEADLINE!!" +
//						// cons);
//					}
				}
			}

			System.out.println("---COMMA---");
			for (Constituent cons : comma.getConstituentsCoveringSpan(
					sent.getStartSpan(), sent.getEndSpan())) {
				System.out.println(cons + " " + cons.getLabel());
				{
					List<Relation> relations = cons.getOutgoingRelations();
					for(Relation rel:relations)
					{
						Constituent trgt = rel.getTarget();
						System.out.println("TARGET: "+trgt);
					}
				}
			}
			System.out.println("====================");
		}
	}



}
