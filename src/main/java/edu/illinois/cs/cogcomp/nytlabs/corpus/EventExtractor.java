package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.nlp.utilities.CollinsHeadFinder;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.srl.data.FrameData;
import edu.illinois.cs.cogcomp.srl.data.FramesManager;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.io.FileNotFoundException;
import java.util.*;

//import edu.illinois.cs.cogcomp.classification.densification.representation.SparseVectorT;
//import edu.illinois.cs.cogcomp.classification.hierarchy.datastructure.ConceptData;
//import edu.illinois.cs.cogcomp.classification.representation.esa.simple.SimpleESALocal;
//import edu.illinois.cs.cogcomp.classification.representation.word2vec.DiskBasedWordEmbedding;

public class EventExtractor {

	// List of ignored predicates copied from Quang's Code
	public static Set<String> ignoredPredicates = new HashSet<String>();

	static List<String> lemmas = new ArrayList<>();
	public static void loadBiases() throws FileNotFoundException {
		ArrayList<String> biases = LineIO.read("nyt_bias_weights.csv");
		for(int i=1;i<biases.size();i++)
		{
			String[] parts = biases.get(i).split(",");
			lemmas.add(parts[0]);
		}
	}

	public static List<EventInstance> fromSRL(TextAnnotation ta, String type) {
		List<EventInstance> ans = new ArrayList<>();
		PredicateArgumentView srl = (PredicateArgumentView) ta
				.getView(type);
		for (int i = 0; i < ta.getNumberOfSentences(); i++) {
			Sentence ss = ta.getSentence(i);
			System.out.println(ss);
			for (Constituent conIns : srl.getConstituentsCoveringSpan(ss.getStartSpan(), ss.getEndSpan())) {
				if (conIns.getLabel().equals("Predicate")) {
					String sense = conIns.getAttribute("SenseNumber");
//					w.println("PREDICATE: "+conIns+" "+sense);
					System.out.println("PREDICATE: " + conIns + " " + sense);
					if(lemmas.contains(TAUtils.getLemma(conIns)))
					{
						System.out.println("YAY! "+conIns);
					}
					FrameData data;
					if(type==ViewNames.SRL_NOM)
					{
						data=nomManager.getFrame(TAUtils.getLemma(conIns));
					}
					else
					{
						data=verbManager.getFrame(TAUtils.getLemma(conIns));
					}
					Constituent arg;
					List<Relation> rels = conIns.getOutgoingRelations();
					for (Relation relIns : rels) {
						arg = relIns.getTarget();
						String relName=relIns.getRelationName();
						if(relName.contains("-"))
						{
							relName=relName.split("-")[1];
						}
						if(data!=null && data.getArgsForSense(sense).contains(relName)) {
							String desc = data.getArgumentDescription(sense, relName);
							Constituent head = getHeadWord(arg);
//							w.println("==>" + relName + " " + arg.getSurfaceForm() + " arg:" + desc + " head:" + head);
							System.out.println("==>" + relName + " " + arg.getSurfaceForm() + " arg:" + desc + " head:" + head);

						}
						else
						{
//							w.println("==>"+relName + " " + arg.getSurfaceForm());
							System.out.println("==>"+relName + " " + arg.getSurfaceForm());
						}
					}
					ans.add(new EventInstance(conIns,TAUtils.getLemma(conIns), rels, i));
				}
			}
		}
		return ans;
	}

	public static List<EventInstance> fromDepparse(TextAnnotation ta) {
		List<EventInstance> ans = new ArrayList<>();
		TreeView dep = (TreeView) ta.getView(ViewNames.DEPENDENCY_STANFORD);

		for (int i = 0; i < ta.getNumberOfSentences(); i++) {
			Sentence ss = ta.getSentence(i);
//			System.out.println(ss);
			for (Constituent cons : dep.getConstituentsCoveringSpan(ss.getStartSpan(), ss.getEndSpan())) {
				List<Relation> rels = cons.getOutgoingRelations();
				Map<Constituent,Constituent> nsubj= new HashMap<>();
				Map<Constituent,Constituent> dobj= new HashMap<>();
				for (Relation rel : rels) {

					switch(rel.getRelationName())
					{
						case "nsubj":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
							nsubj.put(rel.getSource(), rel.getTarget());
							break;
						case "dobj":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
							dobj.put(rel.getSource(), rel.getTarget());
							break;
						case "nsubjpass":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
							nsubj.put(rel.getSource(), rel.getTarget());
							break;
//						case "xsubj":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
//							break;
						case "iobj":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
							dobj.put(rel.getSource(), rel.getTarget());
							break;
//						case "pobj":
//							System.out.println(getLemma(rel.getSource()) + " " + rel.getRelationName() + " " + getLemma(rel.getTarget()));
//							break;
					}
				}
				for(Constituent key:nsubj.keySet())
				{
					if(dobj.containsKey(key))
					{
//						System.out.println("EVENT PRED:"+key+" SUBJ:"+nsubj.get(key)+" OBJ:"+dobj.get(key));
//						System.out.println("EVENT PRED:"+key+" SUBJ:"+SRLTesting.getCorefWord(nsubj.get(key))+" OBJ:"+dobj.get(key));
						EventInstance tmp = new EventInstance(nsubj.get(key), TAUtils.getLemma(nsubj.get(key)), key, TAUtils.getLemma(key), dobj.get(key), TAUtils.getLemma(dobj.get(key)), i);
						System.out.println(tmp);
						ans.add(tmp);
					}
				}
				// create event instance
			}

		}
		System.out.println("EVENTS "+ans.size());
		return ans;
	}




	static int nested=0;
	private static void FindNestedPredicateArgs(PredicateArgumentView srk)
	{
		List<Constituent> predicates = srk.getPredicates();
		for(Constituent predicate:predicates)
		{
			List<Relation> args = srk.getArguments(predicate);
			for(Relation rel:args)
			{
				Constituent arg = rel.getTarget();
				int nested_predicates=0;
				List<Constituent> subtree = srk.getConstituentsCovering(arg);
				for(Constituent cons:subtree)
				{
					if(cons.getLabel().equals("Predicate"))
					{
						nested_predicates++;
						System.out.println("NESTED!"+nested);
						System.out.println("ENCLOSING ARG---------> "+arg);
						System.out.println("NESTED PREDICATE-------> "+cons);
						nested++;
					}
				}
				System.out.println("Total nested under this arg "+nested_predicates);
			}
		}

	}

	private static List<Constituent> getNEWord(TextAnnotation ta, Constituent arg1) {
		SpanLabelView ner = (SpanLabelView) ta.getView(ViewNames.NER_CONLL);
		List<Constituent> cons = ner.getConstituentsCovering(arg1);
		if(cons.size()>0)
		{
			return cons;

		}
		else return null;
	}

	public static Constituent getHeadWord(Constituent c) {
		TextAnnotation ta = c.getTextAnnotation();
		TreeView parse = (TreeView) ta.getView(ViewNames.PARSE_STANFORD);
		CollinsHeadFinder headFinder = CollinsHeadFinder.getInstance();
		try {
			Constituent parsePhrase = parse.getParsePhrase(c);
			Constituent head = headFinder.getHeadWord(parsePhrase);
			// System.out.println("MENTION " + m);
			// System.out.println("PARSE PHRASE " + parsePhrase);
			// System.out.println("HEAD " + head);
			// System.out.println("-----------------------------------");
			return head;

		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;

	}

//	static SimpleESALocal esa;
//	private static void initialize() throws Exception {
//		esa = new SimpleESALocal();
//
//		System.out.println("Initialization Done!");
//	}
//
//	private static SparseVectorT<String> convertConceptDataToVector(List<ConceptData> vec) {
//		List<String> conceptsList = new ArrayList<String>();
//		List<Double> scores = new ArrayList<Double>();
//		if (vec != null) {
//			for (int j = 0; j < vec.size(); j++) {
//				conceptsList.add(vec.get(j).concept + "");
//				scores.add(vec.get(j).score);
//			}
//		}
//		SparseVectorT<String> sparseVector = new SparseVectorT<String>(conceptsList, scores);
//		return sparseVector;
//	}
//
//	private static SparseVectorT<String> generateVector(TimeMLEventInstance event) throws Exception {
//		SparseVectorT<String> vector = null;
//		String text = event.predlemma;
//			List<ConceptData> vec = esa.retrieveConcepts(text, 500);
//			vector = convertConceptDataToVector(vec);
//
//		// TODO:
//		// Argument Lists
//		// Coref features
//		// SparseVectorT<String> Merge Operation
//		return vector;
//	}
//
//
static FramesManager verbManager = FramesManager.getPropbankInstance();
	static FramesManager nomManager = FramesManager.getNombankInstance();

	public static void main(String[] args) throws Exception {
		FrameData data = verbManager.getFrame("kill");
		for(String sense:data.getSenses()) {
			System.out.println(sense);
			for (String aa:data.getArgsForSense(sense))
			{
				System.out.println(data.getArgumentDescription(sense,aa));
			}
		}
	}
}
