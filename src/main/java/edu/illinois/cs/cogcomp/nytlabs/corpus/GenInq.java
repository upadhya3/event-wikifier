package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.core.io.LineIO;
// import edu.illinois.cs.cogcomp.nlp.utility.TextAnnotationBuilder;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenInq {
	static Map<String,List<String>> dict = new HashMap<>();
	public static void main(String[] args) throws FileNotFoundException {
		// TextAnnotationBuilder ta;

		List<String> lines = LineIO.read("/Users/Shyam/Desktop/speciteller/resources/inquirerTags.txt");
		for(String line:lines)
		{
			String[] parts = line.trim().split("\\s+");
			String word = parts[0];
				dict.put(word,new ArrayList<String>());
			for (int i = 1; i < parts.length; i++) {
				if(parts[i].equals("Pos") || parts[i].equals("Pstv"))
					dict.get(word).add("+ve");
				else if(parts[i].equals("Neg") || parts[i].equals("Ngtv"))
					dict.get(word).add("-ve");
				else
				{
					dict.get(word).add(parts[i]);
				}
			}

		}
	}
}
