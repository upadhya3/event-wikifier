package edu.illinois.cs.cogcomp.nytlabs.corpus.annotation;

import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.ELMention;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocumentParser;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class NYTAnnotations {

    public List<String> docids;
    public Map<String, List<ELMention>> docid2ELMention;

    public NYTAnnotations() {
        docids = new ArrayList<String>();
        docid2ELMention = new HashMap<String, List<ELMention>>();
    }

    // call with world.txt
    public static void ExtractAbstracts(String outfile) throws IOException {
        NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();
        System.out.println("Writing abstracts  to .... "+outfile);
        PrintWriter w = new PrintWriter(outfile);
        File file = new File(Params.dataPath);
        int cnt = 0;
        int seen = 0;
        for (File docid : file.listFiles()) {
            seen++;
            String textFile = docid.getAbsolutePath();
            NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(new File(textFile), false);
            // modify isGood to cover other pages
            if (isGood(nydoc)) {
//                System.out.println(nydoc.getGuid() + "\t" + nydoc.headline + "\t" + nydoc.getOnlineSection() + "\t" + nydoc.getDescriptors());
//                System.out.println(nydoc.getArticleAbstract());
                w.println(nydoc.getGuid() + "\t" + nydoc.headline + "\t" + nydoc.getOnlineSection() + "\t" + nydoc.getDescriptors());
                w.println(nydoc.getArticleAbstract());
                cnt++;
                if (cnt % 1000 == 0) {
                    System.out.println(cnt + "/" + seen);
                }
//                if (cnt == 5000) {
//                    System.out.println("Done!");
//                    break;
//                }
            }
        }
        System.out.println("Done!!");
        w.close();
    }


    public static void main(String[] args) {
        NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();
        File file = new File(Params.dataPath);
        int cnt = 0;
        int seen = 0;
        for (File docid : file.listFiles()) {
            seen++;
            String textFile = docid.getAbsolutePath();
            NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(new File(textFile), false);
            if (isGood(nydoc)) {
                System.out.println(nydoc.getGuid() + "\t" + nydoc.headline + "\t" + nydoc.getOnlineSection() + "\t" + nydoc.getDescriptors());
                System.out.println(nydoc.getArticleAbstract());
                cnt++;
                if (cnt % 1000 == 0) {
                    System.out.println(cnt + "/" + seen);
                }
                if (cnt == 5000) {
                    System.out.println("Done!");
                    break;
                }
            }
        }
    }


    private static boolean isGood(NYTCorpusDocument nydoc) {
        boolean section = false;
        boolean desc = false;
        boolean abs = false;
        boolean headline = false;
//		System.out.println(nydoc.getOnlineSection());

        // Uncomment to get pages which have both!
//		if(nydoc.getOnlineSection()!=null && nydoc.getOnlineSection().contains("World") && nydoc.getOnlineSection().contains("Front Page"))
//		{
//			section=true;
//		}

        if (nydoc.getOnlineSection() != null && nydoc.getOnlineSection().contains("World")) {
            section = true;
        }
        if (nydoc.articleAbstract != null && nydoc.articleAbstract.length()>=20 && !AnnotateAbstracts.IsBadAbstract(nydoc.articleAbstract)) {
            abs = true;
        }
        if (!nydoc.getDescriptors().contains("Public Opinion") && !nydoc.getDescriptors().contains("Biographical Information") && !nydoc.getDescriptors().contains("Social Conditions and Trends")) {
            desc = true;
        }
        if (nydoc.headline != null && !nydoc.headline.contains("World Briefing |")) {
            headline = true;
        }
        if (section && desc && abs && headline)
            return true;
        return false;
    }

}
