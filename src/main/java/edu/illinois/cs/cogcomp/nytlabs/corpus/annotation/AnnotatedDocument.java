package edu.illinois.cs.cogcomp.nytlabs.corpus.annotation;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;

import java.io.*;
import java.util.List;

public class AnnotatedDocument implements Serializable{

	static final long serialVersionUID = -7588980448693010399L;
	public List<List<EventInstance>> para_events;
	public List<TextAnnotation> paraTa;
	public NYTCorpusDocument nydoc;
	// dont save TA! Its already there in below
	public List<EventInstance> abs_events;
	public List<EventInstance> body_events;
	public TextAnnotation bodyTA,absTA;
	public int bestpara = -1;
	public int isbad  = 0;
	public AnnotatedDocument()
	{
		isbad=1;
	}

	public AnnotatedDocument(List<List<EventInstance>> para_events,List<EventInstance> body_events, List<EventInstance> abs_events, List<TextAnnotation> paraTas, TextAnnotation bodyTa, TextAnnotation absTa) {
		this.para_events= para_events;
		this.abs_events= abs_events;
		this.body_events= body_events;
		this.bodyTA = bodyTa;
		this.absTA = absTa;
		this.paraTa= paraTas;
	}

	public TextAnnotation getBodyTA()
	{
		return bodyTA;
	}
	public TextAnnotation getAbsTA() {	return absTA; }
	public void save(String path) throws IOException {
		FileOutputStream fileOut =
				new FileOutputStream(path);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(this);
		out.close();
		fileOut.close();
	}

	public void setParents() {
		for(EventInstance ee:abs_events)
		{
			ee.setParent(this);
		}
		for(EventInstance ee:body_events)
		{
			ee.setParent(this);
		}
	}

//	public void ClusterEvents()
//	{
//		FastVectorLoader fv = new FastVectorLoader();
//		Map<TimeMLEventInstance,List<TimeMLEventInstance>> pairClusters = new HashMap<>();
//		for(int i=0;i<events.size();i++)
//		{
//			for(int j=i+1; j<events.size();j++)
//			{
//				TimeMLEventInstance ei = events.get(i);
//				TimeMLEventInstance ej = events.get(j);
//				String surfi= ei.predlemma;
//				String surfj= ej.predlemma;
//				float[] vi = fv.get(surfi);
//				float[] vj = fv.get(surfj);
//				if (vi==null || vj==null)
//				{
//					System.out.println("OOV! "+surfi+" "+surfj);
//					continue;
//				}
//				float cosine = fv.cosine(vi, vj);
////				System.out.println(surfi+" "+surfj+" "+cosine);
//				if(cosine> Params.cosineThresHold)
//				{
//					System.out.println("YAY! " + surfi+" "+surfj+" "+cosine);
//					System.out.println(ei+" "+ej);
//					int iidx=events.indexOf(ei);
//					int jidx=events.indexOf(ej);
//					TimeMLEventInstance smaller = iidx < jidx ? ei : ej;
//					TimeMLEventInstance larger = iidx >= jidx ? ei : ej;
//					if(pairClusters.containsKey(smaller))
//					{
//						pairClusters.get(smaller).add(larger);
//					}
//					else
//					{
//						pairClusters.put(smaller,new ArrayList<TimeMLEventInstance>());
//						pairClusters.get(smaller).add(larger);
//					}
//				}
//			}
//		}
//
//	}

//	public static AnnotatedDocument load(String savePath) throws IOException, ClassNotFoundException {
//	FileInputStream fileIn = new FileInputStream(savePath);
//	ObjectInputStream in = new ObjectInputStream(fileIn);
//	AnnotatedDocument e = (AnnotatedDocument) in.readObject();
//	in.close();
//	fileIn.close();
//		return e;
//}
//	public static void main(String[] args) throws IOException, ClassNotFoundException {
//		AnnotatedDocument aa = load(Params.dumpPath + "/" + "1475571.ser");
//		System.out.println(aa.nydoc.headline);
//		System.out.println(aa.nydoc.articleAbstract);
//		System.out.println(aa.ta.getNumberOfSentences());
//	}


}
