package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.readers.LazyNYTReader;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;

/**
 * Created by Shyam on 11/9/15.
 */
public class ForBrat {

//    public static void DumpToAnnotate() throws FileNotFoundException {
//        List<NYAbstracts> abstracts = AnnotateAbstracts.readAbstracts("world.txt", 260);
//        NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();
//        for (NYAbstracts abs : abstracts) {
//            System.out.println(abs.docid + " " + abs.headline);
//            String textFile = Params.dataPath + "/" + abs.docid + ".xml";
//            NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(new File(
//                    textFile), false);
//            String path = Params.brat_data + "/"+abs.docid;
//            PrintWriter w = new PrintWriter(path+".txt");
//            w.println("HEADLINE: "+nydoc.headline);
//            w.println();
//            w.println();
//            for(String p: nydoc.paragraphs)
//            {
//                w.println(p);
//                w.println();
//            }
//            w.close();
////            w = new PrintWriter(dataPath+".ann");
////            w.close();
//        }
//    }

    public static void main(String[] args) throws Exception {
        LazyNYTReader reader = new LazyNYTReader();
        AnnotatedDocument doc = reader.next();
        String DIR  = "/shared/bronte/upadhya3/NYTsummaries/final";
        while(doc!=null)
        {
            List<EventInstance> goldann = Evaluator.readGoldData(doc, Params.goldPath + "/" + doc.nydoc.getGuid() + "_abs.ann", "abs", false);
            Evaluator.projectLabelToDoc(doc.body_events,goldann);
            writeDocBody(DIR, doc.nydoc.guid + "", "final", doc.bodyTA);
            writeDocEvents(DIR, doc.nydoc.guid + "", "final", doc.body_events);
            doc=reader.next();
        }

//        MyAceReader reader = new MyAceReader();
//        Annotation maps = reader.read("/shared/corpora/corporaWeb/written/multi/ACE-2005/Extracted/data/English/");
//        List<CoreMap> sents= maps.get(CoreAnnotations.SentencesAnnotation.class);
//        int c=0;
//        for(CoreMap sent:sents)
//        {
////            List<>events=aaa.get(MachineReadingAnnotations.EventMentionsAnnotation.class);
////            System.out.println(aaa.get(MachineReadingAnnotations.EventMentionsAnnotation.class));
//            List<CoreLabel> words = sent.get(CoreAnnotations.TokensAnnotation.class);
//            List<EventMention> l = sent.get(MachineReadingAnnotations.EventMentionsAnnotation.class);
//            if(l==null)
//                continue;
//            for(EventMention ee:l)
//            {
//                System.out.println(ee);
//                System.out.println(ee.getExtentString());
//                System.out.println(ee.getAnchor().getExtentTokenStart()+" "+ee.getAnchor().getExtentTokenEnd());
//                c++;
////                System.out.println(ee);
//            }
//        }
//        System.out.println(c);
//        Annotation data = reader.parse("/shared/corpora/corporaWeb/written/multi/ACE-2005/Extracted/data/English/bc/fp1/CNN_CF_20030303.1900.06-2.apf.xml");
    }

    /**
     * suffix is "abs" or "body", datadir is Params.brat_dir or something similar
     * @param docid
     * @param events
     * @param ta
     * @param suffix
     * @throws FileNotFoundException
     */
    public static void writeEvents(String datadir, String docid, List<EventInstance> events, TextAnnotation ta, String suffix) throws FileNotFoundException {
        String path = datadir + "/"+docid+"_"+suffix;
        PrintWriter w = new PrintWriter(path+".txt");
        String text = ta.getText();
        w.println(text);
        w.close();

        PrintWriter ann = new PrintWriter(path+".ann");
        for(int i=0;i<events.size();i++)
        {
            EventInstance ee = events.get(i);
            String buf="";
            buf+=ee.frame+"--";
            int start = ee.pred_srl.getStartCharOffset();
            int end = ee.pred_srl.getEndCharOffset();
            int id=i+1;
            int label=ee.salient;
            ann.println("T"+id+"\t"+label+" "+start+" "+end+"\t"+ee.pred_srl.getSurfaceForm());
            ann.println("#"+id+"\t"+"AnnotatorNotes"+" "+"T"+id+"\t"+buf);

        }
        ann.close();
    }

    public static void writeDocEvents(String datadir, String docid, String suffix, List<EventInstance> events) throws FileNotFoundException {
        String path = datadir + "/"+docid+"_"+suffix;
        if(IOUtils.exists(path+".ann"))
            return;
        System.out.println("writing to "+path);
        PrintWriter w = new PrintWriter(path+".ann");
        for(int i=0;i<events.size();i++)
        {
            EventInstance ee = events.get(i);
            String buf="";
            buf+=ee.frame+"--";
            int start = ee.pred_srl.getStartCharOffset();
            int end = ee.pred_srl.getEndCharOffset();
            int id=i+1;
            int label=ee.salient;
            w.println("T"+id+"\t"+label+" "+start+" "+end+"\t"+ee.pred_srl.getSurfaceForm());
            w.println("#"+id+"\t"+"AnnotatorNotes"+" "+"T"+id+"\t"+buf);
        }
        w.close();
    }
    public static void writeDocBody(String datadir, String docid, String suffix, TextAnnotation ta) throws FileNotFoundException {
        String path = datadir + "/"+docid+"_"+suffix;
        if(IOUtils.exists(path+".txt"))
            return;
        System.out.println("writing to "+path);
        PrintWriter w = new PrintWriter(path+".txt");
        String text = ta.getText();
        w.println(text);
        w.close();
    }
}
