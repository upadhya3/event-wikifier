package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.heurisitic.Heuristic;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by Shyam on 2/25/16.
 */
public class IsFirstHeuristic extends Heuristic {

    @Override
    public void apply(EventInstance ee, List<EventInstance> others) throws EdisonException {
        boolean isFirst= TAUtils.isFirst(ee, others);
        if(isFirst)
        {
            ee.salient=1;
        }
    }
}
