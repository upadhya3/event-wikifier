package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

/**
 * Created by upadhya3 on 10/23/15.
 */
public class NYAbstracts {
    public String abs;
    public String docid;
    public String headline;

    public NYAbstracts(String abs, String docid, String headline)
    {
        this.abs = abs;
        this.docid = docid;
        this.headline = headline;
    }
}
