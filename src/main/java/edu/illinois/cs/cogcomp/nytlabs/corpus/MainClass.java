package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.CoreferenceView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.NYTAnnotations;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainClass {

//	public static List<ELMention> getExamples(int limit, String mode)
//			throws IllegalArgumentException, Exception {
//		NYTAnnotations annotations;
//		if (mode.equals("train"))
//			annotations = readAnnotations(Params.trainAnnotations);
//		else
//			annotations = readAnnotations(Params.testAnnotations);
//
//		// showDist();
//		// System.exit(-1);
//
//		MyCuratorClient cc = new MyCuratorClient(false);
//		int c = 0;
//		NYTCorpusDocumentParser pp = new NYTCorpusDocumentParser();
//		for (String docid : annotations.docids) {
//
//			String textFile = Params.dataPath + "/" + docid + ".xml";
//			NYTCorpusDocument nydoc = pp.parseNYTCorpusDocumentFromFile(new File(
//					textFile), false);
//
//			AnnotatedDocument anndoc = AnnotateDocument(cc, nydoc);
//			propogateTA(docid, anndoc, annotations);
//			c++;
//			if (c == limit)
//				break;
//		}
//
//		cc.cleanUp();
//
//		List<ELMention> examples = new ArrayList<>();
//		for (String id : annotations.docids) {
//			List<ELMention> mm = annotations.docid2ELMention.get(id);
//			for (ELMention men : mm) {
//				if (!men.notFound()) {
//					examples.add(men);
//				}
//			}
//		}
//		return examples;
//	}

//	public static void propogateTA(String docid, AnnotatedDocument anndoc,
//								   NYTAnnotations annotations) {
//		SpanLabelView ner = (SpanLabelView) anndoc.ta
//				.getView(ViewNames.NER_CONLL);
////		CoreferenceView coref = (CoreferenceView) anndoc.ta
////				.getView(StanfordCoref.MYVIEW);
//		CoreferenceView coref = (CoreferenceView) anndoc.ta
//				.getView(ViewNames.COREF);
//		matchCount(annotations, ner, coref, docid);
//		// corefDemo(pipeline, doc.body);
//		List<ELMention> mentions = annotations.docid2ELMention.get(docid);
//		for (ELMention men : mentions) {
//			men.setTA(anndoc.ta);
//			men.setNYDoc(anndoc.nydoc);
//		}
//	}

	static int docidCurator=0;
//	public static AnnotatedDocument AnnotateDocument(MyCuratorClient cc,
//													 NYTCorpusDocument nydoc)
//			throws IllegalArgumentException, Exception {
//
//
//		TextAnnotation ta;
//
////		System.out.println("COL "+nydoc.columnName);
////		System.out.println("COL "+nydoc.biographicalCategories);
////		System.out.println("COL "+nydoc.onlineSection);
//
//		String cleanText = StringCleanup.normalizeToAscii(nydoc.body);
//
//		// String cleanText = textCleaner.cleanText(doc.body);
//		// System.out.println(cleanText);
//		docidCurator++;
//		ta = cc.client.createBasicTextAnnotation("nyt", docidCurator+"", cleanText);
//		// System.out.println(cleanText);
//		for (String viewName : cc.rm
//				.getCommaSeparatedValues("viewsToAdd")) {
//			cc.client.addView(ta, viewName);
//		}
//		return new AnnotatedDocument(nydoc, ta);
//
//	}

	private static void showDist(NYTAnnotations annotations) {
		int[] counts = new int[50];
		for (String doc : annotations.docids) {
			List<ELMention> mens = annotations.docid2ELMention.get(doc);
			int c = 0;
			for (ELMention m : mens) {
				c += m.getSalience();
			}
			System.out.println(doc + " " + c);
			counts[c]++;
		}
		for (int i = 0; i < counts.length; i++) {
			System.out.println(i + " " + counts[i]);
		}
	}

	private static void matchCount(NYTAnnotations annotations,
								   SpanLabelView ner, CoreferenceView coref, String docid) {
		List<ELMention> mentions = annotations.docid2ELMention.get(docid);
		int c = 0;
		for (ELMention mention : mentions) {
			for (Constituent cons : ner) {
				if (mention.getMention().equals(cons.getSurfaceForm())
						&& mention.notFound()) {
					mention.setStartSpan(cons.getStartSpan());
					mention.setEndSpan(cons.getEndSpan());
					c++;
				}
			}
		}
		for (ELMention mention : mentions) {
			if (!mention.notFound()) {
				// System.out.println(mention);
				List<Constituent> cons = coref.getConstituentsCoveringSpan(
						mention.getStartSpan(), mention.getEndSpan());
				// System.out.println(cons);
			}
		}
		// System.out.println("===============");
		// for (Constituent cons : coref) {
		// // System.out.println(cons.getAttributeKeys());
		// // System.out.println(cons.getAttribute("MENTION_TYPE"));
		// // System.out.println(cons.getAttribute("ENTITY_TYPE"));
		// // System.out.println(cons.getAttribute("COARSE_ENTITY_TYPE"));
		// System.out.println("COREFLABEL " + cons.getLabel());
		// System.out.println(cons + " " + cons.getStartSpan() + " "
		// + cons.getEndSpan());
		// }
		// System.out.println("--------------------------------");
		System.out.println(docid + ":" + c + "/" + mentions.size());
	}

	private static void processBuf(List<String> buf, NYTAnnotations annotations) {

		String str = buf.get(0);
		String[] parts;
		String docid = str.substring(0, str.indexOf('\t'));
		// System.out.println(docid);
		annotations.docids.add(docid);
		assert (!annotations.docid2ELMention.containsKey(docid));

		annotations.docid2ELMention.put(docid, new ArrayList<ELMention>());

		for (int i = 1; i < buf.size(); i++) {
			parts = buf.get(i).split("\\t");
			String surface = parts[3];
			int salience = Integer.parseInt(parts[1]);
			// int start = Integer.parseInt(parts[4]);
			// int end = Integer.parseInt(parts[5]);
			String mid = parts[6];
			ELMention mention = new ELMention(docid, surface, salience, mid);
			// System.out.println(surface + " " + start + " " + end);

			annotations.docid2ELMention.get(docid).add(mention);
		}
		// System.out.println(buf);
	}


	public static NYTAnnotations readAnnotations(String annotationFile)
			throws IOException {
		NYTAnnotations annotations = new NYTAnnotations();
		BufferedReader br = new BufferedReader(new BufferedReader(
				new FileReader(new File(annotationFile))));
		String line;
		List<String> buf = new ArrayList<String>();
		// Map<String,List<>>
		while ((line = br.readLine()) != null) {
			// System.out.println(line);
			if (line.trim().length() == 0) {
				// process
				processBuf(buf, annotations);
				buf = new ArrayList<String>();
				continue;
			}
			buf.add(line);
		}
		br.close();
		return annotations;
	}

}
