package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.annotation.Annotator;
import edu.illinois.cs.cogcomp.annotation.TextAnnotationBuilder;
import edu.illinois.cs.cogcomp.annotation.handler.*;
import edu.illinois.cs.cogcomp.comma.CommaLabeler;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.curator.CuratorAnnotator;
import edu.illinois.cs.cogcomp.curator.CuratorClient;
import edu.illinois.cs.cogcomp.nlp.common.PipelineConfigurator;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.util.SimpleCachingPipeline;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.SemaforAnnotator;
import edu.illinois.cs.cogcomp.srl.SemanticRoleLabeler;
import edu.stanford.nlp.pipeline.POSTaggerAnnotator;
import edu.stanford.nlp.pipeline.ParserAnnotator;

import java.util.*;

public class MyCuratorClient {

	private static final String CONFIG_FILE = "config/caching-curator.properties";
	public ResourceManager rm;
	public SimpleCachingPipeline client;
	public static TextAnnotationBuilder taBuilder = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());;

	public MyCuratorClient(boolean alreadyCached) throws IllegalArgumentException,
			Exception {

		if (alreadyCached)
		{
			rm = new ResourceManager(CONFIG_FILE);
			Map<String, Annotator> viewProviders = new HashMap<>();
			viewProviders.put(ViewNames.NER_CONLL, null);
			viewProviders.put(ViewNames.NER_ONTONOTES, null);
			viewProviders.put(ViewNames.COREF, null);
			viewProviders.put(ViewNames.SRL_NOM, null);
			viewProviders.put(ViewNames.SRL_VERB, null);
			viewProviders.put("SRL_COMMA", null);
			viewProviders.put(ViewNames.SHALLOW_PARSE, null);
			viewProviders.put(ViewNames.LEMMA, null);
			viewProviders.put(ViewNames.POS, null);
			viewProviders.put(ViewNames.PARSE_STANFORD, null);
			viewProviders.put(ViewNames.DEPENDENCY_STANFORD, null);
			viewProviders.put("FRAMENET_FRAMES", null);
			client = new SimpleCachingPipeline(taBuilder, viewProviders, rm);
		}
		else
		{
			rm = new ResourceManager(CONFIG_FILE);
			CuratorClient curatorClient = new CuratorClient(rm);
			String[] requiredViews;
			ResourceManager defaultRM = new PipelineConfigurator().getConfig(rm);
			List<Annotator> annotators = new ArrayList<>();

			annotators.add(new IllinoisPOSHandler());
			annotators.add(new IllinoisNerHandler(defaultRM, ViewNames.NER_CONLL));
			annotators.add(new IllinoisNerHandler(defaultRM, ViewNames.NER_ONTONOTES));

			requiredViews = new String[]{ViewNames.SENTENCE, ViewNames.TOKENS,
					ViewNames.POS};
			annotators.add(new IllinoisChunkerHandler());
			annotators.add(new IllinoisLemmatizerHandler(defaultRM));
			annotators.add(new CuratorAnnotator(curatorClient, ViewNames.COREF,
					requiredViews));

			// custom annotators go here!!
//			annotators.add(new SemanticRoleLabeler("config/srl-config.properties", "Verb"));
//			annotators.add(new SemanticRoleLabeler("config/srl-config.properties", "Nom"));

			annotators.add(new CommaLabeler());
			annotators.add(new SemaforAnnotator());
			annotators.add(new SemanticRoleLabeler("Verb"));
			annotators.add(new SemanticRoleLabeler("Nom"));

			Properties stanfordProps = new Properties();
			stanfordProps.put("annotators", "pos, parse");
			stanfordProps.put("parse.originalDependencies", true);
			stanfordProps.put("parse.maxlen", 60);
			stanfordProps.put("parse.maxtime", 1000); // per sentence? could be per document but no idea from stanford javadoc
			POSTaggerAnnotator posAnnotator = new POSTaggerAnnotator("pos", stanfordProps);
			ParserAnnotator parseAnnotator = new ParserAnnotator("parse", stanfordProps);

			annotators.add(new StanfordDepHandler(posAnnotator, parseAnnotator));
			annotators.add(new StanfordParseHandler(posAnnotator, parseAnnotator));


			Map<String, Annotator> viewProviders = new HashMap<>(annotators.size());
			for (Annotator annotator : annotators) {
				viewProviders.put(annotator.getViewName(), annotator);
			}

			client = new SimpleCachingPipeline(taBuilder, viewProviders, rm);
		}
	}

	public void cleanUp() {
//		client.closeCache();
	}

}
