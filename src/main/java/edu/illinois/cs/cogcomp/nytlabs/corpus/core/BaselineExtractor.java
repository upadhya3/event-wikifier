package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Sentence;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.utils.TAUtils;

public class BaselineExtractor {

	private static int getBaselineScore(NYTCorpusDocument nydoc,
			MyCuratorClient cc) throws AnnotatorException {
		TextAnnotation absTA = AnnotateAbstracts.annotate(nydoc.articleAbstract, cc, true);
		System.out.println("FIRST PARA:" + nydoc.paragraphs.get(0));
		TextAnnotation TA = AnnotateAbstracts.annotate(nydoc.paragraphs.get(0), cc, false);
		Constituent pred;
		try {
			pred = getBaselineEvent(TA);
			System.out.println("BEST " + pred);
		} catch (EdisonException e) {
			System.out.println("Edison error");
			return 0;
		}
		TokenLabelView tok_lemmas = (TokenLabelView) absTA
				.getView(ViewNames.LEMMA);
		List<String> ll = new ArrayList<>();
		for (Constituent cons : tok_lemmas.getConstituents()) {
			ll.add(cons.getLabel());
		}
		if (ll.contains(TAUtils.getLemma(pred))) {
			System.out.println("LEMMA MATCH!!" + pred + " lemma was:"
					+ TAUtils.getLemma(pred));
			return 1;
		}
		return 0;
	}

	private static Constituent getBaselineEvent(TextAnnotation ta)
			throws EdisonException {
		// PredicateArgumentView verb = (PredicateArgumentView)
		// ta.getView(ViewNames.SRL_VERB);
		// PredicateArgumentView nom = (PredicateArgumentView)
		// ta.getView(ViewNames.SRL_NOM);
		List<String> views = Arrays.asList(ViewNames.SRL_VERB,
				ViewNames.SRL_NOM, "SRL_COMMA");
		List<Constituent> subOrdClauses = TAUtils.getSubOrdClauses(ta);
		List<Constituent> relativeClauses = TAUtils.getRelativeClauses(ta,
				subOrdClauses);

		Constituent best = null;
		int start = 999999;
		int end = 999999;
		for (int i = 0; i < ta.getNumberOfSentences(); i++) {
			Sentence sent = ta.getSentence(i);
			for (String view : views) {
				PredicateArgumentView paview = (PredicateArgumentView) ta
						.getView(view);
				for (Constituent cons : paview.getConstituentsCoveringSpan(
						sent.getStartSpan(), sent.getEndSpan())) {
					if (cons.getLabel().equals("Predicate")) {
						System.out.println("VOICE: "
								+ TAUtils.voiceFeat(cons) + " " + cons);
						List<Relation> rels = cons.getOutgoingRelations();

						for (Relation relIns : rels) {
							Constituent arg = relIns.getTarget();
							System.out.println("==>" + relIns.getRelationName()
									+ " " + arg.getSurfaceForm());
						}

						boolean insideSubord = false;
						boolean insideRelative = false;
						for (Constituent clause : subOrdClauses) {
							if (clause.doesConstituentCover(cons)) {
								insideSubord = true;
								break;
							}
						}
						for (Constituent clause : relativeClauses) {
							if (clause.doesConstituentCover(cons)) {
								insideRelative = true;
								break;
							}
						}

						if (insideSubord) {
							if (insideRelative) {
								System.out.println("INSIDE RELATIVE");
							} else
								System.out.println("INSIDE SUBORD");
						}

						if (!insideRelative && !insideSubord) {
							if (cons.getStartSpan() < start) {
								System.out.println("SWITCH " + cons + " "
										+ best);
								best = cons;
								start = cons.getStartSpan();
							}
						}
					}
					if (view.equals("SRL_COMMA")) {
						System.out.println(cons + " " + cons.getLabel());
					}
				}
			}
		}
		assert best != null : "This cannot be null!";
		return best;
	}

}
