package edu.illinois.cs.cogcomp.nytlabs.corpus;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;

import edu.stanford.nlp.ie.machinereading.domains.ace.AceReader;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.StringUtils;

public class ACEEvents {

	
	public static void main(String[] args) throws IOException {
	    Properties props = new Properties();
	    //StringUtils.argsToProperties(args);
	    props.load(new FileReader(new File("StanfordCoreNLP.properties")));
	    AceReader r = new AceReader(new StanfordCoreNLP(props, false), false);
	    r.setLoggerLevel(Level.INFO);
	    r.parse("/shared/shelley/yqsong/eventData/ACE2005/data/English");
	    // Annotation a = r.parse("/user/mengqiu/scr/twitter/nlp/corpus_prep/standalone/ar/data");
	    // BasicEntityExtractor.saveCoNLLFiles("/tmp/conll", a, false, false);
	    System.err.println("done");
	  }
}
