package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.readers.FlatReader;
import edu.illinois.cs.cogcomp.readers.LazyNYTReader;
import edu.illinois.cs.cogcomp.readers.NoisyLabeler;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.List;

/**
 * Created by upadhya3 on 2/15/16.
 */
public class ComputeCounts {

    static Counter<Pair<String, String>> frameCountBody = new Counter<>();
    static Counter<Pair<String, String>> frameCountAbs = new Counter<>();

    public static void main(String[] args) throws Exception {
        List<String> docids = AnnotateAbstracts.getTrainIds("world.txt"); //Evaluator.getGoldDocIds();
        boolean alreadyCached = true;
        MyCuratorClient cc = new MyCuratorClient(alreadyCached);
        LazyNYTReader docReader = new LazyNYTReader(cc, alreadyCached, docids);
        FlatReader reader = new FlatReader(new NoisyLabeler(docReader),false);
        EventInstance inst = (EventInstance) reader.next();

        while(inst!=null)
        {
            Pair<String, String> tmp = new Pair<>(TAUtils.getLemma(inst.pred_srl), TAUtils.getFrame(inst.pred_srl));
            frameCountBody.incrementCount(tmp);
        }
        for (Pair<String, String> k : frameCountBody
                .getSortedItemsHighestFirst()) {
            System.out.println(k + " " + frameCountBody.getCount(k));
        }
    }
}
