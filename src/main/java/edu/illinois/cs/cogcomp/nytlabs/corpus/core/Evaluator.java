package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.heurisitic.*;
import edu.illinois.cs.cogcomp.nytlabs.corpus.Params;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.readers.LazyNYTReader;
import edu.illinois.cs.cogcomp.utils.TAUtils;

public class Evaluator {

    public static void main(String[] args) throws Exception {
//        computeGoldFeatureCounts(new LazyNYTReader());
//        Evaluator.evaluate(new LazyNYTReader(), new VoiceHeuristic("A"),"abs",false);
//        Evaluator.evaluate(new LazyNYTReader(), new ClauseHeuristic("main"),"abs",false);
        Evaluator.evaluate(new LazyNYTReader(), new VoiceClauseHeuristic(),"abs",true);
//        Evaluator.evaluate(new LazyNYTReader(), new IsFirstHeuristic(),"abs",false);
//        Evaluator.evaluate(new LazyNYTReader(), new IsFirstNAVHeuristic(),"abs",false);
//        NYTAnnotations.ExtractAbstracts("allGoodAbstracts.txt");
    }

    private static void computeGoldFeatureCounts(LazyNYTReader reader, boolean verbose) throws FileNotFoundException, EdisonException {
        AnnotatedDocument doc = reader.next();
        String type="abs";
        String suffix;
        int total_events=0;
        int gold_events=0;
        List<EventInstance> target;
        Counter<String> pos_feats = new Counter<>();
        Counter<String> neg_feats = new Counter<>();
        while(doc!=null) {
            if (type.equals("abs")) {
                suffix = "_abs.ann";
                target = doc.abs_events;
            } else {
                suffix = "_body.ann";
                target = doc.body_events;
            }
            List<EventInstance> goldann = Evaluator.readGoldData(doc, Params.goldPath + "/" + doc.nydoc.getGuid() + suffix, type, verbose);
            gold_events+=goldann.size();
            System.out.println("gold:"+goldann.size());
            PredicateArgumentView verbsrl = (PredicateArgumentView) doc.getAbsTA().getView(ViewNames.SRL_VERB);
            PredicateArgumentView nomsrl = (PredicateArgumentView) doc.getAbsTA().getView(ViewNames.SRL_NOM);
            total_events+=verbsrl.getPredicates().size()+nomsrl.getPredicates().size();
            System.out.println("all events:"+(verbsrl.getPredicates().size()+nomsrl.getPredicates().size()));
//            for (EventInstance ee : goldann) {
//                assert ee.salient == 1;
//                String clause = TAUtils.getClauseNature(ee.pred_srl);
//                String voice = TAUtils.voiceFeat(ee.pred_srl).getName();
//                System.out.println(clause+" "+voice+" "+ee);
//                pos_feats.incrementCount(clause);
//                pos_feats.incrementCount(voice);
//                pos_feats.incrementCount(clause+" "+voice);
//            }
//            for (EventInstance ee : doc.abs_events)
//            {
//                if(!goldann.contains(ee))
//                {
//                    String clause = TAUtils.getClauseNature(ee.pred_srl);
//                    String voice = TAUtils.voiceFeat(ee.pred_srl).getName();
//                    System.out.println(clause+" "+voice+" "+ee);
//                    neg_feats.incrementCount(clause);
//                    neg_feats.incrementCount(voice);
//                    neg_feats.incrementCount(clause+" "+voice);
//                }
//                else
//                {
//                    System.out.println(ee+" in gold");
//                }
//
//            }
            doc=reader.next();
        }
//        System.out.println("POS");
//        for(String cc:pos_feats.getSortedItemsHighestFirst())
//        {
//            System.out.println(cc+" "+pos_feats.getCount(cc));
//        }
//        System.out.println("NEG");
//        for(String cc:neg_feats.getSortedItemsHighestFirst())
//        {
//            System.out.println(cc+" "+neg_feats.getCount(cc));
//        }
        System.out.println(gold_events+"/"+total_events);
    }


    public static void evaluate(LazyNYTReader reader, Heuristic h, String type, boolean verbose) throws EdisonException, FileNotFoundException {
        EvaluationRecord r = new EvaluationRecord();
        String suffix;
        List<EventInstance> target;
        AnnotatedDocument doc = reader.next();
        while(doc!=null)
        {
            Heuristic.ApplyHeuristic(doc, h, type);
            if(type.equals("abs"))
            {
                suffix="_abs.ann";
                target=doc.abs_events;
            }
            else
            {
                suffix="_body.ann";
                target=doc.body_events;
            }
            List<EventInstance> goldann = Evaluator.readGoldData(doc, Params.goldPath + "/" + doc.nydoc.getGuid() + suffix,type,verbose);
            Evaluator.evaluateOnGoldData(r, goldann,target,verbose);
            doc=reader.next();
        }

        if(r.getPrecision()>1.0 || r.getRecall() > 1.0 || r.getF1() > 1.0)
        {
            System.err.println("Something wrong!!! metric > 1 !!!");
            System.out.println(r.getCorrectCount()+" "+r.getPredictedCount()+" "+r.getGoldCount()+" "+r.getPrecision()+" "+r.getRecall()+" "+r.getF1());
            System.exit(-1);
        }
        System.out.println("Gold:"+r.getGoldCount()+" Predicted:"+r.getPredictedCount()+" Correct:"+r.getCorrectCount()+" P:"+r.getPrecision()+" R:"+r.getRecall()+" F:"+r.getF1());
    }

    /**
     * Reads gold data. The annotations are read from file directly.
     * @param doc
     * @param goldFile full dataPath to file
     * @param type either abs or body
     * @param verbose
     * @return
     * @throws FileNotFoundException
     */
    public static List<EventInstance> readGoldData(AnnotatedDocument doc, String goldFile, String type, boolean verbose) throws FileNotFoundException {
        List<EventInstance> gold = new ArrayList<>();
        List<String> lines = LineIO.read(goldFile);
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            if (line.startsWith("#"))
                continue;
            else {
                String[] parts = line.split("\\s+");
                int salient = Integer.parseInt(parts[1]);
                int start=Integer.parseInt(parts[2]);
                int end=Integer.parseInt(parts[3]);
                String surface = parts[4];
                if(salient==1) {
                    TextAnnotation ta;
                    if(type.equals("abs"))
                        ta = doc.getAbsTA();
                    else
                        ta = doc.getBodyTA();

                    int tokenId = ta.getTokenIdFromCharacterOffset(start);

                    PredicateArgumentView srlverb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
                    PredicateArgumentView srlnom = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
                    List<Constituent> verbcons = TAUtils.filterPredicates(srlverb.getConstituentsCoveringToken(tokenId));
                    List<Constituent> nomcons = TAUtils.filterPredicates(srlnom.getConstituentsCoveringToken(tokenId));
                    Constituent con = null;
                    if(verbcons.size()!=0)
                        con=verbcons.get(0);
                    else if(nomcons.size()!=0)
                        con=nomcons.get(0);
                    else
                    {
                        System.out.println("BAD ANNOTATION!!!");
                        System.out.println(surface);
                        System.out.println(srlverb.getPredicates());
                        System.out.println(srlnom.getPredicates());
                        System.exit(-1);
                    }
                    assert con!=null;
                    EventInstance tmp = new EventInstance(con, TAUtils.getFrame(con));
                    tmp.salient=1;
                    gold.add(tmp);
//                    List<EventInstance> events;
//                    if(type.equals("abs"))
//                        events = doc.abs_events;
//                    else
//                        events = doc.body_events;
//
//                    for (EventInstance e : events) {
//                        if (e.pred_srl.getStartCharOffset() == start && e.pred_srl.getEndCharOffset() == end) {
//                            System.out.println("Found!" + e.pred_srl + " "+ surface);
//                            e.salient = salient;
//                        }
//                    }
                }

            }
        }
//        if(type.equals("abs"))
//            return doc.abs_events;
//        else
//            return doc.body_events;
        if(verbose)
            System.out.println("read "+gold.size() +" gold for ... "+goldFile);
        return gold;

    }

    public static void evaluateOnGoldData(EvaluationRecord r, List<EventInstance> goldann, List<EventInstance> predevents, boolean verbose) throws FileNotFoundException, EdisonException {
//        ClassificationTester tester = new ClassificationTester();
//        System.out.println(goldann.size());
        for (EventInstance inst : predevents) {
            if (inst.salient == 1) // THESE are what we care about
                r.incrementPredicted();
        }
        for (EventInstance gold : goldann) {
            if (gold.salient == 1) // THESE are what we care about
            {
                r.incrementGold();
            }
        }
        for (EventInstance inst : predevents) {
            boolean found = false;
            for (EventInstance gold : goldann) {
                if (gold.start == inst.start && gold.end == inst.end && gold.salient == inst.salient && gold.salient == 1) {
                    if (verbose) {
                        System.out.println("Got it! " + inst);
                    }
                    r.incrementCorrect();
                    found = true;
                }
            }
            if (verbose) {
                if (!found && inst.salient == 1)
                    System.out.println("wrong pred:" + inst);
            }
        }
        if(verbose){
            System.out.println("PRED");
            for (EventInstance pred : predevents) {
                String voice = TAUtils.voiceFeat(pred.pred_srl).getName();
                String clause = TAUtils.getClauseNature(pred.pred_srl);
                boolean isFirst = TAUtils.isFirstNonAppVerbPredicateInList(pred, predevents);
                boolean inapp = TAUtils.isInAppositive(pred.pred_srl);
                System.out.println(pred + " " + voice + " " + clause + " " + isFirst + " " + inapp);
            }
            System.out.println("GOLD");
            for (EventInstance gold : goldann) {
                String voice = TAUtils.voiceFeat(gold.pred_srl).getName();
                String clause = TAUtils.getClauseNature(gold.pred_srl);
                boolean isFirst = TAUtils.isFirstNonAppVerbPredicateInList(gold, predevents);
                boolean inapp = TAUtils.isInAppositive(gold.pred_srl);
                System.out.println(gold + " " + voice + " " + clause + " " + isFirst + " " + inapp);
//            if(!predevents.contains(gold))
//            {
//                System.out.println("missed gold: "+gold);
//            }
            }
            System.out.println("======================");
        }
    }

    /**
     * Reads gold documents body and finds human annotated salient events in them.
     * @param reader
     * @throws FileNotFoundException
     */
    private static void analyzeGoldDocs(LazyNYTReader reader, boolean verbose) throws FileNotFoundException {
        AnnotatedDocument doc = reader.next();
        int totsalient = 0;
        int zeros=0;
        while(doc!=null)
        {
            List<EventInstance> goldann = Evaluator.readGoldData(doc, Params.goldPath + "/" + doc.nydoc.getGuid() + "_body.ann","body", verbose);
            int docsalient = 0;
            for (EventInstance bodyE : goldann) {
                if (bodyE.salient == 1)
                    docsalient++;
            }
            if(docsalient==0) {
                System.out.println(doc.nydoc.getGuid());
                zeros++;
            }
            totsalient+=docsalient;
            doc=reader.next();
        }
        System.out.println("total salient "+totsalient);
        System.out.println("docs with no salient proj " + zeros + "/" + reader.docids.size());
    }

    /**
     * to project (detected/hand annotated) salience from abstract to body
     * @param abs_sal_events
     */
    public static int projectLabelToDoc(List<EventInstance> to_project, List<EventInstance> abs_sal_events){
        int projected = 0;
        List<EventInstance> body_events = to_project;
        for (EventInstance goldE : abs_sal_events) {
//            if(goldE.salient==0)
//                continue;
            for (EventInstance bodyE : body_events) {
                String s1,s2,s3;
                // LEMMA BASED PROJECTION
                s1 = TAUtils.getLemma(goldE.pred_srl).toLowerCase();//goldE.surface.toLowerCase();
                s2 = bodyE.pred_srl.getSurfaceForm().toLowerCase();
                s3= TAUtils.getLemma(bodyE.pred_srl).toLowerCase();

                // FRAME BASED PROJECTION
//                if(goldE.frame!=null)
//                {
//                    if(bodyE.frame.equals(goldE.frame))
//                        bodyE.salient=1;
//                }
//                s1 = TAUtils.getLemma(goldE.pred_srl).toLowerCase();
//                s2 = TAUtils.getLemma(bodyE.pred_srl).toLowerCase();

//                System.out.println(s1+" "+s2+" "+s3);

                if (s1.equals(s2) || s1.equals(s3)) {
//                System.out.println(goldE.pred_srl+" "+bodyE.pred_srl);
                    bodyE.salient = goldE.salient;
                    projected++;
                }
            }
        }
//        ForBrat.writeEvents(Params.goldPath,goldDoc.nydoc.getGuid()+"",goldDoc.body_events, goldDoc.body_events.get(0).pred_srl.getTextAnnotation(),"body");
        return projected;
    }

    public static List<String> getGoldDocIds()
    {
        List<String> docids= new ArrayList<>();
        docids.add("1453101");
        docids.add("1453108");
        docids.add("1453109");
        docids.add("1453115");
        docids.add("1453118");
        docids.add("1453121");
        docids.add("1453123");
        docids.add("1453130");
        docids.add("1453150");
        docids.add("1453154");
        docids.add("1453188");
        docids.add("1453197");
        docids.add("1453199");
        docids.add("1453200");
        docids.add("1453203");
        docids.add("1453317");
        docids.add("1453326");
        docids.add("1453341");
        docids.add("1453343");
        docids.add("1453348");
        docids.add("1453353");
        docids.add("1453354");
        docids.add("1453356");
        docids.add("1453362");
        docids.add("1453363");
        docids.add("1453488");
        docids.add("1453490");
        docids.add("1453497");
        docids.add("1453505");
        docids.add("1453508");
        docids.add("1453528");
        docids.add("1453533");
        docids.add("1453537");
        docids.add("1453552");
        docids.add("1453686");
//        docids.add("1453694"); SRL failures
        //docids.add("1453703");
        //docids.add("1453707"); // SRL failures
        docids.add("1453709");
        docids.add("1453711");
        docids.add("1453733");
        docids.add("1453756");
        docids.add("1454143");
        docids.add("1454147");
        docids.add("1454149");
        docids.add("1454152");
        docids.add("1454153");
        docids.add("1454163");
//        docids.add("1454176"); SRL failures
        docids.add("1454177");
        docids.add("1454368");
        docids.add("1454381");
        docids.add("1454382");
        docids.add("1454387");
        docids.add("1454390");
        docids.add("1454416");
        docids.add("1454423");
        docids.add("1454425");
        docids.add("1454428");
        docids.add("1454440");
        docids.add("1454442");
        docids.add("1454518");
        docids.add("1454523");
        docids.add("1454536");
        docids.add("1454542");
        docids.add("1454546");
//        docids.add("1454547"); SRL fails
        docids.add("1454597");
        docids.add("1454630");
        docids.add("1454755");
        docids.add("1454778");
        docids.add("1454780");
        docids.add("1454786");
        docids.add("1454787");
//        docids.add("1454803");
        docids.add("1454806");
        docids.add("1454807");
        docids.add("1454821");
        docids.add("1454849");
        docids.add("1454852");
        docids.add("1454855");
        docids.add("1454898");
        docids.add("1454906");
        docids.add("1455007");
        docids.add("1455010");
        docids.add("1455014");
        docids.add("1455017");
        docids.add("1455020");
        docids.add("1455023");
        docids.add("1455027");
        docids.add("1455028");
        docids.add("1455031");
        docids.add("1455036");
        docids.add("1455044");
        docids.add("1455069");
        docids.add("1455072");
        docids.add("1455083");
        docids.add("1455252");
        docids.add("1455253");
        docids.add("1455257");
        docids.add("1455295");
        docids.add("1455297");

        return docids;
    }


//    public static void EvaluateOnTriggerSurface(TextAnnotation absTa, List<Constituent> event_triggers, ClassificationTester tester, List<String> acceptableFrames) {
//        List<String>event_trigger_surfaces = new ArrayList<String>();
//        for(Constituent ee:event_triggers)
//        {
//            event_trigger_surfaces.add(ee.getSurfaceForm());
//        }
////        PredicateArgumentView srlverb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
////        PredicateArgumentView srlnom = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
////        List<Constituent> absConsList = srlverb.getConstituents();
////        absConsList.addAll(srlnom.getConstituents());
//
//        System.out.println("----------------------");
//        List<Constituent> absConsList= AnnotateAbstracts.frameTriggeringEvents(absTa, acceptableFrames);
//        System.out.println("ABSTRACT WAS: "+absTa.getText());
//        List<String>abs_predicate_surfaces = new ArrayList<String>();
//        for(Constituent ee:absConsList)
//        {
//            abs_predicate_surfaces.add(ee.getSurfaceForm());
//        }
//
//        for(String abs_predicate: abs_predicate_surfaces) {
//            if(event_trigger_surfaces.contains(abs_predicate))
//            {
//                tester.record("PRESENT","PRESENT");
//            }
//            else
//            {
//                tester.record("PRESENT","ABSENT");
//                System.out.println("Missed recall "+abs_predicate);
//            }
//        }
//        // hurt precision
//        for(String predicted: event_trigger_surfaces) {
//            if(!abs_predicate_surfaces.contains(predicted))
//            {
//                tester.record("ABSENT","PRESENT");
//            }
//        }
//    }

//    static int emptyAbstracts = 0;
//
//    public static void EvaluateOnAbstractFrames(List<EventInstance> absEvents,
//                                                List<EventInstance> bodyEvents, ClassificationTester tester,
//                                                List<String> acceptableFrames) {
//        List<String>triggered_frame_surfaces = new ArrayList<String>();
//        for(EventInstance ee:bodyEvents)
//        {
////            for(Frame ff:ee.frames)
////            triggered_frame_surfaces.add(ff.getName());
//        }
//
//        List<String>abs_frame_surfaces = new ArrayList<String>();
//        for(EventInstance ee:absEvents)
//        {
////            for(Frame ff:ee.frames)
////                abs_frame_surfaces.add(ff.getName());
//        }
//
////        List<Frame> absFrameList= AnnotateAbstracts.TriggeredFrames(absTa, acceptableFrames);
////        if(absFrameList.size()==0)
////        {
////        	emptyAbstracts++;
////        	System.out.println("nothing here!: "+emptyAbstracts);
////        }
//
////        System.out.println("ABSTRACT WAS: "+absTa.getText());
////        List<String>abs_frame_surfaces = new ArrayList<String>();
////        for(Frame ee:absFrameList)
////        {
////            abs_frame_surfaces.add(ee.getName());
////        }
//
//        for(String abs_frame: abs_frame_surfaces) {
//            if(triggered_frame_surfaces.contains(abs_frame))
//            {
//                tester.record("PRESENT","PRESENT");
//            }
//            else
//            {
//                tester.record("PRESENT","ABSENT");
//                System.out.println("Missed recall "+abs_frame);
//            }
//        }
//        // hurt precision
//        for(String predicted: triggered_frame_surfaces) {
//            if(!abs_frame_surfaces.contains(predicted))
//            {
//                tester.record("ABSENT","PRESENT");
//            }
//        }
//    }

}
