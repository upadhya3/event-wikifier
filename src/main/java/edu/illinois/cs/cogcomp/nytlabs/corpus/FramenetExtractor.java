package edu.illinois.cs.cogcomp.nytlabs.corpus;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.framenet.Frame;
import edu.illinois.cs.cogcomp.framenet.FrameNetManager;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Shyam on 11/23/15.
 */
public class FramenetExtractor {

    private static final Logger logger = LoggerFactory
            .getLogger(FramenetExtractor.class);

    static FrameNetManager fm = new FrameNetManager("/shared/bronte/upadhya3/framenet_1.5");
    private final List<String> acceptableFrames;
    private boolean checkFrameMembership;
    public FramenetExtractor() throws FileNotFoundException {
        List<String> all_frames = LineIO.read("accept_frames_ALL.txt");
        this.acceptableFrames = all_frames.subList(0, Params.frameK);
        this.checkFrameMembership = true;
        if(checkFrameMembership)
            System.out.println("DEFAULT: frame membership is on");
        else
            System.out.println("DEFAULT: frame membership is off!");
    }

    public List<EventInstance> extract(TextAnnotation ta){

        List<EventInstance> events = new ArrayList<EventInstance>();
        PredicateArgumentView srlverb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
        PredicateArgumentView srlnom = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
        if(srlverb!=null) {
            for (Constituent cons : srlverb.getConstituents()) {
                if (!cons.getLabel().equals("Predicate"))
                    continue;
                String lemma = TAUtils.getLemma(cons);
                // exclude "be" predicates
                if (lemma.equals("be")) {
                    continue;
                }
                String frame = TAUtils.getFrame(cons);
                if(checkFrameMembership)
                {
                    if (acceptableFrames.contains(frame)) {
                        events.add(new EventInstance(cons, frame));
                    }
                }
                else
                {
                    events.add(new EventInstance(cons, frame));
                }
            }
        }
        else
        {
            System.out.println("srl verb is null");
        }
        if(srlnom!=null) {
            for (Constituent cons : srlnom.getConstituents()) {
                if (!cons.getLabel().equals("Predicate"))
                    continue;
                String frame = TAUtils.getFrame(cons);
                if(checkFrameMembership)
                {
                    if (acceptableFrames.contains(frame)) {
                        events.add(new EventInstance(cons, frame));
                    }
                }
                else
                {
                    events.add(new EventInstance(cons, frame));
                }

            }
        }
        else
        {
            System.out.println("srl nom is null");
        }
        return events;
    }

    private static Set<Frame> getTriggeredFrames(String token, String lemma, String tag) {
        List<Pair<String,String>> tmp=new ArrayList<>();
        tmp.add(new Pair<String, String>(tag,token));
        Set<Frame> frames = fm.getRelevantFrames(tmp, 1);
        if(frames.size()==0 && tag.equals("VERB")) // try lemma only if its a verb
        {
            tmp=new ArrayList<>();
            tmp.add(new Pair<String, String>(tag,lemma));
            frames = fm.getRelevantFrames(tmp, 1);
        }
        return frames;
    }


//    public static List<Constituent> frameTriggeringEvents(TextAnnotation ta, List<String> acceptableFrames) {
//        List<Constituent> triggers = new ArrayList<>();
//        int predicate_count=0;
//
//    }

//    public static List<Frame> TriggeredFrames(TextAnnotation ta,
//                                              List<String> acceptableFrames) {
//        List<Frame> triggered_frames = new ArrayList<>();
//        int predicate_count=0;
//        PredicateArgumentView srlverb = (PredicateArgumentView) ta.getView(ViewNames.SRL_VERB);
//        for(Constituent tok:srlverb.getConstituents())
//        {
//            boolean keep=false;
//            if(!tok.getLabel().equals("Predicate"))
//                continue;
//            List<Pair<String,String>> tmp=new ArrayList<>();
//            tmp.add(new Pair<String, String>("VERB",tok.getSurfaceForm()));
//            Set<Frame> frames = fm.getRelevantFrames(tmp, 1);
//            if(frames.size()==0)
//            {
//                tmp=new ArrayList<>();
//                tmp.add(new Pair<String, String>("VERB",TAUtils.getLemma(tok)));
//                frames = fm.getRelevantFrames(tmp, 1);
//            }
//            System.out.print("SRLVERB "+tok+" triggered frame: ");
//            for(Frame ff:frames)
//            {
//                System.out.print(ff.getName() + " ");
//                if(acceptableFrames.contains(ff.getName()))
//                {
//                    keep=true;
//                    triggered_frames.add(ff);
//                }
//            }
//            System.out.println();
//            if(keep)
//            {
//                System.out.println("ACCEPT: "+tok);
//            }
//            predicate_count++;
//        }
//        PredicateArgumentView srlnom = (PredicateArgumentView) ta.getView(ViewNames.SRL_NOM);
//        for(Constituent tok:srlnom.getConstituents())
//        {
//            boolean keep=false;
//            if(!tok.getLabel().equals("Predicate"))
//                continue;
//            List<Pair<String,String>> tmp=new ArrayList<>();
//            tmp.add(new Pair<String, String>("NOUN",tok.getSurfaceForm()));
//            Set<Frame> frames = fm.getRelevantFrames(tmp, 1);
//            if(frames.size()==0)
//            {
//                tmp=new ArrayList<>();
//                tmp.add(new Pair<String, String>("NOUN",TAUtils.getLemma(tok)));
//                frames = fm.getRelevantFrames(tmp, 1);
//            }
//            System.out.print("SRLNOM "+tok + " triggered frame: ");
//            for(Frame ff:frames)
//            {
//                System.out.print(ff.getName() + " ");
//                if(acceptableFrames.contains(ff.getName()))
//                {
//                    keep=true;
//                    triggered_frames.add(ff);
//                }
//            }
//            System.out.println();
//            if(keep)
//            {
//                System.out.println("ACCEPT: "+tok);
//            }
//            predicate_count++;
//        }
//        System.out.println("trigger out of "+triggered_frames.size()+"/"+predicate_count);
//        return triggered_frames;
//    }


    /**
     * Reads ACE mentions from a tsv file and finds the frames they trigger. Writes them for generating acceptable frames.
     * @param s
     * @throws FileNotFoundException
     */
    public static void ACEFramesEvent2(String s) throws Exception {
        MyCuratorClient cc = new MyCuratorClient(false);

        List<String> lines = LineIO.read(s);
        for(int i=0;i<lines.size();i++)
        {
            String line = lines.get(i);
            String[] parts = line.split("_SEP_");
            assert parts.length==2;
            String sentence = parts[0];
            String[] spans= parts[1].split("\\s+");
            assert spans.length==2;
            int start=Integer.parseInt(spans[0]);
            int end=Integer.parseInt(spans[1]);
            TextAnnotation ta = AnnotateAbstracts.annotate(sentence,cc,false);
            SpanLabelView frameView = (SpanLabelView) ta.getView("FRAMENET_FRAMES");
            System.out.println(frameView.getLabel(start));

//            System.out.println(Arrays.asList(ta.getTokensInSpan(start, end+1))+" "+frameView.getLabel(start));
//            List<Constituent> cons = frameView.getSpanLabels(start, end + 1);
//            for(Constituent con:frameView.getConstituents())
//            {
//                System.out.println(con+" "+con.getLabel());
//            }
        }
    }

    /**
     * Reads ACE mentions from a tsv file and finds the frames they trigger. Writes them for generating acceptable frames.
     * @param s
     * @throws FileNotFoundException
     */
    public static void ACEFramesEvent(String s) throws FileNotFoundException {
        List<String> lines = LineIO.read(s);
        for(String line:lines)
        {
            String[] parts=line.split("\t");
            String tok=parts[0];
            String pos = parts[1];
            String lemma = parts[2];
            List<Pair<String, String>> tmp = new ArrayList<>();
            String tag="";
            if(pos.contains("NN"))
            {
                tag="NOUN";
            }
            if(pos.contains("VB"))
            {
                tag="VERB";
            }

            Set<Frame> frames = getTriggeredFrames(tok, lemma, tag);

//            tmp.add(new Pair<String, String>(tag,tok));
//            Set<Frame> frames = fm.getRelevantFrames(tmp, 1);
//
//            if(frames.size()==0)
//            {
//                tmp=new ArrayList<>();
//                tmp.add(new Pair<String, String>(tag,lemma));
//                frames = fm.getRelevantFrames(tmp, 1);
//            }

            System.out.print(tok + ":");
            for(Frame frame:frames)
            {
                System.out.print(frame.getName() + " ");
            }
            System.out.println();


        }
    }
}
