package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import edu.cmu.cs.lti.ark.fn.Semafor;
import edu.cmu.cs.lti.ark.fn.data.prep.formats.Sentence;
import edu.cmu.cs.lti.ark.fn.parsing.SemaforParseResult;
import edu.illinois.cs.cogcomp.annotation.Annotator;
import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import edu.illinois.cs.cogcomp.framenet.SemaforUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by upadhya3 on 12/6/15.
 */
public class SemaforAnnotator extends Annotator {
    private static final String framenet_frames = "FRAMENET_FRAMES";
    Semafor semafor;
    private static final String[] ans = new String[]{ViewNames.TOKENS,ViewNames.POS};

    public SemaforAnnotator() throws URISyntaxException, IOException, ClassNotFoundException {
        super(framenet_frames,ans);
        semafor= Semafor.getSemaforInstance("/home/upadhya3/code/java_code/models/semafor_malt_model_20121129");
    }

    @Override
    public String getViewName() {
        return framenet_frames;
    }

    @Override
    public void addView(TextAnnotation ta) throws AnnotatorException {
        List<Sentence> cmusents = SemaforUtils.getCMUSentences(ta);
        SpanLabelView frames = new SpanLabelView(getViewName(), ta);
        for(int sidx=0;sidx<cmusents.size();sidx++) {
            Sentence cmusent=cmusents.get(sidx);
            SemaforParseResult result = null;
            try {
                result = semafor.parseSentence(cmusent);
//                System.out.println(result.toJson());
                edu.illinois.cs.cogcomp.core.datastructures.textannotation.Sentence tasent = ta.getSentence(sidx);
                for (SemaforParseResult.Frame frame : result.frames) {
//                    System.out.println(frame.target.name);
                    for (SemaforParseResult.Frame.Span span : frame.target.spans) {
//                        System.out.println(span.start + " " + span.end + " " + span.text);
                        int sstart=tasent.getStartSpan()+span.start;
                        int send=tasent.getStartSpan()+span.end;
//                        System.out.println("SENT:"+tasent+" "+tasent.getStartSpan()+" "+tasent.getEndSpan());
//			            System.out.println(sstart+" "+send);
//                        System.out.println(Arrays.asList(ta.getTokensInSpan(sstart, send)));
                        frames.addSpanLabel(sstart, send,frame.target.name,1.0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ta.addView(framenet_frames,frames);
    }

    @Override
    public String[] getRequiredViews() {
        return ans;
    }
}
