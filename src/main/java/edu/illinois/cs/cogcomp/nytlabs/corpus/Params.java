package edu.illinois.cs.cogcomp.nytlabs.corpus;

import java.util.HashMap;
import java.util.Map;

public class Params {
	public static final float OVERLAP_THRESHOLD = 0.4f;
	public static String dataPath = "/shared/corpora/corporaWeb/written/eng/NYT_annotated_corpus/data/accum2003-07";// args[0];
	public static String trainAnnotations = "/home/upadhya3/nyt/nyt-train";
	public static String testAnnotations = "/home/upadhya3/nyt/nyt-eval";

	public static float cosineThresHold = 0.5f;
	public static String dumpPath = "mycache";
	public static String brat_data = "/shared/bronte/upadhya3/other/brat/data/nyt/data/viz";
	public static int frameK = 224;
    public static String goldPath = "/home/upadhya3/newspeg/gold";//"/Users/Shyam/code/other/brat-v1.3_Crunchy_Frog/data/examples/newspeg/stephen";

	static
	{
		Map<String,String> ace2frame = new HashMap<>();
//		ace2frame.put("BORN","being_born");
//		marry,
//		injure
//		Contacting Phone-Write
//		Extradition Justice-Extradition
//		Attack Conflict-Attack
//		Being Born Life-Be Born
//		Personal_relationship: date.v sleep_with.v widow.n affair.n moll.n bachelor.n seeing.v betrothed.n marriage.n break-up.n partner.n engagement.n spinster.n adultery.n friendship.n mistress.n paramour.n significant_other.n suitor.n widow.v court.v sugar_daddy.n divorcee.n lover.n amigo.n spouse.n boyfriend.n widower.n befriend.v mate.n beau.n chum.n cobber.n pal.n inamorata.n companion.n cohabit.v cohabitation.n couple.n husband.n friend.n wife.n romance.n crush.n buddy.n girlfriend.n

	}
}
