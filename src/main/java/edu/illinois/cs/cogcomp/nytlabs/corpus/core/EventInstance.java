package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import com.fasterxml.jackson.databind.introspect.Annotated;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.framenet.Frame;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by upadhya3 on 10/20/15.
 */
public class EventInstance implements IInstance{

    public Constituent subj;
    public String surface;
    public int start; // char offset
    public int end; // char offset
    public  String subjlemma;
    public Constituent dep_pred;
    public  String predlemma;
    public Constituent obj;
    public  String objlemma;
    public int sentId;
    public int salient=0;

    public Constituent pred_srl;
    public List<Pair<Constituent,String>> srl_args; // cons and its role

    public String frame;
	public IFeatureVector base_fv; // feature vector for this event predicate-arg struct
    public AnnotatedDocument parent;

    public EventInstance(Constituent predicate, String frame)
    {
        this.frame=frame;
        this.pred_srl=predicate;
        this.start=predicate.getStartCharOffset();
        this.end=predicate.getEndCharOffset();
        this.surface=predicate.getSurfaceForm();
    }

    public void setParent(AnnotatedDocument doc)
    {
        this.parent=doc;
    }

    public EventInstance(Constituent subj, String subjlemma,Constituent pred,String predlemma, Constituent obj,String objlemma, int sentId) {
        this.subj = subj;
        this.subjlemma = subjlemma;
        this.dep_pred = pred;
        this.predlemma = predlemma;
        this.obj = obj;
        this.objlemma = objlemma;
        this.sentId = sentId;
    }

    public EventInstance(Constituent predCons,String predlemma, List<Relation> arguments, int sentId) {
        this.pred_srl = predCons;
        this.predlemma = predlemma;
        this.srl_args = new ArrayList<>();
        for(Relation arg:arguments)
        {
            srl_args.add(new Pair(arg.getTarget(),arg.getRelationName()));
        }
    }

    public EventInstance(int salience,int start, int end, String surface) {
        this.salient=salience;
        this.start=start;
        this.end=end;
        this.surface=surface;
    }

    @Override
    public String toString() {
        return start+" "+end+" "+pred_srl.getSurfaceForm()+" "+salient+" "+pred_srl.getViewName();
//        if(srl_args==null)
//            return "["+subj+","+ dep_pred +","+obj+"]"+":"+sentId;
//        else
//        {
//            String srlargsSign = "";
//            for(Pair<Constituent,String >pp:srl_args)
//            {
//                srlargsSign+=pp.getFirst().getSurfaceForm()+"--"+pp.getSecond();
//            }
//            return "PRED: "+ pred_srl +" "+srlargsSign+":"+sentId;
//        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventInstance that = (EventInstance) o;

        if (start != that.start) return false;
        if (end != that.end) return false;
        return !(surface != null ? !surface.equals(that.surface) : that.surface != null);

    }

    @Override
    public int hashCode() {
        int result = surface != null ? surface.hashCode() : 0;
        result = 31 * result + start;
        result = 31 * result + end;
        return result;
    }
}
