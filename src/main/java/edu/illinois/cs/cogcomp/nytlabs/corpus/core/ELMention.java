package edu.illinois.cs.cogcomp.nytlabs.corpus.core;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.nlp.utilities.CollinsHeadFinder;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;

import java.util.List;

/**
 * Copied from entity linking
 *
 * @author upadhya3
 *
 */
public class ELMention implements IInstance {
	private String id;
	public String mention;
	private String docid;
	private String type;
	private int start_offset=-1; // the true offset not the one in nyt-salience
	private int end_offset=-1;
	private String language;
	private String mid = "NIL";
	private String freebase_title = "NIL";
	private String wiki_title = "NIL";
	private int salience;
	public IFeatureVector base_fv; // feat for SL
	public int base_nfeats; // total # of feats
	private TextAnnotation ta; // ta of parent doc
	private int endSpan=-1;
	private int startSpan=-1;
	private NYTCorpusDocument nydoc;

	// public ELMention(String docid, int start, int end) {
	// this.docid = docid;
	// this.start_offset = start;
	// this.end_offset = end;
	//
	// }

	public ELMention(String id, String mention, String docid) {
		this.id = id;
		this.mention = mention;
		this.docid = docid;
	}

	public ELMention(String docid, String mention, int salience, String mid) {
		this.docid = docid;
		this.mention = mention;
		this.setSalience(salience);
		this.freebase_title = mid;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setLanguage(String lang) {
		this.language = lang;
	}

	public void setStartOffset(int start) {
		this.start_offset = start;
	}

	public void setEndOffset(int end) {
		this.end_offset = end;
	}

	public void setMid(String ans) {
		this.mid = ans;
	}

	public void setMention(String mention) {
		this.mention = mention;
	}

	public void setFBTitle(String t) {
		this.freebase_title = t;
	}

	public void setWikiTitle(String t) {
		this.wiki_title = t;
	}

	public String getID() {
		return this.id;
	}

	public String getMention() {
		return this.mention;
	}

	public String getDocID() {
		return this.docid;
	}

	public String getType() {
		return this.type;
	}

	public String getLanguage() {
		return this.language;
	}

	public int getStartOffset() {
		return this.start_offset;
	}

	public int getEndOffset() {
		return this.end_offset;
	}

	public String getMid() {
		return this.mid;
	}

	public String getWikiTitle() {
		return this.wiki_title;
	}

	public String getFBTitle() {
		return this.freebase_title;
	}

	@Override
	public String toString() {
		return mention + " " + startSpan+ "," + endSpan;
	}

	public int getSalience() {
		return salience;
	}

	public void setSalience(int salience) {
		this.salience = salience;
	}

	public void setTA(TextAnnotation TA) {
		ta = TA;
	}

	public TextAnnotation getTA() {
		return ta;
	}

	public boolean notFound() {
		if(startSpan==-1 && endSpan==-1)
			return true;
		return false;
	}

	public void setStartSpan(int startSpan) {
		this.startSpan=startSpan;
	}
	public int getStartSpan() {
		return startSpan;
	}
	public void setEndSpan(int endSpan) {
		this.endSpan=endSpan;
	}
	public int getEndSpan() {
		return endSpan;
	}

	public Constituent  getMentionSentence() {
		TextAnnotation ta = this.getTA();
		SpanLabelView sent = (SpanLabelView) ta.getView(ViewNames.SENTENCE);
		List<Constituent> mention = sent.getConstituentsCoveringSpan(
				this.getStartSpan(), this.getEndSpan());
		assert mention.size() == 1 : " COULD NOT FIND SENT! mention was " + this;
		Constituent sentence = mention.get(0);
		return sentence ;
	}

	public Constituent getHeadWord() {
		TextAnnotation ta = this.getTA();
		TreeView parse = (TreeView) ta.getView(ViewNames.PARSE_STANFORD);
		CollinsHeadFinder headFinder = CollinsHeadFinder.getInstance();
		try {
			Constituent parsePhrase = parse.getParsePhrase(new Constituent("",
					"", ta, this.getStartSpan(), this.getEndSpan()));
			Constituent head = headFinder.getHeadWord(parsePhrase);
			// System.out.println("MENTION " + m);
			// System.out.println("PARSE PHRASE " + parsePhrase);
			// System.out.println("HEAD " + head);
			// System.out.println("-----------------------------------");
			return head;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	static int corefErr=0;

	public List<Constituent> getCorefCluster() {
		TextAnnotation ta = this.getTA();
		CoreferenceView coref = (CoreferenceView) ta.getView(ViewNames.COREF);
//		CoreferenceView coref = (CoreferenceView) ta.getView(StanfordCoref.MYVIEW);
		List<Constituent> mentions = coref.getConstituentsCoveringSpan(
				this.getStartSpan(), this.getEndSpan());
		// assert mentions.size()==1 : " COULD NOT FIND! mention was "+m;
		// Constituent mention = mentions.get(0);
		if (mentions.size() != 0) {
			Constituent mention = mentions.get(0);
			List<Constituent> cluster = coref.getCoreferentMentions(mention);
			return cluster;
		} else {
			System.err.println("WHAT IS WRONG WITH COREF? " + corefErr + " "
					+ this.getSalience());
//			System.exit(-1);
			corefErr++;
		}
		return null;
	}

	Constituent getFirstMention() {
		TextAnnotation ta = this.getTA();
		CoreferenceView coref = (CoreferenceView) ta.getView(ViewNames.COREF);
//		CoreferenceView coref = (CoreferenceView) ta.getView(StanfordCoref.MYVIEW);
		List<Constituent> mentions = coref.getConstituentsCoveringSpan(
				this.getStartSpan(), this.getEndSpan());
		Constituent leftmost = coref.getCanonicalEntity(mentions.get(0));
		return leftmost;
	}

	public void setNYDoc(NYTCorpusDocument nydoc) {
		this.nydoc=nydoc;
	}


	public NYTCorpusDocument getNYDoc() {
		return nydoc;
	}

}
