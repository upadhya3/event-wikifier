package edu.illinois.cs.cogcomp.salience.learning;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.core.utilities.Table;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.ELMention;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.salience.learning.entity.Label;
import edu.illinois.cs.cogcomp.salience.learning.event.EventBasicFeatureExtractor;
import edu.illinois.cs.cogcomp.salience.learning.event.EventSalienceFeatureGenerator;
import edu.illinois.cs.cogcomp.salience.learning.event.EventSimpleInferenceSolver;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.core.SLModel;
import edu.illinois.cs.cogcomp.sl.core.SLParameters;
import edu.illinois.cs.cogcomp.sl.core.SLProblem;
import edu.illinois.cs.cogcomp.sl.learner.Learner;
import edu.illinois.cs.cogcomp.sl.learner.LearnerFactory;
import edu.illinois.cs.cogcomp.sl.util.DenseVector;
import edu.illinois.cs.cogcomp.sl.util.Lexiconer;
import edu.illinois.cs.cogcomp.sl.util.WeightVector;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

	static String learnerConfig="config/DCD.config";

	public static void main(String args[]) throws Exception {
		InteractiveShell<Main> tester = new InteractiveShell<Main>(Main.class);
		if (args.length == 0)
			tester.showDocumentation();
		else {
			tester.runCommand(args);
		}
	}

	public static void NoisySupervisionTrain(AnnotatedDocument doc) throws Exception {
			List<EventInstance> abs_events = doc.abs_events, body_events = doc.body_events;
			for (EventInstance ee : body_events) {
				for (EventInstance aa : abs_events) {
					if (ee.pred_srl.getSurfaceForm().toLowerCase().equals(aa.pred_srl.getSurfaceForm().toLowerCase())) {
						ee.salient = 1;
					}
				}
			}
	}

//	private static SLProblem getBalancedExamples(List<AnnotatedDocument> docs) {
//		List<EventInstance> positive=new ArrayList<>();
//		List<EventInstance> negative=new ArrayList<>();
//		int docSalientCount=0;
//		int c=0;
//		for(AnnotatedDocument doc:docs){
//			docSalientCount=0;
//			for (EventInstance ee : doc.body_events) {
//				if(ee.salient==1)
//				{
//					positive.add(ee);
//					docSalientCount++;
//				}
//			}
//			c=0;
//			for (EventInstance ee : doc.body_events) {
//				if(ee.salient==0 && c<=docSalientCount)
//				{
//					negative.add(ee);
//					c++;
//				}
//			}
//
//		}
//		System.out.println(positive.size());
//		System.out.println(negative.size());
//
//		positive.addAll(negative);
//		Collections.shuffle(positive,new Random(0));
//		SLProblem trainProb = createSLProblem(positive);
//		return trainProb;
//	}
//
//	private static SLProblem getAllExamples(List<AnnotatedDocument> docs) {
//		List<EventInstance> positive=new ArrayList<>();
//		List<EventInstance> negative=new ArrayList<>();
//		int docSalientCount=0;
//		for(AnnotatedDocument doc:docs){
//			for (EventInstance ee : doc.body_events) {
//				if(ee.salient==1)
//				{
//					positive.add(ee);
//				}
//			}
//			for (EventInstance ee : doc.body_events) {
//				if(ee.salient==0)
//				{
//					negative.add(ee);
//				}
//			}
//
//		}
//		System.out.println("+ve instance: "+positive.size());
//		System.out.println("-ve instance: "+negative.size());
//
//		positive.addAll(negative);
//		Collections.shuffle(positive,new Random(0));
//		SLProblem trainProb = createSLProblem(positive);
//		return trainProb;
//	}

//	private static SLModel trainEventSalience(SLProblem trainProb, EventBasicFeatureExtractor ff) throws Exception {
//		ff.lm.setAllowNewFeatures(true);
////
//		// pre extract
//		for (IInstance m : trainProb.instanceList) {
//			ff.populateBaseFeature((EventInstance) m);
//		}
//
//		SLModel model = new SLModel();
//		ff.lm.setAllowNewFeatures(false);
//
//		model.para = new SLParameters();
//		model.para.loadConfigFile(learnerConfig);
//
//		model.lm = ff.lm;
//		System.out.println(ff.lm.getNumOfFeature());
//		model.featureGenerator = new EventSalienceFeatureGenerator(ff.lm.getNumOfFeature());
//		model.infSolver = new EventSimpleInferenceSolver(ff.lm.getNumOfFeature());
//
//		Learner learner = LearnerFactory.getLearner(model.infSolver,
//				model.featureGenerator, model.para);
//		model.wv = learner.train(trainProb);
//		model.lm.setAllowNewFeatures(false);
//		String modelPath = "sp_model.save";
////		model.saveModel(modelPath);
//		WeightVector.printSparsity(model.wv);
//		printToFileMultiClass(model.wv,model.lm,2,"tmp.weights");
//		testModel(trainProb, model);
//		return model;
//	}
//
//	public static void printToFileMultiClass(DenseVector wv, Lexiconer lm,int numClasses, String filepath) throws FileNotFoundException {
//		float[] ff= wv.getInternalArray();
//		PrintWriter w = new PrintWriter(filepath);
//		for(int c=0;c<numClasses;c++)
//		{
//			for(int i=0;i<lm.getNumOfFeature();i++)
//			{
//				w.println(lm.getFeatureString(i)+"#"+c+" "+ff[c*lm.getNumOfFeature()+i]);
//			}
//		}
//		// sanity
//		for(int i=numClasses*lm.getNumOfFeature();i<ff.length;i++)
//		{
//			if(ff[i]!=0.0f)
//			{
//				System.out.println("whoops at "+ff[i]+" "+i);
//			}
//		}
//		System.out.println("size of wv:"+wv.getLength());
//		w.close();
//	}
////	public static void expt(String learnerConfig) throws Exception {
////		List<ELMention> all_examples = MainClass.getExamples(200, "train");
////		List<ELMention> test_examples = MainClass.getExamples(10, "test");
////
////		List<ELMention> balanced_examples = getBalance(all_examples);
////
////		BaseFeatureExtractor ff = new BaseFeatureExtractor();
////		ff.lm.setAllowNewFeatures(true);
////
////		// pre extract
////		for (ELMention m : all_examples) {
////			ff.populateBaseFeature(m);
////		}
////
////		for (ELMention m : test_examples) {
////			ff.populateBaseFeature(m);
////		}
////
////		SLProblem trainProb = createSLProblem(balanced_examples);
////		SLProblem testProb = createSLProblem(all_examples);
////		SLProblem testProb2 = createSLProblem(test_examples);
////
////		SLModel model = new SLModel();
////		ff.lm.setAllowNewFeatures(false);
////
////		model.para = new SLParameters();
////		model.para.loadConfigFile(learnerConfig);
////
////		model.lm = ff.lm;
////		model.featureGenerator = new EntitySalienceFeatureGenerator(
////				ff.lm.getNumOfFeature());
////		model.infSolver = new SimpleInferenceSolver(ff.lm.getNumOfFeature());
////
////		Learner learner = LearnerFactory.getLearner(model.infSolver,
////				model.featureGenerator, model.para);
////		model.wv = learner.train(trainProb);
////		model.lm.setAllowNewFeatures(false);
////		String modelPath = "sp_model.save";
////		model.saveModel(modelPath);
////		WeightVector.printSparsity(model.wv);
////		testModel(trainProb, modelPath);
////		testModel(testProb, modelPath);
////		testModel(testProb2, modelPath);
////	}
//
//	private static List<ELMention> getBalance(List<ELMention> examples) {
//		int pos = 0;
//		int neg = 0;
//		List<ELMention> ans = new ArrayList<ELMention>();
//		for (ELMention men : examples) {
//			// System.out.println(men + " " + men.getStartSpan() + " "
//			// + men.getEndSpan());
//			// System.out.println(Arrays.asList(men.getTA().getTokensInSpan(
//			// men.getStartSpan(), men.getEndSpan())));
//			if (men.getSalience() == 1) {
//				pos++;
//				ans.add(men);
//			} else {
//				neg++;
//			}
//		}
//		System.out.println("Got " + examples.size());
//		System.out.println("POS " + pos);
//		System.out.println("NEG " + neg);
//		pos = pos / 2; // take half of positive
//		ans = ans.subList(0, ans.size() / 2);
//
//		Collections.shuffle(examples, new Random(0)); // seed for reproducing
//		for (ELMention men : examples) {
//			if (men.getSalience() == 0 && ans.size() < 2 * pos) {
//				ans.add(men);
//			}
//		}
//		pos = 0;
//		neg = 0;
//		for (ELMention men : ans) {
//			// System.out.println(men + " " + men.getStartSpan() + " "
//			// + men.getEndSpan());
//			// System.out.println(Arrays.asList(men.getTA().getTokensInSpan(
//			// men.getStartSpan(), men.getEndSpan())));
//			if (men.getSalience() == 1) {
//				pos++;
//			} else {
//				neg++;
//			}
//		}
//		System.out.println("Got " + examples.size());
//		System.out.println("POS " + pos);
//		System.out.println("NEG " + neg);
//
//		return ans;
//	}
//
////	private static SLProblem createSLProblem(List<ELMention> examples) {
////		SLProblem sp = new SLProblem();
////		for (ELMention example : examples) {
////			sp.instanceList.add(example);
////			Label l = new Label();
////			l.label = example.getSalience();
////			sp.goldStructureList.add(l);
////		}
////		return sp;
////	}
//
//	private static SLProblem createSLProblem(List<EventInstance> examples) {
//		SLProblem sp = new SLProblem();
//		for (EventInstance example : examples) {
//			sp.instanceList.add(example);
//			Label l = new Label();
//			l.label = example.salient;
//			sp.goldStructureList.add(l);
//		}
//		return sp;
//	}
//
//	public static void testModel(SLProblem sl, SLModel model) throws Exception {
//		ClassificationTester tester = new ClassificationTester();
//		for (int i = 0; i < sl.instanceList.size(); i++) {
//			EventInstance m = (EventInstance) sl.instanceList.get(i);
//			Label pred = (Label) model.infSolver.getBestStructure(model.wv, m);
//			tester.record(m.salient + "", pred.label+ "");
//		}
//		Pair<Table, List<String>> conftable = tester.getConfusionTable();
//		Table table = conftable.getFirst();
//		List<String> labels = conftable.getSecond();
//		// System.out.println("ATTENTION!"+table.getColumnCount()+table.getRowCount());
//		// System.out.println(table.toOrgTable());
//		// System.out.println(tester.getAverageF1());
//		// System.out.println(tester.getPerformanceTable().toOrgTable());
//		Table perfTable = tester.getPerformanceTable();
////			perfTable.getValueAt(perfTable.getRowCount(),)
////		System.out.println(perfTable.getRowCount()+" "+perfTable.getColumnCount());
////		System.out.println(perfTable.getValueAt(perfTable.getRowCount()-1,4));
//		System.out.println(perfTable.toOrgTable());
//	}
//
//	static void testModel(SLProblem sl, String modelPath) throws Exception {
//		SLModel model = SLModel.loadModel(modelPath);
//		ClassificationTester tester = new ClassificationTester();
//		for (int i = 0; i < sl.instanceList.size(); i++) {
//			ELMention m = (ELMention) sl.instanceList.get(i);
//			Label pred = (Label) model.infSolver.getBestStructure(model.wv, m);
//			tester.record(m.getSalience() + "", pred.label + "");
//		}
//		Pair<Table, List<String>> conftable = tester.getConfusionTable();
//		Table table = conftable.getFirst();
//		List<String> labels = conftable.getSecond();
//		// System.out.println("ATTENTION!"+table.getColumnCount()+table.getRowCount());
//		// System.out.println(table.toOrgTable());
//		// System.out.println(tester.getAverageF1());
//		// System.out.println(tester.getPerformanceTable().toOrgTable());
//		Table perfTable = tester.getPerformanceTable();
//		System.out.println("P " + perfTable.getValueAt(1, 4));
//		System.out.println("R " + perfTable.getValueAt(1, 5));
//		System.out.println("F1 " + perfTable.getValueAt(1, 6));
//
//	}
}
