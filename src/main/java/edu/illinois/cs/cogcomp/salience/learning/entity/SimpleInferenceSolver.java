package edu.illinois.cs.cogcomp.salience.learning.entity;

import edu.illinois.cs.cogcomp.nytlabs.corpus.core.ELMention;
import edu.illinois.cs.cogcomp.sl.core.AbstractInferenceSolver;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.core.IStructure;
import edu.illinois.cs.cogcomp.sl.util.WeightVector;

public class SimpleInferenceSolver extends AbstractInferenceSolver {

	private int numFeats;

	public float[][] lossMatrix = new float[2][2];

	public SimpleInferenceSolver(int numFeats) {
		this.numFeats = numFeats;
		lossMatrix[0][0] = 0.0f;
		lossMatrix[0][1] = 10.0f; // when gold is 0 and we predict 1
		lossMatrix[1][0] = 20.0f;
		lossMatrix[1][1] = 0.0f;

	}

	@Override
	public IStructure getBestStructure(WeightVector w, IInstance x)
			throws Exception {
		ELMention mention = (ELMention) x;
		float score1 = w.dotProduct(mention.base_fv, 0);
		float score2 = w.dotProduct(mention.base_fv, numFeats);
		Label l = new Label();
		if (score1 > score2)
			l.label = 0;
		else
			l.label = 1;

		return l;
	}

	@Override
	public float getLoss(IInstance arg0, IStructure gold, IStructure pred) {
		Label l1 = (Label) gold;
		Label l2 = (Label) pred;
		if (l1.label != l2.label) {
//			return (float) Math.log(1 + Math
//					.exp(-lossMatrix[l1.label][l2.label]));
			 return lossMatrix[l1.label][l2.label];
		}
		return 0;
	}

	@Override
	public IStructure getLossAugmentedBestStructure(WeightVector w,
			IInstance x, IStructure y) throws Exception {
		Label l = (Label) y;
		ELMention mention = (ELMention) x;
		float score1 = w.dotProduct(mention.base_fv, 0)
				+ lossMatrix[l.label][0];
		float score2 = w.dotProduct(mention.base_fv, numFeats)
				+ lossMatrix[l.label][1];
		Label ans = new Label();

		// this makes it very hard for classifier in training, so gets better
		// numbers
		if (l.label == 1) {
			ans.label = 0;
		} else {
			ans.label = 1;
		}

		// if(score1 > score2)
		// {
		// ans.label=0;
		// }
		// else{
		// ans.label=1;
		// }

		return ans;
	}

}
