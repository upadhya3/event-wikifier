package edu.illinois.cs.cogcomp.salience.learning.event;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.sl.util.FeatureVectorBuffer;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;
import edu.illinois.cs.cogcomp.sl.util.Lexiconer;
import edu.illinois.cs.cogcomp.utils.TAUtils;

public class EventBasicFeatureExtractor {

	public EventBasicFeatureExtractor() {
		lm = new Lexiconer();
	}

	public Lexiconer lm;

	public void populateBaseFeature(EventInstance m) {
		Map<String, Float> fmap = extractFeatures(m);
		m.base_fv = getBaseFeatureVector(fmap);
	}

	Map<String, Float> extractFeatures(EventInstance m) {
		Map<String, Float> fmap = new HashMap<>();
		addPositionFeats(m, fmap);
		addClauseNatureVoiceConjFeats(m,fmap);
		addTenseFeats(m,fmap);
//		addBOWFeats(m,fmap);
		// add other features
		return fmap;

	}

	private void addTenseFeats(EventInstance m, Map<String, Float> fmap) {
		String nature=TAUtils.getTense(m.pred_srl);
		fmap.put("tense_"+nature, 1.0f);
	}

	private void addClauseNatureVoiceConjFeats(EventInstance m, Map<String, Float> fmap) {
		String nature=TAUtils.getClauseNature(m.pred_srl);
		fmap.put("clause_"+nature, 1.0f);
//		try {
//			Set<Feature> voice = TAUtils.voiceFeat(m.pred_srl);
//			fmap.put("voice_"+voice.toString(),10.f);
//		} catch (EdisonException e) {
//			e.printStackTrace();
//		}
	}

	

	private void addBOWFeats(EventInstance m, Map<String, Float> fmap) {
		Constituent pred = m.pred_srl;
		int window = 3;
		// String[] rtok = TAUtils.getTokensInWindow(pred, window, "R");
		String[] rpos = TAUtils.getPosInWindow(pred, window, "R");
		// String[] ltok = TAUtils.getTokensInWindow(pred, window, "L");
		String[] lpos = TAUtils.getPosInWindow(pred, window, "L");

		// assert rtok.length==rpos.length;
		// assert ltok.length==lpos.length;

		// for (int i = 0; i < ltok.length; i++) {
		// 	fmap.put("L_" + ltok[i] + "_" + lpos[i] + "_" + pred.getSurfaceForm(),
		// 			1.0f);
		// }
		// for (int i = 0; i < rtok.length; i++) {
		// 	fmap.put("R_" + rtok[i] + "_" + rpos[i] + "_" + pred.getSurfaceForm(),
		// 			1.0f);
		// }
		for (int i = 0; i < lpos.length; i++) {
			fmap.put("L_" + lpos[i] + "_"  + pred.getSurfaceForm(),
					1.0f);
		}
		for (int i = 0; i < rpos.length; i++) {
			fmap.put("R_" + rpos[i] + "_"  + pred.getSurfaceForm(),
					1.0f);
		}
	}

	int countMatches(TextAnnotation ta, String head) {
		int ans = 0;
		for (String tok : ta.getTokens()) {
			if (tok.equals(head)) {
				ans++;
			}
		}
		return ans;
	}

	private IFeatureVector getBaseFeatureVector(Map<String, Float> fmap) {
		FeatureVectorBuffer fv = new FeatureVectorBuffer();
		for (String f : fmap.keySet()) {
			if (lm.isAllowNewFeatures())
				lm.addFeature(f);
			if (lm.containFeature(f))
				fv.addFeature(lm.getFeatureId(f), fmap.get(f));
			else
				fv.addFeature(lm.getFeatureId("W:unknownword"), 1.0f);

		}
		return fv.toFeatureVector();
	}

	FeatureNode[] getLibLinearBaseFeatureVector(List<String> fList) {
		FeatureNode[] fnode = new FeatureNode[fList.size()];
		int i = 0;
		for (String f : fList) {
			fnode[i++] = new FeatureNode(lm.getFeatureId(f), 1.0);
		}
		return fnode;

	}

	private void addPositionFeats(EventInstance m, Map<String, Float> fList) {
		Constituent pred= m.pred_srl;
		fList.put("sent_pos", (float) pred.getSentenceId());
	}

	public void toLiblinearProblem(List<EventInstance> examples)
			throws IOException {
		Problem svm = new Problem();
		svm.l = 1000; // # of examples
		svm.x = examples.toArray(new FeatureNode[examples.size()][]);
		svm.y = new double[1000];
		svm.n = lm.getNumOfFeature();
		SolverType solver = SolverType.L2R_LR; // -s 0
		double C = 1.0; // cost of constraints violation
		double eps = 0.01; // stopping criteria

		Parameter parameter = new Parameter(solver, C, eps);
		Model model = Linear.train(svm, parameter);
		File modelFile = new File("liblinear.model");
		model.save(modelFile);
		// load model or use it directly
		model = Model.load(modelFile);
	}
}
