package edu.illinois.cs.cogcomp.salience.learning.entity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.ELMention;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.NYTCorpusDocument;
import edu.illinois.cs.cogcomp.sl.util.FeatureVectorBuffer;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;
import edu.illinois.cs.cogcomp.sl.util.Lexiconer;

public class BaseFeatureExtractor {

	public BaseFeatureExtractor() {
		lm = new Lexiconer();
	}

	public Lexiconer lm;

	public void populateBaseFeature(ELMention m) {
		Map<String, Float> fmap = extractFeatures(m);
		m.base_fv = getBaseFeatureVector(fmap);
	}

	// void addToFeatures(Map<String,Float>fmap,List<String> booleanFeats)
	// {
	// }

	Map<String, Float> extractFeatures(ELMention m) {
		Map<String, Float> fmap = new HashMap<>();
		addPositionFeats(m, fmap);
		addCorefClusterFeats(m, fmap);
		addHeadFeats(m, fmap);
		// addLexFeats(m, fmap);
		addMentionCorefTypeFeats(m, fmap);
		addHeadlineFeats(m, fmap);
		// add other features

		return fmap;

	}

	private void addHeadlineFeats(ELMention m, Map<String, Float> fmap) {
		// Constituent head = m.getHeadWord();
		String mention = m.getMention();
		NYTCorpusDocument doc = m.getNYDoc();
		String[] headline_tokens = doc.getHeadline().split("\\s+");
		int c = 0;
		for (String tok : headline_tokens) {
			// if
			// (tok.toLowerCase().equals(head.getSurfaceForm().toLowerCase()))
			// c++;
			if (tok.toLowerCase().equals(mention.toLowerCase()))
				c++;
		}
		System.out.println("headline=" + c);
		fmap.put("headline=", (float) c);
	}

	// List<String> extractFeatures(ELMention m) {
	// List<String> fList = new ArrayList<>();
	// addPositionFeats(m, fList);
	// addCorefClusterFeats(m, fList);
	// addHeadFeats(m, fList);
	// addLexFeats(m, fList);
	// addMentionCorefTypeFeats(m, fList);
	// // add other features
	//
	// return fList;
	//
	// }

	private void addMentionCorefTypeFeats(ELMention m, Map<String, Float> fList) {
		List<Constituent> cluster = m.getCorefCluster();
		if (cluster != null) {
			int nam = 0;
			int pro = 0;
			int nom = 0;
			for (Constituent cons : cluster) {
				// System.out.println(cons.getAttribute("MENTION_TYPE"));
				switch (cons.getAttribute("MENTION_TYPE")) {
				case "NAM":
					nam++;
					break;
				case "NOM":
					nom++;
					break;
				case "PRO":
					pro++;
					break;

				default:
					break;
				}
			}
			// System.out.println(nam+" "+pro+" "+nom);
			fList.put("nam=", (float) nam);
			fList.put("nom=", (float) nom);
			fList.put("pro=", (float) pro);
			// fList.put("namnom=", (float) nam*nom);
			// fList.put("nompro=", (float) nom*pro);
			// fList.put("pronam=", (float) pro*nam);
			// fList.put("pronamnom=", (float) pro*nam*nom);

		}
	}

	

	// hurts a bit
	private void addLexFeats(ELMention m, Map<String, Float> fList) {
		Constituent head = m.getHeadWord();
		fList.put("head-lex=" + head.getSurfaceForm().toLowerCase(), 1.0f);
	}

	int countMatches(TextAnnotation ta, String head) {
		int ans = 0;
		for (String tok : ta.getTokens()) {
			if (tok.equals(head)) {
				ans++;
			}
		}
		return ans;
	}

	private void addHeadFeats(ELMention m, Map<String, Float> fList) {
		Constituent head = m.getHeadWord();
		TextAnnotation ta = m.getTA();
		if (head != null) {
			int count = countMatches(ta, head.getSurfaceForm());
			// if (count > 5)
			// fList.add("head_count=" + 1);
			// else
			// fList.add("head_count=" + 0);
			fList.put("head_count=", (float) count);

		}
	}

	private IFeatureVector getBaseFeatureVector(Map<String, Float> fmap) {
		FeatureVectorBuffer fv = new FeatureVectorBuffer();
		for (String f : fmap.keySet()) {
			if (lm.isAllowNewFeatures())
				lm.addFeature(f);
			if (lm.containFeature(f))
				fv.addFeature(lm.getFeatureId(f), fmap.get(f));
			else
				fv.addFeature(lm.getFeatureId("W:unknownword"), 1.0f);

		}
		return fv.toFeatureVector();
	}

	// IFeatureVector getBaseFeatureVector(List<String> fList) {
	// FeatureVectorBuffer fv = new FeatureVectorBuffer();
	// for (String f : fList) {
	// if (lm.isAllowNewFeatures())
	// lm.addFeature(f);
	// if (lm.containFeature(f))
	// fv.addFeature(lm.getFeatureId(f), 1.0f);
	// else
	// fv.addFeature(lm.getFeatureId("W:unknownword"), 1.0f);
	//
	// }
	// return fv.toFeatureVector();
	// }

	FeatureNode[] getLibLinearBaseFeatureVector(List<String> fList) {
		FeatureNode[] fnode = new FeatureNode[fList.size()];
		int i = 0;
		for (String f : fList) {
			fnode[i++] = new FeatureNode(lm.getFeatureId(f), 1.0);
		}
		return fnode;

	}

	static int corefErr = 0;

	private void addCorefClusterFeats(ELMention m, Map<String, Float> fList) {
		List<Constituent> cluster = m.getCorefCluster();
		if (cluster != null) {
			fList.put("cluster_size=", (float) cluster.size());
			
			int max=m.getStartSpan();
			for(Constituent cc:cluster)
			{
				if(cc.getStartSpan()>max)
				{
					max=cc.getStartSpan();
				}
			}
			System.out.println(max);
			int distance = max - m.getStartSpan();
			System.out.println("distance "+distance);
			
//			fList.put("farthest_mention=", (float) String.valueOf(distance).length());
			
		}
	}

	private void addPositionFeats(ELMention m, Map<String, Float> fList) {
		Constituent sentence = m.getMentionSentence();

		// if (sentence.getSentenceId() > 5) {
		// fList.add("sent_pos=" + 0);
		// } else {
		// fList.add("sent_pos=" + 1);
		// }

		fList.put("sent_pos", (float) sentence.getSentenceId());

	}

	public void toLiblinearProblem(List<ELMention> examples) throws IOException {
		Problem svm = new Problem();
		svm.l = 1000; // # of examples
		svm.x = examples.toArray(new FeatureNode[examples.size()][]);
		svm.y = new double[1000];
		svm.n = lm.getNumOfFeature();
		SolverType solver = SolverType.L2R_LR; // -s 0
		double C = 1.0; // cost of constraints violation
		double eps = 0.01; // stopping criteria

		Parameter parameter = new Parameter(solver, C, eps);
		Model model = Linear.train(svm, parameter);
		File modelFile = new File("liblinear.model");
		model.save(modelFile);
		// load model or use it directly
		model = Model.load(modelFile);
	}
}
