package edu.illinois.cs.cogcomp.salience.learning.entity;

import java.io.Serializable;

import edu.illinois.cs.cogcomp.nytlabs.corpus.core.ELMention;
import edu.illinois.cs.cogcomp.sl.core.AbstractFeatureGenerator;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.core.IStructure;
import edu.illinois.cs.cogcomp.sl.util.FeatureVectorBuffer;
import edu.illinois.cs.cogcomp.sl.util.IFeatureVector;

/**
 * 
 * @author upadhya3
 *
 */
public class EntitySalienceFeatureGenerator extends AbstractFeatureGenerator
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8246812640996043593L;

	int base_nfeats;
	public EntitySalienceFeatureGenerator(int numFeats) {
		base_nfeats=numFeats;
	}

	@Override
	public IFeatureVector getFeatureVector(IInstance x, IStructure y) {
		ELMention mention = (ELMention) x;
		Label l = (Label) y;
		FeatureVectorBuffer fvb = new FeatureVectorBuffer(mention.base_fv);
		fvb.shift(l.label * base_nfeats);
		return fvb.toFeatureVector();
	}

}
