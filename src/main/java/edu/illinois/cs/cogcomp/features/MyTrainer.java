package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.heurisitic.ClassifierHeuristic;
import edu.illinois.cs.cogcomp.heurisitic.VoiceClauseHeuristic;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjsrc.MyClassifier;
import edu.illinois.cs.cogcomp.lbjsrc.MyLabel;
import edu.illinois.cs.cogcomp.nytlabs.corpus.MyCuratorClient;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotateAbstracts;
import edu.illinois.cs.cogcomp.nytlabs.corpus.annotation.AnnotatedDocument;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.Evaluator;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.readers.ConstraintNoisyLabeler;
import edu.illinois.cs.cogcomp.readers.FlatReader;
import edu.illinois.cs.cogcomp.readers.LazyNYTReader;
import edu.illinois.cs.cogcomp.readers.NoisyLabeler;
import edu.illinois.cs.cogcomp.summarizers.GoldSummarizer;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

//import edu.illinois.cs.cogcomp.readers.NoisySupervisionReader;

/**
 * Created by upadhya3 on 2/15/16.
 */
public class MyTrainer {
    public static void main(String[] args) throws Exception {
        boolean alreadyCached = true;
        List<String> docids = AnnotateAbstracts.getTrainIds("world.txt"); //Evaluator.getGoldDocIds();
        List<String> train_ids = docids.subList(200,800);
        List<String> test_ids = docids.subList(100, 200);
        System.out.println("train size:"+train_ids.size());
        System.out.println("test size:"+test_ids.size());


        LazyNYTReader d1= new LazyNYTReader(train_ids);
        LazyNYTReader d2= new LazyNYTReader(test_ids);

        ConstraintNoisyLabeler trainReader = new ConstraintNoisyLabeler(d1, new VoiceClauseHeuristic());
        FlatReader f1 = new FlatReader(trainReader,false);

        MyClassifier learner = new MyClassifier();
        BatchTrainer trainer = new BatchTrainer(learner, f1);
        trainer.train(1);
        gold(learner,true);

//        getFeatureWeights(learner);

//        EventInstance doc = (EventInstance) testReader.next();
//        int i=0;
//        while(doc!=null)
//        {
//            System.out.println(i++);
//            doc= (EventInstance) testReader.next();
//        }

//        List<String> docs = Evaluator.getGoldDocIds();
//        LazyNYTReader goldReader = new LazyNYTReader(cc, alreadyCached, docs);
//        NoisySupervisionReader gold= new NoisySupervisionReader(goldReader);

//        trainReader.reset();
//        testReader.reset();
//        f1.reset();
//        f2.reset();
        // on train data
//        test(learner, new AllFeatures(), f1);
        // on test data
//        test(learner, new AllFeatures(), f2);

//        LazyNYTReader.main();
    }

    public static void gold(MyClassifier learner, boolean verbose) throws Exception {
        List<String> docids = Evaluator.getGoldDocIds();
        boolean alreadyCached = true;
        MyCuratorClient cc = new MyCuratorClient(alreadyCached);
//        KLSummarizer tmp = new KLSummarizer("/shared/bronte/upadhya3/NYTsummaries/kl/", "_body.txt.kldiv.1");
        GoldSummarizer tmp = new GoldSummarizer();
        LazyNYTReader reader = new LazyNYTReader(cc, alreadyCached, docids, tmp);
        ClassifierHeuristic ch = new ClassifierHeuristic(new MyClassifier(), new AllFeatures());
        Evaluator.evaluate(reader, ch,"abs",verbose);
        System.out.println("prediction profile pos:"+ch.posPred+" neg:"+ch.negPred);
    }
    public static void getFeatureWeights(Learner c) {
        ByteArrayOutputStream sout = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(sout);
        c.write(out);
        String s = sout.toString();
        System.out.println(s);
//        String[] lines = s.split("\n");
//        Lexicon lexicon = c.getLexicon();

//        Map<String, Double> feats = new HashMap<String, Double>();
//        for (int i = 2; i < lines.length - 1; ++i) {
//            String line = lines[i];
//            String featid = lexicon.lookupKey(i - 2).toStringNoPackage(); // .getStringIdentifier();
//            feats.put(featid, Double.parseDouble(line));
//            // String[] parts = line.split("   *");
//            // if (1 < parts.length)
//            // feats.put(parts[0], Double.parseDouble(parts[1]));
//        }
        // TODO: A non-hard-coded way to deal with conjunctions:
        // Map<String,Double> mins = getMinimumPartsOfCompounds(feats);
        // Map<String,Double> totals = getSumsOfParts(feats, mins);
        // Map<String, Double> totals = getTotalsOfPartsOfCompounds(feats);
        // for (Map.Entry<String, Double> pair : totals.entrySet()) {
        // feats.put(pair.getKey() + "_ALL", pair.getValue());
        // }
//        return feats;
    }

    public static void getDist(NoisyLabeler reader) throws Exception {

        AnnotatedDocument inst= (AnnotatedDocument) reader.next();
        int pos=0;
        int neg=0;
        while(inst!=null) {
            for(int i=0;i<inst.body_events.size();i++) {
                if (inst.body_events.get(i).salient == 1)
                    pos++;
                else
                    neg++;
            }
            inst= (AnnotatedDocument) reader.next();
        }
        System.out.println("Dist: pos-"+pos+" neg-"+neg);
        reader.reset();
    }
    public static void getDist(FlatReader reader) throws Exception {

        EventInstance inst= (EventInstance) reader.next();
        int pos=0;
        int neg=0;
        while(inst!=null) {
            if(inst.salient==1)
                pos++;
            else
                neg++;
            inst= (EventInstance) reader.next();
        }
        System.out.println("Dist: pos-"+pos+" neg-"+neg);
    }

    public static void test(MyClassifier learner, AllFeatures feat, FlatReader gold){
        MyClassifier c = new MyClassifier();

//        SparseNetworkLearner.Parameters params = new SparseNetworkLearner.Parameters();
//        c.setParameters(params);

        TestDiscrete.testDiscrete(new TestDiscrete(), c, new MyLabel(), gold,  true, 1000);

//        EventInstance inst= (EventInstance) gold.next();
//        while(inst!=null) {
//            FeatureVector f = feat.classify(inst);
//            System.out.println(f.featuresSize());
//            String ans = learner.discreteValue(f);
//            ScoreSet ss = learner.scores(inst);
//            System.out.println(ss.values()+" "+ans);
//            inst= (EventInstance) gold.next();
//        }
    }

}
