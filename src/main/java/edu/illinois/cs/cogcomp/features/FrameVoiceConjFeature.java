package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.edison.features.DiscreteFeature;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by upadhya3 on 2/17/16.
 */
public class FrameVoiceConjFeature extends LBJavaFeatureExtractor {
    @Override
    public Set<Feature> getFeatures(EventInstance instance) throws EdisonException {
        Feature f;
        f = new DiscreteFeature(TAUtils.getFrame(instance.pred_srl)+"_"+TAUtils.voiceFeat(instance.pred_srl).getName());
        HashSet<Feature> ans = new HashSet<Feature>();
        ans.add(f);
        return ans;
    }
}
