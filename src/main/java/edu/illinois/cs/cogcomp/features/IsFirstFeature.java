package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.edison.features.DiscreteFeature;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shyam on 2/24/16.
 */
public class IsFirstFeature extends LBJavaFeatureExtractor{
    @Override
    public Set<Feature> getFeatures(EventInstance instance) throws EdisonException {
        Feature f;
        assert instance.parent!=null;
        if(TAUtils.isFirstNonAppVerbPredicateInList(instance,instance.parent.abs_events))
            f = new DiscreteFeature("isFirst");
        else
            f = new DiscreteFeature("notFirst");

        HashSet<Feature> ans = new HashSet<Feature>();
        ans.add(f);
        return ans;
    }
}
