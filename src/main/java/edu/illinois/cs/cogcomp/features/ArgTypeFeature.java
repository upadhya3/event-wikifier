package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.edison.features.DiscreteFeature;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by upadhya3 on 2/18/16.
 */
public class ArgTypeFeature extends LBJavaFeatureExtractor {
    @Override
    public Set<Feature> getFeatures(EventInstance instance) throws EdisonException {
        Feature f;
        HashSet<Feature> ans = new HashSet<Feature>();
        for(Relation r: instance.pred_srl.getOutgoingRelations())
        {
            f=new DiscreteFeature(r.getRelationName());
            ans.add(f);
        }
        return ans;
    }
}
