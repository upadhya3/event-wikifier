package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.FeatureUtilities;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by upadhya3 on 2/11/16.
 */
public class AllFeatures extends Classifier {
    private static List<LBJavaFeatureExtractor> features;
    static
    {
        features = new ArrayList<>();
        features.add(new ClauseFeature());
        features.add(new TenseFeature());
//        features.add(new UnigramFeatures());
//        features.add(new BigramFeatures());
        features.add(new IsFirstFeature());
        features.add(new VoiceFeature()); // BAD BAD FEATURE WHEN ON GOLD DATA
        features.add(new FrameFeature());
//         features.add(new FrameVoiceConjFeature()); // 93.495 48.640 F1 63.990
//         features.add(new AppositiveVoiceConjFeature()); // 93.495 48.640 F1 63.990
        features.add(new FrameIsFirstConjFeature());
        features.add(new AppositiveFeature());
//        features.add(new SpecificityFeature());
//        features.add(new ArgTypeFeature());

//        features.add(new GoldFeature()); // cheating feature, for sanity
    }


    @Override
    public FeatureVector classify(Object o) {
        if (!(o instanceof EventInstance))
            throw new IllegalArgumentException("Instance must be of type EventInstance");
        EventInstance instance = (EventInstance) o;
        FeatureVector f = new FeatureVector();
        for(LBJavaFeatureExtractor fgen:features)
        {
            Set<Feature> feats = null;
            try {
                feats = fgen.getFeatures(instance);
                FeatureVector ff = FeatureUtilities.getLBJFeatures(feats);
                f.addFeatures(ff);
            } catch (EdisonException e) {
                e.printStackTrace();
            }
        }
        return f;
    }


}
