package edu.illinois.cs.cogcomp.features;

/**
 * Created by upadhya3 on 2/11/16.
 */
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.FeatureUtilities;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;

import java.util.Set;

/**
 * A simple wrapper for {@code LBJava}-based feature extractors
 */
public abstract class  LBJavaFeatureExtractor extends Classifier {

    @Override
    public String getOutputType() {
        return "discrete%";
    }

    @Override
    public FeatureVector classify(Object o) {
        //Make sure the object is a Constituent
        if (!(o instanceof EventInstance))
            throw new IllegalArgumentException("Instance must be of type EventInstance");
        EventInstance instance = (EventInstance) o;

        FeatureVector featureVector;
        try {
            featureVector = FeatureUtilities.getLBJFeatures(getFeatures(instance));
        } catch (EdisonException e) {
            throw new RuntimeException(e);
        }
        return featureVector;
    }

    public abstract Set<Feature> getFeatures(EventInstance instance) throws EdisonException;
}
