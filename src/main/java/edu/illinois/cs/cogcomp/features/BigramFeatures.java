package edu.illinois.cs.cogcomp.features;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Sentence;
import edu.illinois.cs.cogcomp.edison.features.DiscreteFeature;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.nytlabs.corpus.core.EventInstance;
import edu.illinois.cs.cogcomp.utils.TAUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by upadhya3 on 2/18/16.
 */
public class BigramFeatures extends LBJavaFeatureExtractor {
    @Override
    public Set<Feature> getFeatures(EventInstance instance) throws EdisonException {
        Feature f;
        Constituent cons = instance.pred_srl;
        Sentence sent = TAUtils.getSentence(cons);
        HashSet<Feature> ans = new HashSet<Feature>();
        String[] tokens = sent.getTokens();
        for(int i=0; i<tokens.length;i++)
        {
            if(i+1<tokens.length)
                f = new DiscreteFeature("bigram_"+tokens[i].toLowerCase()+"_"+tokens[i+1].toLowerCase());
            else
                f = new DiscreteFeature("bigram_"+tokens[i].toLowerCase()+"_"+"NULL");
            ans.add(f);
        }
        return ans;
    }
}
