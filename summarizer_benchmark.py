from __future__ import absolute_import
from __future__ import division, unicode_literals
import sys
import glob
import argparse
import os
# USAGE python summarizer_benchmark.py -dir ~/newspeg/stephen/ -outdir /shared/bronte/upadhya3/NYTsummaries/kl -suffix kldiv.1 -sent 1 -sum kl

LANGUAGE = "english"
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.sum_basic import SumBasicSummarizer
from sumy.summarizers.lex_rank import LexRankSummarizer
from sumy.summarizers.kl import KLSummarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words

import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Short sample app')
    parser.add_argument('-dir', action="store", dest="dir",type=str)
    parser.add_argument('-outdir', action="store", dest="outdir",type=str)
    parser.add_argument('-sent', action="store", dest="sent",type=int)
    parser.add_argument('-sum', action="store", dest="sum",type=str)
    parser.add_argument('-suffix', action="store", dest="suffix",type=str)
    options=parser.parse_args(sys.argv[1:])
    SENTENCES_COUNT = options.sent
    DATAPATH=options.dir
    OUTDIR=options.outdir
    METHOD=options.sum
    SUFFIX=options.suffix
    stemmer = Stemmer(LANGUAGE)
    dd={'lsa':LsaSummarizer,
        'sb':SumBasicSummarizer,
        'lex':LexRankSummarizer,
        'kl':KLSummarizer}
    print "using",dd[METHOD]
    summarizer = dd[METHOD](stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    start = time.time()
    for f in glob.glob(DATAPATH+"/*_body.txt"):
        fname=f.split("/")[-1]
        print fname
        file_path=OUTDIR+'/'+fname+"."+SUFFIX
        if os.path.exists(file_path):
            continue
        parser = PlaintextParser.from_file(f, Tokenizer(LANGUAGE))
        summary=summarizer(parser.document, SENTENCES_COUNT)
        out=open(file_path,'w')
        for i,sentence in enumerate(summary):
            print>>out,sentence
        out.close()
    elapsed = time.time()-start
    print "elapsed",elapsed 
